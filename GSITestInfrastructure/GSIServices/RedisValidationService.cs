﻿using GSITestInfrastructure.Infrastructure;
using GSITestInfrastructure.Redis;
using StackExchange.Redis;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace GSITestInfrastructure.GSIServices
{
    public class RedisValidationService
    {
        private GSIRealTimeCommand _realTimeCommandToValidate;
        private System.Timers.Timer _timer;
        private bool _redisEventWasFount;
        private bool _timerTimeHasElapsed;

        private RedisSubcriptionService _redisSubcriptionService = RedisSubcriptionService.Instance;

        public RedisValidationService(string redisCommandToValidate)
        {
            _realTimeCommandToValidate = GSIRealTimeCommand.FormJson(redisCommandToValidate);
            SetTimer();
        }

        public RedisValidationService(GSIRealTimeCommand redisCommandToValidate)
        {
            _realTimeCommandToValidate = redisCommandToValidate;
            SetTimer();
        }
        public bool Validate()
        {
            _redisSubcriptionService.RegisterMethod(OnNewDataEventArrived);
            _timer.Start();

            while (!_timerTimeHasElapsed)
            {
                if (_redisEventWasFount)
                {
                    Stop();
                    return true;
                }
                Thread.Sleep(20);
            }
            return _redisEventWasFount;
        }
        private void OnNewDataEventArrived(RedisChannel arg1, RedisValue arg2)
        {
            var incommingEvent = GSIRealTimeCommand.FormJson(arg2);
            if (incommingEvent == _realTimeCommandToValidate)
                _redisEventWasFount = true;
        }
        private void SetTimer()
        {
            var maxTimeToWait = double.Parse(ConfigurationManagerWrapper.GetParameterValue("TimeToWaitForRedisMessageMsec"));

            _timer = new System.Timers.Timer
            {
                Interval = maxTimeToWait,
                AutoReset = false
            };
            _timer.Elapsed += OnTimedEvent;
        }
        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            _timerTimeHasElapsed = true;
            Stop();
        }

        internal Task<bool> StartRedisValidationTask() => Task.Factory.StartNew(() => Validate());

        private void Stop()
        {
            _redisSubcriptionService.UnRegisterMethod(OnNewDataEventArrived);
            _timer.Close();
        }
    }
}
