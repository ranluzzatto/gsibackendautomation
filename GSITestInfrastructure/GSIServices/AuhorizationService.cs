﻿using GSITestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;

namespace GSITestInfrastructure.GSIServices
{
    public class AuhorizationService
    {
        private AuhorizationService() { }

        private static AuhorizationService instance = null;
        public static AuhorizationService Instance
        {
            get
            {
                if (instance == null)
                    instance = new AuhorizationService();
                return instance;
            }
        }
        public string GetUserTokenFromAppConfigCredentials()
        {
            var httpService = new HttpClientServices();
            var baseUri = ConfigurationManagerWrapper.GetParameterValue("BaseUri");

            string body = @"{""username"":""UserPlaceHolder"",""password"":""PasswordPlaceHolder""}"
                         .Replace("UserPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("GSIUser"))
                         .Replace("PasswordPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("GSIPassword"));

            var response = httpService.RunPostRequest(baseUri + "Admin/Login", body);
            var responseBody = JObject.Parse(response.Result);

            return responseBody["Body"]["AccountToken"].ToString();
        }

        public string GetUserToken(string userName, string password)
        {
            var httpService = new HttpClientServices();
            var baseUri = ConfigurationManagerWrapper.GetParameterValue("BaseUri");

            string body = @"{""username"":""UserPlaceHolder"",""password"":""PasswordPlaceHolder""}"
                         .Replace("UserPlaceHolder", userName)
                         .Replace("PasswordPlaceHolder", password);

            var response = httpService.RunPostRequest(baseUri + "Admin/Login", body);
            var responseBody = JObject.Parse(response.Result);

            return responseBody["Body"]["AccountToken"].ToString();
        }

    }
}
