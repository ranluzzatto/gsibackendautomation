﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Account
{
    public class ExchangeGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Account/Exchange";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
