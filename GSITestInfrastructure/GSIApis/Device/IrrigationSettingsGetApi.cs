﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class IrrigationSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/IrrigationSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
