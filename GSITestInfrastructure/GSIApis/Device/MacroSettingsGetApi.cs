﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class MacroSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/MacroSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
