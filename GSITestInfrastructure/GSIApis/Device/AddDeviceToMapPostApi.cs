﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AddDeviceToMapPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/AddDeviceToMap";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Map_Latitude"":0,""Map_Longitude"":0,""Weather_Latitude"":null,""Weather_Longitude"":null,""DifferentWeatherDataLocation"":false}";
    }
}
