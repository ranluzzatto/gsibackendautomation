﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FilterFlushLogsGetApi : GSIApiBase
    {
        public FilterFlushLogsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/FilterFlushLogs";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
