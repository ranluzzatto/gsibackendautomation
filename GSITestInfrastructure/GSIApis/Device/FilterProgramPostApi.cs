﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FilterProgramPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/FilterProgram";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":""Untitled Program"",""ProgramID"":1282572,""ProgramNumber"":8,""Priority"":0,""ConfigID"":190009,""Status"":""Active"",""FilterSetting"":{""FilterFlashID"":128799,""WaterForFlash"":0,""TimeForFlash"":60,""PipeFillDelay"":1,""Continuous"":2,""UseInputCondition"":false,""UnitOperTime"":30,""UnitWaitTime"":15,""OpenMaster"":false,""PauseWhenFault"":true,""InputFlash"":0,""MaxParallelRequests"":5,""StopIrrigation"":false,""Conditions"":null,""SustainOutput"":0,""SustainTime"":0,""FlushStandAlone"":false},""ValveInProgram"":[{""Name"":""S12"",""OrderNumber"":0,""OutputNumber"":12,""ProgramID"":1282572,""ValveID"":2510165}]}";
    }
}
