﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class SubElementConditionsGetApi : GSIApiBase
    {
        public SubElementConditionsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/SubElementConditions";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
