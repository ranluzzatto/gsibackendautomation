﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class OutputSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/OutputSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
