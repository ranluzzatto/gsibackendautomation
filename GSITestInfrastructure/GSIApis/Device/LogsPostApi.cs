﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class LogsPostApi : GSIApiBase
    {
        public LogsPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{UnitID}/Logs";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""TotalCount"":0,""Search"":"""",""PageSize"":10,""PageNumber"":1,""ColumnName"":""StartDate"",""OrderType"":""DESC""}";
    }
}
