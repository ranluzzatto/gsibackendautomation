﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class IrrigationsGetApi : GSIApiBase
    {
        public IrrigationsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/Irrigations";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
