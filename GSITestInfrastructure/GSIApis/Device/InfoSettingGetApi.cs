﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class InfoSettingGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{UnitID}/InfoSetting";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
