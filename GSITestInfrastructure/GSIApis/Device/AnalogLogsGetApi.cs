﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AnalogLogsGetApi : GSIApiBase
    {
        public AnalogLogsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/Analog/Logs";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
