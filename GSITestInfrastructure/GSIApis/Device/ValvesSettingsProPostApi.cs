﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ValvesSettingsProPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/ValvesSettings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"[{""Name"":""S3"",""OutputNumber"":3,""Status"":""Active"",""LowFlowDeviation"":30,""LowFlowDelay"":2,""HighFlowDeviation"":30,""HighFlowDelay"":2,""LineFillTime"":5,""IrrigrationArea"":null,""ValveColor"":-10115132,""PrecipitationRate"":null,""LastFlow"":5.6,""LastFlow_Date"":""0001-01-01T00:00:00"",""LastFlow_FlowTypeID"":11,""TypeID"":""Regular"",""FertilizerConnected"":true,""SetupNominalFlow"":0,""StopOnFertFailure"":false,""Kc"":0,""PlantTypeID"":null,""SubCategoryID"":null,""WaterFactor"":100,""ValveID"":2489023,""ValveAlarm"":0,""FlowViewTypeID"":11,""LineNum"":1,""WaterMeter"":1,""IgnoreWaterCounter"":true,""IgnoreMasterValve"":true,""WaterMeterType"":3,""PulseSize"":0,""FlowViewTypeWM"":0,""Condition"":[0,0,0],""CropTypeID"":null}]";
    }
}
