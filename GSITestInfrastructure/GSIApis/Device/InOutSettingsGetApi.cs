﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class InOutSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/inoutSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
