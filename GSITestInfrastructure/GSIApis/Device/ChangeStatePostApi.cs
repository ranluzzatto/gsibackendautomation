﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ChangeStatePostApi : GSIApiBase
    {
        public ChangeStatePostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/ChangeState";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
