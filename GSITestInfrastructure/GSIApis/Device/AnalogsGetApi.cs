﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AnalogsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/Analogs";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
