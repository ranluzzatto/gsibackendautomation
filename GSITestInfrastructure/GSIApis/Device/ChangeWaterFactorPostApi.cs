﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ChangeWaterFactorPostApi: GSIApiBase
    {
        public ChangeWaterFactorPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/1/ChangeWaterFactor";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""ControlUnitID"":[""unitIDPlaceHolder""]}"
                                .Replace("unitIDPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
