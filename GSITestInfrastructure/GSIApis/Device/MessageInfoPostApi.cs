﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class MessageInfoPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/MessageInfo";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""AlertID"": 0, ""Comment"": ""check1"", ""Noticet"": true}{""AlertID"": 1, ""Comment"": ""check2"", ""Noticet"": true}";
    }
}
