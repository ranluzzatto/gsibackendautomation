﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class MarkAsReadPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/MarkAsRead";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"[{""ID"": 0}]";
    }
}
