﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class LogExpansionIrrGetApi : GSIApiBase
    {
        public LogExpansionIrrGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/LogExpansionIrr";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
