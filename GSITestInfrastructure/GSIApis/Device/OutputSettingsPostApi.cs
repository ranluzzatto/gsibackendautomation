﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class OutputSettingsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/OutputSettings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{
                                  ""InputSetting"": [
                                    {
                                      ""ConditionInputNumber"": 0,
                                      ""OnDelay"": 0,
                                      ""OffDelay"": 0,
                                      ""Name"": ""string"",
                                      ""TypeID"": 0
                                    }
                                  ],
                                  ""OutputSetting"": [
                                    {
                                      ""Name"": ""O0"",
                                      ""TypeID"": 0,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
                                    {
                                      ""Name"": ""O1"",
                                      ""TypeID"": 1,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O2"",
                                      ""TypeID"": 2,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O3"",
                                      ""TypeID"": 3,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O4"",
                                      ""TypeID"": 4,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O5"",
                                      ""TypeID"": 5,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O6"",
                                      ""TypeID"": 6,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O7"",
                                      ""TypeID"": 7,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O8"",
                                      ""TypeID"": 8,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O9"",
                                      ""TypeID"": 9,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    },
{
                                      ""Name"": ""O10"",
                                      ""TypeID"": 10,
                                      ""UseRelay"": true,
                                      ""OutputComandDuration"": 0
                                    }
                                  ]
                                }";
    }
}
