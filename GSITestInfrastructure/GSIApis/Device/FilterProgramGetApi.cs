﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FilterProgramGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/FilterProgram";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
