﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ConditionsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/Conditions";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
