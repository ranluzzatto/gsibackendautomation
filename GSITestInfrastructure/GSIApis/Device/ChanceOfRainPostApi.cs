﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ChanceOfRainPostApi : GSIApiBase
    {
        public ChanceOfRainPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/ChanceOfRain";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
