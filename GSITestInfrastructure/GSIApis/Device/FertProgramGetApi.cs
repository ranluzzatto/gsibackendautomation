﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FertProgramGetApi : GSIApiBase
    {
        public FertProgramGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/Fertprogram";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
