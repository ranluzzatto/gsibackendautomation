﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class GenLogExpansionGetApi : GSIApiBase
    {
        public GenLogExpansionGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/GenLogExpansion";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
