﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AnalogSettingsGetApi : GSIApiBase
    {
        public AnalogSettingsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/Analog/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
