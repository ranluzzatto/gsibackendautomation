﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ConditionsSettingGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/Conditions/Setting";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
