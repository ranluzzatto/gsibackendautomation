﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FertProgramPostApi : GSIApiBase
    {
        public FertProgramPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/Fertprogram";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""FertCenterNum"":1,""ProgramNum"":1,""Name"":""Fert Program 1"",""ProgramID"":479862,""FertUnit"":2,""FertUnitName"":""ByDuration_Seconds"",""ECRequiered"":0,""PHRequiered"":0,""WaterCounterNum"":1,""Status"":true,""FertProgramSetup"":1,""fertValves"":[{""fertNum"":1,""value"":150},{""fertNum"":2,""value"":0},{""fertNum"":3,""value"":0},{""fertNum"":4,""value"":0}]}";
    }
}
