﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AlertSettingsPostApi : GSIApiBase
    {
        public AlertSettingsPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/AlertsSettings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""EnableEmailNotify"":true,""ResumeAfterFailure"":true,""AlertsAutoResetTime"":0,""AlertAutoCancel"":false,""AlertSetting"":[{""Device_Settings"":{""AlertCode"":0,""ValveStopIrrigation"":true,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":true,""ForceCommunication"":true,""SendMessage"":true,""AlertAutoCancel"":false}},{""Device_Settings"":{""AlertCode"":1,""ValveStopIrrigation"":true,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":true,""ForceCommunication"":true,""SendMessage"":true,""AlertAutoCancel"":false}},{""Device_Settings"":{""AlertCode"":2,""ValveStopIrrigation"":false,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":true,""ForceCommunication"":true,""SendMessage"":true,""AlertAutoCancel"":false}},{""Device_Settings"":{""AlertCode"":3,""ValveStopIrrigation"":false,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":true,""ForceCommunication"":false,""SendMessage"":true,""AlertAutoCancel"":true}},{""Device_Settings"":{""AlertCode"":4,""ValveStopIrrigation"":false,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":true,""ForceCommunication"":true,""SendMessage"":true,""AlertAutoCancel"":false}},{""Device_Settings"":{""AlertCode"":6,""ValveStopIrrigation"":false,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":false,""ForceCommunication"":false,""SendMessage"":true,""AlertAutoCancel"":false}},{""Device_Settings"":{""AlertCode"":16,""ValveStopIrrigation"":true,""SeriesStopIrrigation"":false,""RetryNextIrrigation"":false,""ForceCommunication"":false,""SendMessage"":true,""AlertAutoCancel"":false}}]}";
    }
}
