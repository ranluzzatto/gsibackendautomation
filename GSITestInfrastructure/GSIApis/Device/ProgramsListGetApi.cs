﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ProgramsListGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/ProgramsList";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
