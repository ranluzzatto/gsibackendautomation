﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AnalogSettingsPostApi : GSIApiBase
    {
        public AnalogSettingsPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/Analog/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SensorID"":127466,""SensorNumber"":1,""SensorType"":0,""Name"":""Analog 1"",""MinValueAlert"":0,""MaxValueAlert"":0,""LastValue"":0,""LastDateUpadte"":null,""LastDate"":null,""Calibration"":0,""MinValue"":0,""MaxValue"":0,""Accuracy"":0,""AlarmDelay"":0}";
    }
}
