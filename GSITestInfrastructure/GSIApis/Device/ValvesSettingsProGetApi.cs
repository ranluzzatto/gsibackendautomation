﻿using GSITestInfrastructure.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ValvesSettingsProGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/ValvesSettingsPro";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
