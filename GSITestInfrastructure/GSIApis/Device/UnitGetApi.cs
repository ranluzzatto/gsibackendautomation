﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class UnitGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
