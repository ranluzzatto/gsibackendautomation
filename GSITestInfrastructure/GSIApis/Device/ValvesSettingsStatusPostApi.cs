﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    internal class ValvesSettingsStatusPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/ValvesSettingsStatus";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
