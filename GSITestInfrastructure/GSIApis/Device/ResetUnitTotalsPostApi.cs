﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ResetUnitTotalsPostApi : GSIApiBase
    {
        public ResetUnitTotalsPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/ResetUnitTotals";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
