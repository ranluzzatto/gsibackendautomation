﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class LogExpansionIrrDWHGetApi : GSIApiBase
    {
        public LogExpansionIrrDWHGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/LogExpansionIrrDWH";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
