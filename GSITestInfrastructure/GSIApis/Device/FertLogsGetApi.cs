﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FertLogsGetApi : GSIApiBase
    {
        public FertLogsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/FertLogs";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
