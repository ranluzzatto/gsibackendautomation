﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class MacroSettingsPostApi : GSIApiBase
    {
        public MacroSettingsPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/MacroSettings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""ContractorID"":0,""RegionID"":0,""GeneralID"":0}";
    }
}
