﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ValvesSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/ValvesSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
