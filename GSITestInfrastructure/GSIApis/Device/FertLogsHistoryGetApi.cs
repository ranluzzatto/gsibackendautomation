﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FertLogsHistoryGetApi : GSIApiBase
    {
        public FertLogsHistoryGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/FertLogsHistory";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
