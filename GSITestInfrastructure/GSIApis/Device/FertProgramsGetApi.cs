﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FertProgramsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/Fertprograms";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
