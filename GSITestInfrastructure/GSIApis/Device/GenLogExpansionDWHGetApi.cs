﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class GenLogExpansionDWHGetApi : GSIApiBase
    {
        public GenLogExpansionDWHGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/GenLogExpansionDWH";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
