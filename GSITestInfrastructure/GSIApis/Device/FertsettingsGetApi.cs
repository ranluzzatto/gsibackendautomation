﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class FertSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/Fertsettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
