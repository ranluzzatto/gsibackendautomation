﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class SettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
