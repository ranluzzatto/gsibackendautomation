﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class RemoveDeviceFromMapPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/RemoveDeviceFromMap";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
