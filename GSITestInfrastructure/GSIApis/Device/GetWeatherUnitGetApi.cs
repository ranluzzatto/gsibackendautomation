﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class GetWeatherUnitGetApi : GSIApiBase
    {
        public GetWeatherUnitGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{unitID}/GetWathearUnit";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
