﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class IrrigationSettingsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/IrrigationSettings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Types"":{""WaterPulseTypes"":[{""TypeID"":1,""Name"":""LITER_AS_LITER_X1""},{""TypeID"":2,""Name"":""LITER_AS_M3_X10""},{""TypeID"":3,""Name"":""LITER_AS_M3_X100""},{""TypeID"":10,""Name"":""M3_AS_M3_X1""},{""TypeID"":11,""Name"":""M3_AS_M3_X10""},{""TypeID"":20,""Name"":""GAL_AS_GAL_X1""},{""TypeID"":21,""Name"":""THGAL""}],""AvailableInputs"":[{""TypeID"":1,""Name"":""1""},{""TypeID"":2,""Name"":""2""},{""TypeID"":3,""Name"":""3""},{""TypeID"":4,""Name"":""4""}],""Condition"":[],""AvailableOutput"":[],""LineStatus"":[{""TypeID"":1,""Name"":""reset""},{""TypeID"":2,""Name"":""auto""},{""TypeID"":3,""Name"":""pause""},{""TypeID"":4,""Name"":""alwaysOpen""}]},""PipeLineSetting"":[{""MainPipeSettings"":{""WaterFactor"":100,""PauseInputConditionNumber"":0,""IsLocalSequenceActive"":true,""ProgramsAsQueue"":false,""DSundayState"":false,""DMondayState"":false,""DTuesdayState"":false,""DWednesdayState"":false,""DThursdayState"":false,""DFridayState"":false,""DSaturdayState"":false,""OverlapTime"":5,""ProgramHFilter"":false,""UseMaster"":false,""ValveOpenningPattern"":""ValveFirst"",""ValveClosingPattern"":""MainFirst"",""ValveCloseDelay"":5,""ValveOpenDelay"":5,""MasterOutputNumber"":0,""MasterOutputName"":""M"",""MainPipNumber"":1,""ValveOverlapping"":true,""Define"":2,""Name"":null,""LogicCondition"":[0,0,0],""EnableAutoCancleAlarm"":false,""AlertsAutoResetTime"":0,""EnableFertilizer"":true,""UpdateDataCycle"":10,""EnableValveFertEventCycle"":0,""FlushingDataCycle"":10},""ValidDays"":{""DSundayState"":true,""DMondayState"":true,""DTuesdayState"":true,""DWednesdayState"":true,""DThursdayState"":true,""DFridayState"":true,""DSaturdayState"":true},""RainSensor"":{""IsEnabled"":false,""DaysStopper"":0,""NC"":false,""RainInputConditionNumber"":""None""}}],""WaterMeter"":[{""PulseSize"":100,""MeterType"":""Regular"",""IsEnabled"":true,""PulseTypeID"":""LITER_AS_M3_X100"",""FlowTypeID"":""M3PH"",""NoWaterPulseDelay"":10,""LeakageLimit"":0,""InputNumber"":""FS"",""WaterMeterType"":3,""PulseViewTypeID"":3,""WaterMeterNumber"":1,""LineNum"":1,""WaterMeterID"":245013,""EnableAutoCancleAlarm"":false,""AlertsAutoResetTime"":0,""ReleaseWaterLeakageFault"":5}]}";
    }
}
