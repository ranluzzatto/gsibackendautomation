﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AlertSettingsGetApi : GSIApiBase
    {
        public AlertSettingsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/unit/{unitID}/AlertsSettings";
        
        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
