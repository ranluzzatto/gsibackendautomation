﻿using GSITestInfrastructure.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class UpdateWaterQuantityPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{unitID}/UpdateWaterQuantity";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"[{""OldWaterMeterType"":2,""NewWaterMeterType"":3,""WaterMeterNumber"":3}]";
    }
}
