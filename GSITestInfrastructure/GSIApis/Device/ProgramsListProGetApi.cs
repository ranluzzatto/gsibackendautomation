﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class ProgramsListProGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/ProgramsListPro";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
