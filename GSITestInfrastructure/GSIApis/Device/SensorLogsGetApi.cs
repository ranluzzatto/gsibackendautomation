﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class SensorLogsGetApi : GSIApiBase
    {
        public SensorLogsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Unit/{ProUnitID}/SensorLogs";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
