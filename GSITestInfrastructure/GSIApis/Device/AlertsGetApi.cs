﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Device
{
    public class AlertsGetApi : GSIApiBase
    {
        public AlertsGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "api/Unit/{unitID}/Alerts";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
