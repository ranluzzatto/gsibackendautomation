﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ReleaseTodayFertPipePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ReleaseTodayFertPipe";

        internal override HttpRequestType RequestType => HttpRequestType.POST;


        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""PipeNum"": 4,""ProgramNum"": 0,""MinPause"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
