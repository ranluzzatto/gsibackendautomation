﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ReleaseTodayFertProgramPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ReleaseTodayFertProgram";

        internal override HttpRequestType RequestType => HttpRequestType.POST;


        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""PipeNum"": 1,""ProgramNum"": 0,""MinPause"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
