﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class CancelTodayFertProgramPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/CancelTodayFertProgram";

        internal override HttpRequestType RequestType => HttpRequestType.POST;


        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""PipeNum"": 1,""ProgramNum"": 6,""MinPause"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
