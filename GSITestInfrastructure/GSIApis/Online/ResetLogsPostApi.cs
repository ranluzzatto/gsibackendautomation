﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ResetLogsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ResetLogs";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""LogType"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
