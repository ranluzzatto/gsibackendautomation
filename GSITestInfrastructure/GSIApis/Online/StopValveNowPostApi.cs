﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class StopValveNowPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/StopValveNow";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"":""SerialNumberPlaceHolder"",""ValveNumber"":3,""TimeLeft"":70}"
            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
