﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ValveOpenNowProPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ValveOpenNowPRO";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"":""SerialNumberPlaceHolder"",""ValveNumber"":3,""Left"":70,""WaterUnit"":2}"
            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
