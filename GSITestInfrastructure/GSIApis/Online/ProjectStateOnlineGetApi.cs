﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ProjectStateOnlineGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/{projectID}/ProjectStateOnline";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
