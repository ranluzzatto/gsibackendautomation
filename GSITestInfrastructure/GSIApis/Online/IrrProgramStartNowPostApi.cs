﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class IrrProgramStartNowPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/IrrProgramStartNow";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""ProgramNumber"": 1,""StepNumber"":0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
