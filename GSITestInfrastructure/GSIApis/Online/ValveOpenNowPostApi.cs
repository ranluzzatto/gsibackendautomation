﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ValveOpenNowPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ValveOpenNow";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
       
        public string payload = @"{""SN"":""SerialNumberPlaceHolder"",""ValveNumber"":2,""TimeLeft"":70}"
            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}