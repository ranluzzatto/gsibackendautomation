﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ClearTestResultPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ClearTestResult";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""PipeNumber"": 2}"
                        .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
