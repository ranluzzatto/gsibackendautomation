﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class IrrProgramsCloseNowPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/IrrProgramsCloseNow";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""ProgramNumber"": 1,""StepNumber"":0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
