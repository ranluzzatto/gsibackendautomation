﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class RainOffDelayUpdatePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/RainOffDelayUpdate";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""RainyPipeNum"": 0,""RainOffDelay"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
