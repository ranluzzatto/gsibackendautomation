﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class SendNewVersionPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/SendNewVersion";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"[
                                    {
                                    ""SN"": ""0000000000115264"",
                                    ""Version"": ""6.5.1.103"",
                                    ""ConnectionStatus"": false,
                                    ""CurrentVersion"": ""6.5.1.103""
                                    }
                                ]";
    }
}
