﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class UnitStateOnlineGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/{unitID}/UnitStateOnline";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
