﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class PauseProgramPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/PauseProgram";

        internal override HttpRequestType RequestType => HttpRequestType.POST;


        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""PipeNum"": 1,""ProgramNum"": 3,""MinPause"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
