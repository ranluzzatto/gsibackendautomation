﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class UpdateTimeLeftPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/UpdateTimeLeft";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""ValveNumber"": 3,""ProgramNumber"": 101,""TimeLeft"": 120,
                                   ""Slot"": 0,""WaterUnit"": 2,""WaterMeterUnit"": 2}"
                           .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
