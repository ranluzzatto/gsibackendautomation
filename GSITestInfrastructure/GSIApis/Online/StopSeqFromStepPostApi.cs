﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class StopSeqFromStepPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/StopSeqFromStep";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{
                                ""SN"": ""SerialNumberPlaceHolder"",
                                ""ProgramNumber"": 1,
                                ""StepNumber"": 2
                                }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
