﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Online
{
    public class ReleaseProgramPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Online/ReleaseProgram";

        internal override HttpRequestType RequestType => HttpRequestType.POST;


        public string payload = @"{""SN"": ""SerialNumberPlaceHolder"",""PipeNum"": 1,""ProgramNum"": 4,""MinPause"": 0}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
