﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.IrrimaxApi
{
    public class IrrimaxApiKeyGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Irrimax/Unit/{unitID}/Irrimax-api-key";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
