﻿using GSITestInfrastructure.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSITestInfrastructure.GSIApis.IrrimaxApi
{
    public class EncryptedTokenGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Irrimax/Unit/{unitID}/Irrimax-api-key/encrypted-token";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
