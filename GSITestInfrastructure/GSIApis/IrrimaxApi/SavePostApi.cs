﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.IrrimaxApi
{
    public class SavePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Irrimax/Unit/{unitID}/Irrimax-api-key/Save";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""ApiKey"": ""Check""}";
    }
}
