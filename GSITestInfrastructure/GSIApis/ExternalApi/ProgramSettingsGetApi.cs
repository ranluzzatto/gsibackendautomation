﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.ExternalApi
{
    public class ProgramSettingsGetApi : GSIApiBase
    {
        public ProgramSettingsGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "API/External/{unitID}/{programID}/ProgramSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
