﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.ExternalApi
{
    public class GetConfigUnitPostApi : GSIApiBase
    {
        public GetConfigUnitPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "API/External/GetConfigUnit";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UserName"": ""UserNamePlaceHolder"",""Password"": ""PasswordPlaceHolder"",""SN"": ""SerialNumberPlaceHolder""}"
                                .Replace("UserNamePlaceHolder", ConfigurationManagerWrapper.GetParameterValue("GSIUser"))
                                .Replace("PasswordPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("GSIPassword"))
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));
    }
}
