﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.ExternalApi
{
    public class IrrigationLogsPostApi : GSIApiBase
    {
        public IrrigationLogsPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "API/External/IrrigationLogs";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Requetst"": {""TotalCount"": 0,""Search"": """",""PageSize"": 0,""PageNumber"": 0,""ColumnName"": """",""OrderType"": """"},""Body"": {""ControlUnitID"": [""unitIDPlaceHolder""]}}"
                                .Replace("unitIDPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
