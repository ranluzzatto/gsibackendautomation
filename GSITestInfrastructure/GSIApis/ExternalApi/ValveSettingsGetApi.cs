﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.ExternalApi
{
    public class ValveSettingsGetApi : GSIApiBase
    {
        public ValveSettingsGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "API/External/{unitID}/{ValveID}/ValveSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
