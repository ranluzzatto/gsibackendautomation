﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.ExternalApi
{
    public class KeyInfoGetApi : GSIApiBase
    {
        public KeyInfoGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "API/External/KeyInfo";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
