﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitProjectTreeGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/ProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
