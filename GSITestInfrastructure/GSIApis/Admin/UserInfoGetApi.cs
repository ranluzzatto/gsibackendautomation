﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserInfoGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/Info";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
