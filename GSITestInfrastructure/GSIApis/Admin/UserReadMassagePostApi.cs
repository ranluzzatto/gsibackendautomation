﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserReadMessagePostApi : GSIApiBase
    {
        public UserReadMessagePostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "Admin/User/UserReadMassage";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
