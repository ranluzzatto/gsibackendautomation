﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ChildSubUsersPostApi : GSIApiBase
    {
        public ChildSubUsersPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Admin/User/ChildeSubUsers";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Search"":null,""PageSize"":50,""PageNumber"":1,""ColumnName"":null,""OrderType"":null}";
    }
}
