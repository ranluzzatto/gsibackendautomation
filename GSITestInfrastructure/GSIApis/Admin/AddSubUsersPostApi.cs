﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class AddSubUsersPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/AddSubUsers";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UserID"":[0]}";
    }
}
