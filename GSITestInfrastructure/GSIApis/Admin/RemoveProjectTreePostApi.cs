﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class RemoveProjectTreePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/RemoveProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
