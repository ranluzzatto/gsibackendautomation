﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class RemoveSubUsersPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/RemoveSubUsers";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UserID"": [0]}";
    }
}
