﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ResetBasicRolePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{userID}/ResetBasicRole";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
