﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class TermsOfServicePostApi : GSIApiBase
    {
        public TermsOfServicePostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Admin/User/TermsOfService";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
