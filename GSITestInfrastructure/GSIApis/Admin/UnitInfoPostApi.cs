﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitInfoPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/Info";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":""DC-115881"",
                                ""Address"":null,
                                ""CustomTimeZoneID"":154,
                                ""CommTypeID"":2,
                                ""Description"":null,
                                ""FreeText1"":null,
                                ""FreeText2"":null,
                                ""FreeText3"":null,
                                ""FreeText4"":null,
                                ""IsGSIAg"":true,
                                ""UnpaidUnit"":true}";
    }
}
