﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserTypesGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/Types";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
