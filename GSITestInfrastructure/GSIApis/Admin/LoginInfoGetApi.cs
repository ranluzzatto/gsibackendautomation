﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class LoginInfoGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/LoginInfo";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
