﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitUpdateProjectTreePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/UpdateProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Add"":[{""logicAreaID"":427}],""Remove"":[{""logicAreaID"":427}]}";
    }
}
