﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitTypesGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/Types";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
