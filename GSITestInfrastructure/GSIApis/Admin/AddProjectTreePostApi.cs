﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class AddProjectTreePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/AddProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
