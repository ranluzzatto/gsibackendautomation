﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class AddProjectPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/AddProject";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":"""",""type"":1,""DefaultTimeZoneID"":154,""ParentAreaID"":null}";
    }
}
