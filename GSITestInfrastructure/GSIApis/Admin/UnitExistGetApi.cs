﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitExistGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/UnitExist/{SerialNumber}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
