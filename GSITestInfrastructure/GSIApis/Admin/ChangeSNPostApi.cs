﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ChangeSNPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitIDtoChange}/ChangeSN/{CommUnitID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
