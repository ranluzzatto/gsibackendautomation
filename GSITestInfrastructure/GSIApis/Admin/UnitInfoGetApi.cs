﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitInfoGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/Info";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
