﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitProjectTreeChildGetApi : GSIApiBase
    {
        public UnitProjectTreeChildGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/ProjectTreeChilde";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
