﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class SerchProjectTreeGetApi : GSIApiBase
    {
        public SerchProjectTreeGetApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Admin/Project/SerchProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
