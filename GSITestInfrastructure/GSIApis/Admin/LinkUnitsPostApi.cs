﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class LinkUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/{projectID}/LinkUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""ControlUnitID"":[581]}";
    }
}
