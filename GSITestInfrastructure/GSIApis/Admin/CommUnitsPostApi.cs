﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class CommUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/CommUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Requetst"":
                                    {""Search"":null,
                                    ""PageSize"":10,
                                    ""PageNumber"":1,
                                    ""ColumnName"":""SN"",
                                    ""OrderType"":""ASC""},
                                    ""Body"":
                                    {""LogicAreaID"":null,
                                    ""IncludeSubAreas"":true,
                                    ""IncludeUnlinkedUnits"":false,
                                    ""OnlySharedUnits"":false}}";
    }
}
