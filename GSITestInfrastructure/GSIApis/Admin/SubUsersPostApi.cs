﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class SubUsersPostApi : GSIApiBase
    {
        public SubUsersPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        public SubUsersPostApi(): base() { }

        protected override string ApiRoutePart => "Admin/User/SubUsers";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Search"":null,
                                   ""PageSize"":50,
                                   ""PageNumber"":1,
                                   ""ColumnName"":null,
                                   ""OrderType"":null}";
    }
}
