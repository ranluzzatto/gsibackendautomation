﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class PasswordPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/Password";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{
                                  ""UserName"": null,
                                  ""Password"": null}";
    }
}
