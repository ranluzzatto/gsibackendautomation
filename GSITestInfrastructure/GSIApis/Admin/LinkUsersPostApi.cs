﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class LinkUsersPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/{projectID}/LinkUsers";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UserID"":[0]}";
    }
}
