﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class PaymentManuallyPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/PaymentManually";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
