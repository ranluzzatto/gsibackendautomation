﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserInfoPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/Info";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""CustomRoleItems"":[],""AlertsSetting"":[],""UserInfo"":{""CreationDate"":""2022-02-17T13:52:25.133"",""UpdateDate"":""2022-02-20T14:15:42.827"",""IsEnabled"":true,""AlwaysValid"":true,""IsUserLink"":0,""GlobalizationZoneID"":154,""ValidFromDate"":""2022-02-17T13:52:25.133"",""ValidToDate"":""2022-02-17T13:52:25.133"",""Email"":""qaAutomationDemo@galcon.co.il"",""MobileNumber"":null,""OfficeNumber"":null,""UIFormatID"":0,""Flag_EnableSMSNotify"":true,""Flag_EnableEmailNotify"":true,""ConcurrentSessions"":0,""CultureCode"":""en-US"",""Werranty"":false,""WerrantyVersion"":0,""ReadSystemMessage"":false,""UserPaymentExpirationDate"":null,""UserPaymentStatus"":1,""DaysLefttoExpired"":null,""UnpaidUser"":false,""UserID"":584,""FirstName"":""QA"",""LastName"":""Automation Demo"",""RoleTypeID"":50,""ParentUserID"":485,""Username"":""qaAutomationDemo"",""ParentUserDisplayName"":null,""LastActivityDate"":""2022-02-20T14:15:37.81"",""LastLoginDate"":""2022-02-17T13:56:22.26"",""IsUserWatcher"":true}}";
    }
}
