﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnLinkUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/{projectID}/UnLinkUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""ControlUnitID"":[581]}";
    }
}
