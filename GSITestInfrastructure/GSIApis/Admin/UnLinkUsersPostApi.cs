﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnLinkUsersPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/{projectID}/UnLinkUsers";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UserID"":[0]}";
    }
}
