﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class DeleteUserDeleteApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/DeleteUser";

        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
