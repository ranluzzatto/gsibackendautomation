﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class DownloadVersionFilterPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/DownloadVersionFilter";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
