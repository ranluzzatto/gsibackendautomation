﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserProjectTreeChildGetApi : GSIApiBase
    {
        public UserProjectTreeChildGetApi(Dictionary<string, string> uriQueryParams)
                        : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/ProjectTreeChilde";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
