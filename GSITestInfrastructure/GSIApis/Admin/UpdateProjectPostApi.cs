﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UpdateProjectPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/{projectID}/UpdateProject";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":"""",""TimeZone"":154}";
    }
}
