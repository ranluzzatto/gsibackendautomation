﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserNotifyPostApi : GSIApiBase
    {
        public UserNotifyPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Admin/User/UserNotify";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
