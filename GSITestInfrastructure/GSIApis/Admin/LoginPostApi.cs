﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class LoginPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Login";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UserName"": ""qa_100"",  ""Password"": ""123456""}";
    }
}
