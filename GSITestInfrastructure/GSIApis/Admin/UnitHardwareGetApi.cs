﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitHardwareGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{SerialNumber}/UnitHardware";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
