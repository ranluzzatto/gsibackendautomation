﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserUpdateProjectTreePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{userID}/UpdateProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Add"":[{""logicAreaID"":0}],""Remove"":[{""logicAreaID"":0}]}";
    }
}
