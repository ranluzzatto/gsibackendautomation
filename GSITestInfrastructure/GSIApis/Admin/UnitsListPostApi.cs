﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UnitsListPostApi : GSIApiBase
    {
        public UnitsListPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Admin/Unit/UnitsList";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Search"":null,
                        ""PageSize"":10,
                        ""PageNumber"":1,
                        ""ColumnName"":""CreationDate"",
                        ""OrderType"":""DESC""}";
    }
}
