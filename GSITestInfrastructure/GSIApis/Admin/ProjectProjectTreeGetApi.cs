﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ProjectProjectTreeGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/ProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
