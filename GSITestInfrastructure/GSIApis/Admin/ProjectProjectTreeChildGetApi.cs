﻿using GSITestInfrastructure.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ProjectProjectTreeChildGetApi : GSIApiBase
    {
        public ProjectProjectTreeChildGetApi(Dictionary<string, string> uriQueryParams)
                        : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "Admin/Project/ProjectTreeChilde";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
