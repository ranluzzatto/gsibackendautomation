﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class AddUserPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/AddUser";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""FirstName"":""ADD"",
                                    ""LastName"":""USER TEST"",
                                    ""Email"":""addUserTest@galcon.co.il"",
                                    ""Password"":""123456"",
                                    ""RoleTypeID"":40,
                                    ""LinkToAreaID"":null,
                                    ""userName"":"""",
                                    ""GlobalizationZoneID"":154}";
    }
}
