﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class DeleteProjectPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Project/{GSIIDDemo}/DeleteProject";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""UnlinkSubUnits"":false,""ImmigrateAreasUp"":false}";
    }
}
