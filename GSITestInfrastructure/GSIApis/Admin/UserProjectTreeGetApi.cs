﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class UserProjectTreeGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/{userID}/ProjectTree";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
