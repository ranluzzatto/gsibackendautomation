﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class AppUserInfoGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/AppUserInfo";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
