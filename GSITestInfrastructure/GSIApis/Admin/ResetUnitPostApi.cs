﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ResetUnitPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/ResetUnit";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Reset_Acc"":true,""Reset_TempLogs"":true,""Reset_CacheLogs"":true}";
    }
}
