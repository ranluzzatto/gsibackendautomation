﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ChangeModelPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/ChangeModel/{modelID}";

    internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
