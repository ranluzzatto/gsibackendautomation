﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class AddUnitPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/AddUnit";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":""auto 114736"",
                                    ""CommTypeId"":2,
                                    ""CustomTimeZoneID"":154,
                                    ""SN"":""0000000000114736"",
                                    ""IsPro"":true,
                                    ""ModelId"":19,
                                    ""AttachToAreaID"":null}";

    }
}
