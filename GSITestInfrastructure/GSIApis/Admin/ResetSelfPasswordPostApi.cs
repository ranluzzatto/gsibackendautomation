﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class ResetSelfPasswordPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/User/ResetSelfPassword";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{   ""OldUserName"": ""string"",
                                      ""NewUserName"": ""string"",
                                      ""OldPassword"": ""string"",
                                      ""NewPassword"": ""string""}";
    }
}
