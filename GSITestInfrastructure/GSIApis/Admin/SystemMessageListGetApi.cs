﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class SystemMessageListGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/SystemMessageList";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
