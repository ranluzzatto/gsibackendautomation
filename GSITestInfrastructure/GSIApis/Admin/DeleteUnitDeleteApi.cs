﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class DeleteUnitDeleteApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Admin/Unit/{unitID}/DeleteUnit";

        internal override HttpRequestType RequestType => HttpRequestType.DETELE;
    }
}
