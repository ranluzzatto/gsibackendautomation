﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Admin
{
    public class RoleTypePostApi : GSIApiBase
    {
        public RoleTypePostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "Admin/User/{GSIIDDemo}/RoleType";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
