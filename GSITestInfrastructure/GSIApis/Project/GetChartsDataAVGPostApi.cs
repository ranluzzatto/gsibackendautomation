﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class GetChartsDataAVGPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/GetChartsDataAVG";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""code"":2,""UnitID1"":{""ControlUnitID"":[""SerialNumberPlaceHolder""]},""UnitID2"":{""ControlUnitID"":[""SerialNumberPlaceHolder""]},""Year"":2022}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
