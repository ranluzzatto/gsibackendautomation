﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UpdateFactorUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Macro/UpdateFactorUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""itemID"":[""SerialNumberPlaceHolder""],""actionValue"":100,""unitID"":[]}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
