﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    internal class NetWorkGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NetWork/{netWorkID}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
