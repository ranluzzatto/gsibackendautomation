﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class CalcToPayPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Payment/{projectId}/CalcToPay";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
