﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UserProjectsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/UserProjects";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
