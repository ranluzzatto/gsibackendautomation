﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ProjectSubUsersGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/ProjectSubUsers";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
