﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ChartsDataPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/ChartsData";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""code"":2,""UnitID1"":{""ControlUnitID"":[""SerialNumberPlaceHolder""]},""UnitID2"":{""ControlUnitID"":[""SerialNumberPlaceHolder""]},""Year"":2022}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
