﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class StatusUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/StatusUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
