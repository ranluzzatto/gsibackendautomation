﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class AlertsSettingsAllUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/AlertsSettingsAllUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
