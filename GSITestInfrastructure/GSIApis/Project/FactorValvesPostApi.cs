﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class FactorValvesPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/FactorValves";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
