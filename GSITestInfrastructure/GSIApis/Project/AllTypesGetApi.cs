﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class AllTypesGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/AllTypes";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
