﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class InfoPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectId}/Info";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
