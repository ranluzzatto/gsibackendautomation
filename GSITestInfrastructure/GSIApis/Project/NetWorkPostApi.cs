﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class NetWorkPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NetWork";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
