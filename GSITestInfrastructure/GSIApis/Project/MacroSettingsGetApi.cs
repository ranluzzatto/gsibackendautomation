﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class MacroSettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectId}/MacroSettings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
