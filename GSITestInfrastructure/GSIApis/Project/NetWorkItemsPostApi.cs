﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class NetWorkItemsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NetWorkItems";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
