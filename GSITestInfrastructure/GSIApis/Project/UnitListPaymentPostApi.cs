﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitListPaymentPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Payment/{projectId}/UnitListPayment";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
