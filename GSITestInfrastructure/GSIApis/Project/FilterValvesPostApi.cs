﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class FilterValvesPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/FilterValves";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
