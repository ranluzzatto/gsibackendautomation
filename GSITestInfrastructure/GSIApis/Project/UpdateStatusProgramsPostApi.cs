﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UpdateStatusProgramsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Macro/UpdateStatusPrograms";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""itemID"":[""SerialNumberPlaceHolder""],""actionValue"":1,""unitID"":[]}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
