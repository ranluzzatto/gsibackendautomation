﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ReportTypesGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/ReportTypes";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
