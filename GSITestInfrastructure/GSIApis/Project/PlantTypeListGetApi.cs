﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    internal class PlantTypeListGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/PlantTypeList";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
