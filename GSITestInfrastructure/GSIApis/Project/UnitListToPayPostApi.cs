﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitListToPayPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Payment/{projectId}/UnitListToPay";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
