﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class FilterProgramsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/FilterPrograms";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
