﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ResetTotalsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/ResetTotals";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
