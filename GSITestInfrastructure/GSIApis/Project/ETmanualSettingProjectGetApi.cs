﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ETmanualSettingProjectGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/ETmanualSettingProject";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
