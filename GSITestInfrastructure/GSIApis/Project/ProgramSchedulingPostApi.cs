﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ProgramSchedulingPostApi : GSIApiBase
    {
        public ProgramSchedulingPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Project/{projectID}/ProgramScheduling";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""TotalCount"": 0}";
    }
}
