﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitListGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/UnitList";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
