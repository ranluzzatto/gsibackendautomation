﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class PaymentUnitsDetailsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Payment/{projectId}/PaymentUnitsDetails";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
