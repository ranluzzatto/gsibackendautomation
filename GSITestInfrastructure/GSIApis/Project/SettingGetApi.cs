﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class SettingGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Setting";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
