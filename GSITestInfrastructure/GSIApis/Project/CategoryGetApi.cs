﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class CategoryGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Category";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
