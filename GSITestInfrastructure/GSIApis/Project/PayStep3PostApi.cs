﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class PayStep3PostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Payment/{projectId}/PayStep3";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
