﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class NetWorkListGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NetWorkList";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
