﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class MacroUpdateListPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/MacroUpdateList/{categoryTypeID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
