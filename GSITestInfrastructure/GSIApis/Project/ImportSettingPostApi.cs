﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ImportSettingPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Category/ImportSetting/{categoryTypeID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
