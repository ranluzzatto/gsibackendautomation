﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class AlertsSettingsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/AlertsSettings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
