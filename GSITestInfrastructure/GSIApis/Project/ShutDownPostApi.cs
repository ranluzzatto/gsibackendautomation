﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ShutDownPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/ShutDown/{netWorkID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
