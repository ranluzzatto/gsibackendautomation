﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitDailyAccumulatorsAndFertPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Unit_DailyAccumulatorsAndFert";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Requetst"":{""StartTicks"":1640995200000,""EndTicks"":1643587200000},""Body"":{""ControlUnitID"":[""SerialNumberPlaceHolder""]}}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
