﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class NetWorkPutApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NetWork";

        internal override HttpRequestType RequestType => HttpRequestType.PUT;
    }
}
