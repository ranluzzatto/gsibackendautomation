﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class PayStep1PostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Payment/{projectId}/PayStep1";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
