﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ValveDailyExportPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Valve_Daily_export";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
