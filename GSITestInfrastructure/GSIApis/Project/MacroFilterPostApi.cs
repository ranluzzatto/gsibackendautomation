﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class MacroFilterPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectId}/MacroFilter";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{ ""ObjectType"":1, ""Status"":null, ""ContractorID"":null, ""RegionID"":null, ""GeneralID"":null, ""ClassificationID"":null, ""CropTypeID"":null, ""IsFilter"":false }";
    }
}
