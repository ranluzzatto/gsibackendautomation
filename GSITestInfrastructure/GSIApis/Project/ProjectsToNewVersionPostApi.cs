﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class ProjectsToNewVersionPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectId}/ProjectsToNewVersion";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Search"":"""",""TotalCount"":10,""PageSize"":10,""PageNumber"":1}";
    }
}
