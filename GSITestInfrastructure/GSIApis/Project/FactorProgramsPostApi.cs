﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class FactorProgramsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/FactorPrograms";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
