﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class LogOutPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/LogOut";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
