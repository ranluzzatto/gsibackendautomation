﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NetWork/Units";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
