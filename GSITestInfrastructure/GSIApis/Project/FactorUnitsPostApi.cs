﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class FactorUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/FactorUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
