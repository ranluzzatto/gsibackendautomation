﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class CashIrrigationPostApi : GSIApiBase
    {
        public CashIrrigationPostApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Project/{projectID}/CashIrrigation";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Requetst"":{""TotalCount"":0,""Search"":"""",""PageSize"":10,""PageNumber"":1,""ColumnName"":""RecordDate"",""OrderType"":""DESC""},""Body"":{""ControlUnitID"":[""SerialNumberPlaceHolder""]}}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
