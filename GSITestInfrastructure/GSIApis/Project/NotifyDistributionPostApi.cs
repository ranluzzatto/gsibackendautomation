﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class NotifyDistributionPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/NotifyDistribution";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
