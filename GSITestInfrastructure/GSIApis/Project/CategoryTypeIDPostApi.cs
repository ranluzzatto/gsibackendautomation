﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class CategoryTypeIDPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Category/{categoryTypeID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
