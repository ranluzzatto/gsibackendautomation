﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UpdateTimePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/UpdateTime";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
