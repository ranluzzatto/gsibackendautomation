﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UpdateFactorValvesPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Macro/UpdateFactorValves";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""itemID"":[""ProgramIDPlaceHolder""],""actionValue"":100,""unitID"":[""SerialNumberPlaceHolder""]}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID")).Replace("ProgramIDPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("programID"));
    }
}
