﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class FilterUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/FilterUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
