﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitDailyExportPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Unit_Daily_export";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
