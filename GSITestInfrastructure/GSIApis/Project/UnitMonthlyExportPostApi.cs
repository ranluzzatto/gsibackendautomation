﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UnitMonthlyExportPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/{projectID}/Unit_Montly_export";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
