﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class StatusProgramsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Category/StatusPrograms";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
