﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Project
{
    public class UpdateStatusUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Project/Macro/UpdateStatusUnits";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""itemID"":[""SerialNumberPlaceHolder""],""actionValue"":1,""unitID"":[]}"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("unitID"));
    }
}
