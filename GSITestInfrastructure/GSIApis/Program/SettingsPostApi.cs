﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Program
{
    public class SettingsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Program/{unitID}/{programID}/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":""ProgramName"",""ProgramID"":""ProgramIDPlaceHolder"",""ProgramNumber"":1,""Priority"":0,""ConfigID"":190009,""IsFertilizerON"":false,""PulseViewTypeID"":3,""UnitPulseViewTypeID"":2,""ProgramSetup"":1,""NextIrrigationDay"":255,""NextIrrigationTime"":null,""LineNum"":1,""MainSecondaryValveID"":null,""ProgramStatus"":5,""ProgramWaterUnit"":null,""ClassificationTypeID"":null,""Status"":""Active"",""ProgramType"":""Duration"",""FertExecutionType"":""ByDuration_Seconds"",""WaterFactor"":100,""PlantTypeID"":null,""WeeklyDaysProgram"":{""DSundayState"":true,""DMondayState"":true,""DTuesdayState"":true,""DWednesdayState"":true,""DThursdayState"":true,""DFridayState"":true,""DSaturdayState"":true},""ExecutionHours"":[0],""HoursExecution"":""Fixed"",""DaysExecutionType"":""Weekly"",""CyclicDayProgram"":{""StartDate"":1631860011890,""DaysInterval"":1},""HourlyCycle"":{""HourlyCyclesPerDay"":3,""HourlyCyclesStartTime"":3600,""HourlyCycleTime"":7200,""HourlyCyclesStopTime"":0},""Advanced"":{""FinalStartDateLimit"":null,""FinalEndDateLimit"":null,""FinalStartHoursLimit"":0,""FinalStopHoursLimit"":null,""RunAndFinish"":0,""RunWhenActive"":0,""TerminateProgram"":0,""PauseWhenActive"":0,""MaxWaitForStartCond"":60},""ValveInProgram"":[{""ProgramID"":1282565,""OutputNumber"":1,""OrderNumber"":1,""ID"":1202834,""WaterQuantity"":2,""FertQuant"":0,""FertTime"":0,""Name"":""S1Check"",""WaterBefore"":0,""WaterAfter"":0,""WaterDuration"":60,""WaterPrecipitation"":2,""SecondaryValveID"":0,""ValveID"":2274119,""ValveType"":0,""ET"":0,""IsDeleted"":false,""FertProgram"":0,""PulseViewTypeID"":0,""IgnoreWaterCounter"":false,""IgnoreMasterValve"":false,""AuxiliaryOutput"":false,""LineNum"":0,""DecimalPlaces"":2}],""UnitPulseType"":{""PulseTypeViewID"":2,""PulseTypeID"":0,""Name"":""Liter (m3 #.##)"",""IsMetric"":true,""IsDefault"":true,""ViewFormat"":""1.23 m3"",""DecimalPlaces"":2},""ProgramPulseType"":{""PulseTypeViewID"":3,""PulseTypeID"":0,""Name"":""Liter (m3 #.#)"",""IsMetric"":true,""IsDefault"":true,""ViewFormat"":""1.2 m3"",""DecimalPlaces"":1},""Conditions"":null}"
                                .Replace("ProgramIDPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("programID"));
    }
}
