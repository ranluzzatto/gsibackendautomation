﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Program
{
    public class SettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Program/{unitID}/{programID}/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
