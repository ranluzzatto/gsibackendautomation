﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Program
{
    public class ScheduledGetApi : GSIApiBase
    {
        public ScheduledGetApi(Dictionary<string, string> uriQueryParams)
            : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "api/Program/{unitID}/Scheduled";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
