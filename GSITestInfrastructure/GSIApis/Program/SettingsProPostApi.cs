﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Program
{
    public class SettingsProPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Program/{ProUnitID}/{ProProgramID}/SettingsPro";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""Name"":""changedByMe"",""ProgramID"":""ProProgramIDPlaceHolder"",""ProgramNumber"":13,""Priority"":0,""ConfigID"":199401,""IsFertilizerON"":true,""PulseViewTypeID"":0,""UnitPulseViewTypeID"":0,""ProgramSetup"":2,""NextIrrigationDay"":1,""NextIrrigationTime"":1.01,""LineNum"":2,""MainSecondaryValveID"":null,""ProgramStatus"":1,""ProgramWaterUnit"":null,""ClassificationTypeID"":null,""Status"":""Active"",""ProgramType"":""Duration"",""FertExecutionType"":""NotDefine"",""WaterFactor"":100,""PlantTypeID"":null,""WeeklyDaysProgram"":{""DSundayState"":true,""DMondayState"":true,""DTuesdayState"":true,""DWednesdayState"":true,""DThursdayState"":true,""DFridayState"":true,""DSaturdayState"":true},""ExecutionHours"":[3660],""HoursExecution"":""Fixed"",""DaysExecutionType"":""Weekly"",""CyclicDayProgram"":{""StartDate"":1636152447433,""DaysInterval"":1},""HourlyCycle"":{""HourlyCyclesPerDay"":0,""HourlyCyclesStartTime"":0,""HourlyCycleTime"":0,""HourlyCyclesStopTime"":0},""Advanced"":{""FinalStartDateLimit"":null,""FinalEndDateLimit"":null,""FinalStartHoursLimit"":0,""FinalStopHoursLimit"":null,""RunAndFinish"":0,""RunWhenActive"":0,""TerminateProgram"":0,""PauseWhenActive"":0,""MaxWaitForStartCond"":0},""ValveInProgram"":[],""UnitPulseType"":null,""ProgramPulseType"":null,""Conditions"":[0,0,0,0]}"
           .Replace("ProProgramIDPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("ProProgramID"));
    }
}

