﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Program
{
    public class SettingsProGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Program/{ProUnitID}/{ProProgramID}/SettingsPro";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
