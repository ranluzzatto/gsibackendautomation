﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Valve
{
    public class SettingsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Valve/{unitID}/{ValveNumber}/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{
                                  ""Name"": """",
                                  ""OutputNumber"": 1,
                                  ""Status"": ""Active"",
                                  ""LowFlowDeviation"": 0,
                                  ""LowFlowDelay"": 0,
                                  ""HighFlowDeviation"": 0,
                                  ""HighFlowDelay"": 0,
                                  ""LineFillTime"": 0,
                                  ""IrrigrationArea"": 0,
                                  ""ValveColor"": 0,
                                  ""PrecipitationRate"": 0,
                                  ""LastFlow"": 0,
                                  ""LastFlow_Date"": ""2022-01-10T09:54:15.471Z"",
                                  ""LastFlow_FlowTypeID"": 0,
                                  ""TypeID"": 0,
                                  ""FertilizerConnected"": true,
                                  ""SetupNominalFlow"": 0,
                                  ""StopOnFertFailure"": true,
                                  ""Kc"": 0,
                                  ""PlantTypeID"": 0,
                                  ""SubCategoryID"": 0,
                                  ""WaterFactor"": 0,
                                  ""ValveID"": 0,
                                  ""ValveAlarm"": 0,
                                  ""ValveSetup"": 0,
                                  ""FlowViewTypeID"": 0,
                                  ""IgnoreWaterCounter"": true,
                                  ""IgnoreMasterValve"": true,
                                  ""AuxiliaryOutput"": true,
                                  ""CropTypeID"": 0
                                }";
    }
}
