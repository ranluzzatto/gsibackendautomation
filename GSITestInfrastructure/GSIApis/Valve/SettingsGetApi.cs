﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Valve
{
    public class SettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "api/Valve/{unitID}/{ValveNumber}/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
