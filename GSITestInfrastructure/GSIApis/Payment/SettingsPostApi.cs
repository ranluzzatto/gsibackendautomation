﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class SettingsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
