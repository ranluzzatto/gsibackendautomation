﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class PaidByCouponPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Paid-By-Coupon";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"{""PaymentActionType"":1,""PaymentType"":0,""Coin"":1,""YearCount"":3,""TotalPrice"":0,""CouponID"":3011,""UnitsPayments"":[{""UnitID"":631,""CurrentExpirationDate"":""2022-01-12T12:39:00.29"",""Price"":0,""FullPrice"":1050}],""UserPayments"":[]}";
    }
}
