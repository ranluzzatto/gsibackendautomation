﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class EditLicensePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/EditLicense/{licenseID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
