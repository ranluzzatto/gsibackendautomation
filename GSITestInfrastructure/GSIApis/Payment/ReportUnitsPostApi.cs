﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class ReportUnitsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Report-Users";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
