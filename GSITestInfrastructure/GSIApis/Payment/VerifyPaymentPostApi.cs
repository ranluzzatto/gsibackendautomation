﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class VerifyPaymentPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/VerifyPayment";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string payload = @"{
                                      """"TotalCount"""":0,
                                        """"PageNumber"""":1
                                        ,""""PageSize"""":50,
                                        """"Search"""":"""""""",
                                        """"ColumnName"""":""""ExpirationDate"""",
                                        """"OrderType"""":""""ASC""""
                                    }";
    }
}
