﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class GraphsUsersPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Graphs-Users";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
