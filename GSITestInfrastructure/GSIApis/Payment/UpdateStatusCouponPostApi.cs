﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class UpdateStatusCouponPostApi : GSIApiBase
    {
        public UpdateStatusCouponPostApi(Dictionary<string, string> uriQueryParams)
         : base(uriQueryParams, null) { }

        protected override string ApiRoutePart => "Payment/{CouponID}/UpdateStatus";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
