﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class UpdateStatusLicensePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "payment/{licenseID}/UpdateStatus";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
