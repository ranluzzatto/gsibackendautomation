﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class DownloadPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Download";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
