﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class Step1PostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Step1";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        
        public string payload = @"{""PaymentActionType"":1,""PaymentType"":0,""Coin"":1,""YearCount"":3,""TotalPrice"":945,""UnitsPayments"":[{""UnitID"":537,""CurrentExpirationDate"":""2021-12-24T00:00:00"",""Price"":945,""FullPrice"":1050}],""UserPayments"":[]}";
    }
}
