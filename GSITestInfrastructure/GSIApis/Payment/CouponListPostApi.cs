﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class CouponListPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/CouponList";

        internal override HttpRequestType RequestType => HttpRequestType.POST;

        public string payload = @"[{""Discount"":5},{""Discount"":10},{""Discount"":15},{""Discount"":20},{""Discount"":25},{""Discount"":30},{""Discount"":35},{""Discount"":40},{""Discount"":45},{""Discount"":50},{""Discount"":55},{""Discount"":60},{""Discount"":65},{""Discount"":70},{""Discount"":75},{""Discount"":80},{""Discount"":85},{""Discount"":90},{""Discount"":95},{""Discount"":100}][{""Discount"":5},{""Discount"":10},{""Discount"":15},{""Discount"":20},{""Discount"":25},{""Discount"":30},{""Discount"":35},{""Discount"":40},{""Discount"":45},{""Discount"":50},{""Discount"":55},{""Discount"":60},{""Discount"":65},{""Discount"":70},{""Discount"":75},{""Discount"":80},{""Discount"":85},{""Discount"":90},{""Discount"":95},{""Discount"":100}]";
    }
}
