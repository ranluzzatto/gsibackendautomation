﻿using GSITestInfrastructure.Infrastructure;
using System.Collections.Generic;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class PaymentListPostApi : GSIApiBase
    {
        public PaymentListPostApi(Dictionary<string, string> uriQueryParams)
           : base(uriQueryParams, null) { }
        protected override string ApiRoutePart => "Payment/PaymentsList";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string payload = @"{""TotalCount"":0,""PageNumber"":1,""PageSize"":50,""Search"":"""",""ColumnName"":""ExpirationDate"",""OrderType"":""ASC""}";
        



    }
}

