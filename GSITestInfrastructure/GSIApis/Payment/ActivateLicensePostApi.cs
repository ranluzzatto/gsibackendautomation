﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class ActivateLicensePostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/ActivateLicense";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
