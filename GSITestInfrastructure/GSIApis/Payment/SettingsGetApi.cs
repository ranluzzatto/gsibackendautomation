﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class SettingsGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Settings";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
