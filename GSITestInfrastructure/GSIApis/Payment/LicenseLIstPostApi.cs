﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class LicenseLIstPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/LicenseList";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
    }
}
