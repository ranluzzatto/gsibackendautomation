﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class PricesGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Prices";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
    }
}
