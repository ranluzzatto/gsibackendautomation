﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class CouponsPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Coupons/{CouponID}";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string payload = @"{
                                      ""ControllerCount"": 2,
                                      ""UserCount"": 0,
                                      ""YearCount"": 3 ,
                                      ""ReceivedbyID"": 1
                                    }";
                                   
        //.Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

    }
}
