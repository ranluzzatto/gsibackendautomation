﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class ManualPostApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/Manual";

        internal override HttpRequestType RequestType => HttpRequestType.POST;
        public string payload = @"{""PaymentType"":0,""Coin"":1,""YearCount"":3,""TotalPrice"":945,""ManualPaymentType"":0,""TotalAlreadyPaidSum"":945,""PaymentCodeOfErp"":""e3434"",""UnitsPayments"":[{""UnitID"":599,""CurrentExpirationDate"":""2021-12-24T00:00:00"",""Price"":945,""FullPrice"":1050}],""UsersPayments"":[]}
                     }";
    }
}
