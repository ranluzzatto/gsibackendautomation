﻿using GSITestInfrastructure.Infrastructure;

namespace GSITestInfrastructure.GSIApis.Payment
{
    public class ActivateCouponGetApi : GSIApiBase
    {
        protected override string ApiRoutePart => "Payment/ActivateCoupon/{CouponCode}";

        internal override HttpRequestType RequestType => HttpRequestType.GET;
        
       
    }
}
