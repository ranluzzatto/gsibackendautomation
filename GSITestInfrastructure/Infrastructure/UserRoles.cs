﻿namespace GSITestInfrastructure.Infrastructure
{
    public enum UserRoles
    {
        Payment = 20,
        Project_Viewer_User = 30,
        Project_User = 40,
        Project_Admin = 50,
        Multi_Projects_Admin = 100,
        Production_Manager = 200,
        Admin = 500,
        System_Admin = 1000,
        Super_System_Admin = 2000,
        Debbuger_Website_Administrator = 10000
    }
}
