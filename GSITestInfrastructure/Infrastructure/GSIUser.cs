﻿namespace GSITestInfrastructure.Infrastructure
{
    public class GSIUser
    {
        public UserRoles Role { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
