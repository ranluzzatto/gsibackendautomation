﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace GSITestInfrastructure.Infrastructure
{
    public static class GSIApiUtils
    {
        private static Regex _paramRegex = new Regex("{([^}]*)}");

        internal static bool HasUriNonQueryParameters(string uri)
        {//example:/config/{configID}/alarms/alarms-to-send
            return _paramRegex.IsMatch(uri)
                && _paramRegex.Matches(uri).FirstOrDefault(x => ConfigurationManagerWrapper.GetAllParametersList()
                .AllKeys.Select(y => y.ToUpper())
                .Contains(x.Value.Substring(1, x.Length - 2).ToUpper())) != null;
            //added .ToUpper() because the api sometimes uses {configId} and sometimes {ConfigID}
        }
        internal static string ReplaceParametersWithValues(string apiRoutePart)
        {
            var allConfigurationParameters = ConfigurationManagerWrapper.GetAllParametersList();

            foreach (KeyValueConfigurationElement param in allConfigurationParameters)
                if (apiRoutePart.Contains(param.Key, System.StringComparison.CurrentCultureIgnoreCase))
                    apiRoutePart = apiRoutePart.Replace("{" + param.Key + "}", param.Value, System.StringComparison.CurrentCultureIgnoreCase);

            return apiRoutePart;
        }
        internal static bool HasUriNonQueryLocalParameters(string uri, Dictionary<string, string> uriLocalParams)
        {//example:/app-settings/{key}
            return _paramRegex.IsMatch(uri)
                && uriLocalParams.Keys.FirstOrDefault(key => uri.Contains(key)) != null;
        }
        internal static string ReplaceLocalParametersWithValues(string apiRoutePart, Dictionary<string, string> uriLocalParams)
        {
            foreach (var param in uriLocalParams)
                apiRoutePart = apiRoutePart.Replace("{" + param.Key + "}", param.Value);

            return apiRoutePart;
        }
    }
}
