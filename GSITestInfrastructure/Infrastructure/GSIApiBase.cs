﻿using GSITestInfrastructure.GSIServices;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace GSITestInfrastructure.Infrastructure
{
    public abstract class GSIApiBase
    {
        private readonly Dictionary<string, string> _uriQueryParameters;
        private readonly Dictionary<string, string> _uriLocalParameters;
        private readonly string _baseUri = ConfigurationManagerWrapper.GetParameterValue("BaseUri");
        private static bool _initilized;
        private Task<bool> _redisValidationTask;
        private Task<bool> _readCnfRedisValidationTask;
        private static readonly List<GSIUser> _allUsers = new();
        private string _nonDefaultToken;
        private static readonly string _readCnfRedisEvent = @"{
                                                      ""SN"": ""SerialNumberPlaceHolder"",
                                                      ""Type"": 2,
                                                      ""CommandCode"": 0,
                                                      ""ComponentTypeCode"": 0,
                                                      ""CNFCode"": 0,
                                                      ""Number"": 0,
                                                      ""Numbers"": null,
                                                      ""StatusCode"": 0,
                                                      ""RealTimeValue"": null,
                                                      ""FirmwareId"": 0,
                                                      ""LogType"": 0
                                                    }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

        public GSIApiBase(Dictionary<string, string> uriQueryParams = null,
            Dictionary<string, string> uriLocalParams = null)
        {
            if (uriQueryParams != null)
                _uriQueryParameters = new Dictionary<string, string>(uriQueryParams);

            if (uriLocalParams != null)
                _uriLocalParameters = new Dictionary<string, string>(uriLocalParams);

            if (!_initilized)//static initilizations
            {
                Token = AuhorizationService.Instance.GetUserTokenFromAppConfigCredentials();
                var users = ConfigurationManagerWrapper.GetGSIUsers();

                foreach (var user in users)
                {
                    user.Token = AuhorizationService.Instance.GetUserToken(user.UserName, user.Password);
                    _allUsers.Add(user);
                }
                _initilized = true;
            }
        }
        public HttpStatusCode ResponseStatusCode { get; private set; }
        public HttpResponseMessage Response { get; private set; }
        public JObject ResponseBody { get; private set; }
        public string RequestPayload { get; set; }
        public static string Token { get; private set; }

        private bool? _redisValidationResult;
        public bool? RedisValidationResult
        {
            get
            {
                if (!_redisValidationResult.HasValue) throw new Exception("Validation result was not assigned, " +
                    "make sure that:\n 1.The Api was runed via Run() method\n " +
                     "2.you called AddRedisEventToValidate() with an expected redis event for comparison");
                return _redisValidationResult;
            }
            private set => _redisValidationResult = value;
        }
        private bool? _readCnfRedisEventValidationResult;
        public bool? ReadCnfRedisEventValidationResult
        {
            get
            {
                if (!_readCnfRedisEventValidationResult.HasValue)
                    throw new Exception("For validating read cnf validation result, you need to set 'ValidateReadCnfRedisEvent' to 'true' before calling 'Run()'");
                return _readCnfRedisEventValidationResult;
            }
            private set => _readCnfRedisEventValidationResult = value;
        }
        public bool ValidateReadCnfRedisEvent { get; set; }
        private string ExpectedRedisEvent { get; set; }
        protected abstract string ApiRoutePart { get; }//specific route part for each api
        internal Uri ActualUri => GetURI();
        internal abstract HttpRequestType RequestType { get; }
        private HttpClientServices HttpClientServices { get; } = new();
        /// <summary>
        /// Add redis event to compare after the API calls Run()
        /// </summary>
        /// <param name="expectedRedisEvent"></param>
        public void AddRedisEventToValidate(string expectedRedisEvent) => ExpectedRedisEvent = expectedRedisEvent;
        private Uri GetURI()
        {
            var apiRoutePart = ApiRoutePart;

            //from global parameters, example: /config/{configID}/alarms/alarms-to-send
            if (GSIApiUtils.HasUriNonQueryParameters(apiRoutePart))
                apiRoutePart = GSIApiUtils.ReplaceParametersWithValues(apiRoutePart);

            //from local parameters, example: /app-settings/{key}
            if (GSIApiUtils.HasUriNonQueryLocalParameters(apiRoutePart, _uriLocalParameters))
                apiRoutePart = GSIApiUtils.ReplaceLocalParametersWithValues(apiRoutePart, _uriLocalParameters);

            var builder = new UriBuilder($"{_baseUri}" + apiRoutePart);

            //query parametes, example:user/exists?userName=test
            return _uriQueryParameters == null ? builder.Uri
                : new Uri(QueryHelpers.AddQueryString(builder.Uri.ToString(), _uriQueryParameters));
        }
        public void Run()
        {
            var shouldValidateRedisDataEvent = !string.IsNullOrEmpty(ExpectedRedisEvent);

            if (shouldValidateRedisDataEvent)
            {
                var validator = new RedisValidationService(ExpectedRedisEvent);
                _redisValidationTask = validator.StartRedisValidationTask();
            }

            if (ValidateReadCnfRedisEvent)
            {
                var readCnfValidator = new RedisValidationService(_readCnfRedisEvent);
                _readCnfRedisValidationTask = readCnfValidator.StartRedisValidationTask();
            }

            Response = HttpClientServices.RunRequest(this, _nonDefaultToken);
            ResponseStatusCode = Response.StatusCode;
            if (Response.Content.Headers.ContentLength != 0)
            {
                ResponseBody = JObject.Parse(Response.Content.ReadAsStringAsync().Result);
            }

            if (shouldValidateRedisDataEvent)
                RedisValidationResult = _redisValidationTask.Result;
            //read cnf validation 
            if (ValidateReadCnfRedisEvent)
                ReadCnfRedisEventValidationResult = _readCnfRedisValidationTask.Result;
        }
        public GSIApiBase AsUser(string userName)
        {
            var user = _allUsers.FirstOrDefault(user => user.UserName == userName);
            if (user != null)
                _nonDefaultToken = user.Token;
            else throw new Exception(string.Format("User with email:'{0}' was not found, please make sure the user is defined in app.config", userName));
            return this;
        }

        public GSIApiBase AsUser(UserRoles role)
        {
            var user = _allUsers.FirstOrDefault(user => user.Role == role);
            if (user != null)
                _nonDefaultToken = user.Token;
            else throw new Exception(string.Format("User with role:'{0}' was not found, please make sure the user is defined in app.config", role));
            return this;
        }

    }
}
