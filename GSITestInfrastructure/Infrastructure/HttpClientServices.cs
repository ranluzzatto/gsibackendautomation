﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GSITestInfrastructure.Infrastructure
{
    public class HttpClientServices
    {
        private readonly HttpClient _restClient = new();
        internal HttpResponseMessage RunRequest(GSIApiBase gsiApi, string otherToken)
        {
            _restClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                string.IsNullOrEmpty(otherToken) ? GSIApiBase.Token : otherToken);

            switch (gsiApi.RequestType)
            {
                case HttpRequestType.GET:
                    return RunGetRequest(gsiApi);

                case HttpRequestType.POST:
                    return RunPostRequest(gsiApi);

                case HttpRequestType.PUT:
                    return RunPutRequest(gsiApi);

                case HttpRequestType.DETELE:
                    return RunDeleteRequest(gsiApi);

                default:
                    return null;
            }
        }

        private HttpResponseMessage RunPutRequest(GSIApiBase gsiApi)
        {
            if (string.IsNullOrEmpty(gsiApi.RequestPayload))
                throw new Exception("Content cannot be empty for PUT request");

            HttpContent c = new StringContent(gsiApi.RequestPayload, Encoding.UTF8, "application/json");
            var response = _restClient.PutAsync(gsiApi.ActualUri, c);

            return response.Result;
        }

        private HttpResponseMessage RunDeleteRequest(GSIApiBase gsiApi)
        {
            var response = _restClient.DeleteAsync(gsiApi.ActualUri);

            return response.Result;
        }

        private HttpResponseMessage RunGetRequest(GSIApiBase gsiApi)
        {
            var response = _restClient.GetAsync(gsiApi.ActualUri);

            return response.Result;
        }
        private HttpResponseMessage RunPostRequest(GSIApiBase gsiApi)
        {
            var content = !string.IsNullOrEmpty(gsiApi.RequestPayload)
                ? new StringContent(gsiApi.RequestPayload, Encoding.UTF8, "application/json")
                : new StringContent(string.Empty);

            var response = _restClient.PostAsync(gsiApi.ActualUri, content);

            return response.Result;
        }
        public async Task<string> RunPostRequest(string uri, string content)
        {
            if (string.IsNullOrEmpty(content))
                throw new Exception("Content cannot be empty for POST request");

            HttpContent c = new StringContent(content, Encoding.UTF8, "application/json");
            var response = await _restClient.PostAsync(uri, c);
            var context = await response.Content.ReadAsStringAsync();

            return context;
        }
    }
}
