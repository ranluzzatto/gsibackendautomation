﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GSITestInfrastructure.Infrastructure
{
    public static class ConfigurationManagerWrapper
    {
        public static List<GSIUser> GetGSIUsers()
        {
            var users = new List<GSIUser>();
            string file = Assembly.GetExecutingAssembly().Location + ".config";
            System.Xml.XmlDocument xDoc = new();
            
            ExeConfigurationFileMap map = new();
            map.ExeConfigFilename = file;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            string xml = config.GetSection("gsiUsers").SectionInformation.GetRawXml();
            xDoc.LoadXml(xml);

            var avaliableRoles = Enum.GetNames(typeof(UserRoles));

            System.Xml.XmlNode xList = xDoc.ChildNodes[0];
            foreach (System.Xml.XmlNode xNodo in xList)
            {
                var userRole = xNodo.Attributes[2].Value;
                if (!avaliableRoles.Contains(userRole))
                    throw new Exception(string.Format("Role:'{0}' was not found avaliable roles are: {1}", userRole, ConvertStringArrayToString(avaliableRoles)));

                users.Add(new GSIUser
                {
                    UserName = xNodo.Attributes[0].Value,
                    Password = xNodo.Attributes[1].Value,
                    Role = (UserRoles)Enum.Parse(typeof(UserRoles), userRole)
                }); 
            }

            return users;
        }

        public static string GetParameterValue(string paramName)
        {
            return ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location)
                .AppSettings.Settings[paramName].Value.ToString();
        }

        public static void SetParameterValue(string paramName, string value)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            configuration.AppSettings.Settings[paramName].Value = value;
            configuration.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection(configuration.AppSettings.SectionInformation.Name);
        }

        public static KeyValueConfigurationCollection GetAllParametersList()
        {
            return ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location)
                .AppSettings.Settings;
        }

        private static string ConvertStringArrayToString(string[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder strinbuilder = new();
            foreach (string value in array)
            {
                strinbuilder.Append(value);
                strinbuilder.Append(' ');
            }
            return strinbuilder.ToString();
        }
    }
}
