﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSITestInfrastructure.Redis
{
    public class GSIRealTimeCommand
    {
        public static GSIRealTimeCommand FormJson(string json)
        {
            return JsonConvert.DeserializeObject<GSIRealTimeCommand>(json);
        }
        public static string ToJson(GSIRealTimeCommand obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }

        #region enum
        public enum RequestsProgram : int
        {
            OpenProgram = 0,
            CloseProgram = 1,
            PauseProgram = 2,
            SkipCurrentValueInProgram = 3,
            StartStep = 4,
            StartSeqFromStep = 5,
            StopSeqFromStep = 6,
            ClearTestResult = 7,
            Other = 0xFFFF
        }

        public enum RequestsValve : int
        {
            OpenValve = 0,
            CloseValve = 1,
            ChangeValveOperation = 2,
            ChangeFactor = 3,
            Other = 0xFFFF
        }
        public enum Commands : int
        {
            DeviceCommand = 0,
            ValveCommand = 1,
            ProgramCommand = 2,
            FullEvent = 3,
            NoEvent = 4,
            ReadCNF = 5,
            ReadModel = 6,
            ReadClock = 7,
            ReadCommType = 8,
            RainyCommand = 9,
            ResetLogs = 10,
            Pause = 11,
            FertPause = 12,
            DownloadVersion = 13,
            Other = 0xFFFF
        }

        public enum RequestsPause : int
        {
            PauseCurrentPipe = 0,
            PauseCurrentSeq = 1,
            PausePipe = 2,
            PauseSeq = 3,
            ReleasePipe = 4,
            ReleaseSeq = 5
        }
        public enum RequestsFertPause : int
        {
            CancelTodayFertSeq = 0,
            CancelTodayFertPipe = 1,
            ReleaseTodayFertSeq = 2,
            ReleaseTodayFertPipe = 3

        }

        #endregion

        #region properties

        public Commands Command
        {
            get
            {
                if (Enum.IsDefined(typeof(Commands), CommandType))
                {
                    return (Commands)CommandType;
                }
                else
                {
                    return Commands.Other;
                }
            }
        }
        public RequestsProgram CommandProgram
        {
            get
            {
                if (Enum.IsDefined(typeof(RequestsProgram), CommandCode))
                {
                    return (RequestsProgram)CommandCode;
                }
                else
                {
                    return RequestsProgram.Other;
                }
            }
        }
        public RequestsValve ValveCommands
        {
            get
            {
                if (Enum.IsDefined(typeof(RequestsValve), CommandCode))
                {
                    return (RequestsValve)CommandCode;
                }
                else
                {
                    return RequestsValve.Other;
                }
            }
        }
        public string SN { get; set; }
        public int CommandType { get; set; }
        public int CommandCode { get; set; }
        public int ValveNumber { get; set; }
        public int Slot { get; set; }
        public decimal TimeLeft { get; set; }
        public int ProgramNumber { get; set; }
        public long Value { get; set; }
        public int WaterUnit { get; set; }
        public int? WaterMeterUnit { get; set; }
        public int? CommType { get; set; }
        public int? Factor { get; set; }
        public decimal? RainOffDelay { get; set; }
        public int? RainyPipeNum { get; set; }
        public int? LogType { get; set; }
        public int? PipeNum { get; set; }
        public int? MinPause { get; set; }
        public int? StepNumber { get; set; }
        public string VersionNum { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as GSIRealTimeCommand;

            return Command == other.Command &&
                CommandProgram == other.CommandProgram &&
                ValveCommands == other.ValveCommands &&
                SN == other.SN &&
                CommandType == other.CommandType &&
                CommandCode == other.CommandCode &&
                ValveNumber == other.ValveNumber&&
                Slot == other.Slot &&
                TimeLeft == other.TimeLeft &&
                ProgramNumber == other.ProgramNumber &&
                Value == other.Value &&
                WaterUnit == other.WaterUnit &&
                WaterMeterUnit == other.WaterMeterUnit &&
                CommType == other.CommType &&
                Factor == other.Factor &&
                RainOffDelay == other.RainOffDelay &&
                RainyPipeNum == other.RainyPipeNum &&
                LogType == other.LogType &&
                PipeNum == other.PipeNum &&
                MinPause == other.MinPause &&
                StepNumber == other.StepNumber &&
                VersionNum == other.VersionNum ;

        }
        public override int GetHashCode() => (Command, CommandProgram, CommandCode, ValveCommands, SN,
            CommandType, CommandCode, ValveNumber, Slot, TimeLeft, ProgramNumber,Value,WaterUnit,WaterMeterUnit,
            CommType,Factor,RainOffDelay,RainyPipeNum,LogType,PipeNum,MinPause,StepNumber,VersionNum)
            .GetHashCode();

        public static bool operator == (GSIRealTimeCommand lhs, GSIRealTimeCommand rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }
        public static bool operator !=(GSIRealTimeCommand lhs, GSIRealTimeCommand rhs) => !(lhs == rhs);

        #endregion
    }
}
