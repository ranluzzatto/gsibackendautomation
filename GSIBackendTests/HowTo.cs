﻿/*using GalileoTestInfrastructure.GalileoApis.Alarm;
using GalileoTestInfrastructure.GalileoApis.AppSettings;
using GalileoTestInfrastructure.GalileoApis.AuxilaryOutputElementGroup;
using GalileoTestInfrastructure.GalileoApis.DataCollectionSensorElementGroup;
using GalileoTestInfrastructure.GalileoApis.Firmware;
using GalileoTestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GalileoBackendTests
{
    public class HowTo
    {
        /// <summary>
        /// How To verify that a redis data request was generated by the backend (Real Time Command)
        /// The example sends the POST request /config/{configID}/element-group/data-collection-sensor/run-events
        /// the request should trigger by the backend a redis data request publication for data collection sensor
        /// </summary>
        [Test]
        [Category("Validate Redis Data Request")]
        public void ValidateRedisEvent()
        {
            //setup the data event request for validation, you can retrive it from the redis console 
            var eventToValidate = @"{
                                      ""SN"": ""SerialNumberPlaceHolder"",
                                      ""Type"": 1,
                                      ""CommandCode"": 32,
                                      ""ComponentTypeCode"": 1,
                                      ""CNFCode"": 0,
                                      ""Number"": 0,
                                      ""Numbers"": [
                                        3
                                      ],
                                      ""StatusCode"": 0,
                                      ""RealTimeValue"": null,
                                      ""FirmwareId"": 0,
                                      ""LogType"": 0
                                    }".Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber"));

            //prepare the backend API
            var dcsRunEventsApi = new DataCollectionSensorRunEventsApi
            {//request to run events for item# 3, assign the Payload to the API object
                RequestPayload = @"{""numbers"":[3]}"
            };

            //an event should be published by the backend, the published event
            //would be compared with the event configured in 'AddRedisEventToValidate'
            dcsRunEventsApi.AddRedisEventToValidate(eventToValidate);

            //run the request, the back should generate the redis data request
            dcsRunEventsApi.Run();

            //Validation resoult can be asserted
            Assert.IsTrue(dcsRunEventsApi.RedisValidationResult, "expected redis mseeage was not found");
        }

        [TestCase(UserRoles.Payment)]
        [TestCase(UserRoles.God)]
        [TestCase(UserRoles.User)]
        [Category("Call Apis with different users")]
        public void RunApiWithDifferentUsers(UserRoles role)
        {
            //setup the API
            var appSettingsGetApi = new AppSettingsGetApi(new Dictionary<string, string> { { "key", "1" } });

            //run the API as a role given in the iteration parameter as defined in the 'TestCase' tag on top of the test function
            //you can also use a specific role: api.AsUser(UserRoles.God).Run()
            //or use specific email:api.AsUser("someone@gmail.com").Run()
            //the user should be defined in app.config under <galileoUsers>
            appSettingsGetApi.AsUser(role).Run();

            //checked the API behaviur according to the given role
            if (role == UserRoles.Payment)
                Assert.AreEqual(appSettingsGetApi.ResponseStatusCode, HttpStatusCode.Forbidden,
                    "For user role:'Payment' - status code in the response should be Forbidden");

            if (role == UserRoles.God)
                Assert.IsTrue(appSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK,
                    "For user role:'God' - status code in the response should be OK");

            if (role == UserRoles.User)
                Assert.IsTrue(appSettingsGetApi.ResponseStatusCode == HttpStatusCode.Forbidden,
                    "For user role:'User' - status code in the response should be Forbidden");
        }

        [Test]
        [Category("Validate response code and (body)result")]
        public void ValidateResponseCodeAndResult()
        {
            //setup an API instanse
            var firmwaresApi = new FirmwaresApi();

            //run the API
            firmwaresApi.Run();

            //the API instanse holds the response statusCode after it was run
            Assert.IsTrue(firmwaresApi.ResponseStatusCode == HttpStatusCode.OK, "status code in the response should be OK");

            //use the extention method GetBoolValueFromPath to retrive the bool result from the response body
            Assert.IsTrue(firmwaresApi.ResponseBody.GetBoolValueFromPath("result"));

            //another option is to work directlly with the response body as JObject and cast to bool
            Assert.IsTrue((bool)firmwaresApi.ResponseBody["result"]);
        }

        [Test]
        [Category("Iterate items in the response body")]
        public void IterateItemsInResponse()
        {
            //setup the API
            var auxilaryOutputGetApi = new AuxilaryOutputGetApi();

            //Run the API
            auxilaryOutputGetApi.Run();

            //get the the auxilary output items from the response
            var programItems = auxilaryOutputGetApi.ResponseBody["body"]["programItems"];

            //iterate
            foreach(var programItem in programItems)
            {
                // get the the auxilary output item name, the 'name' is the property name within the response
                 var auxilaryOutputName = programItem.GetStringValueFromPath("name");

                //auxilary output name it is not empty
                Assert.IsFalse(string.IsNullOrEmpty(auxilaryOutputName));
            }
        }

        [Test]
        [Category("Validate read CNF Redis request was published")]
        public void ValidateReadCNFRedisRequestWasPublished()
        {
            //Read CNF event is sent only if there is a differant between cuttent data in the database and the data that is being sent
            //so in the bellow example, first the data is sent first as the default data and than is is changed from the default
           
            //setup default data 
            var alarmsPutApi = new AlarmsPutApi();
            alarmsPutApi.RequestPayload = AlarmsPutApi.DefaultPayload;
            alarmsPutApi.Run();

            var changedBody = AlarmsPutApi.DefaultPayload.ToJObject();

            //make a change (any change) in the default data and run the API
            changedBody["setup"]["alarmCancelCycle"] = 30;

            alarmsPutApi.RequestPayload = changedBody.ToString();

            //configure the API to validate the read cnf data event
            alarmsPutApi.ValidateReadCnfRedisEvent = true;

            //run the API
            alarmsPutApi.Run();

            //Validate the Read CNF Data event was published
            Assert.IsTrue(alarmsPutApi.ReadCnfRedisEventValidationResult, "Read CNF Data event was not published");
        }
    }
}
*/