﻿using GSITestInfrastructure.GSIApis.IrrimaxApi;
using GSITestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class IrrimaxTests
    {
        [Test]
        [Category("Irrimax")]
        [Category("Irrimax api key")]
        public void IrrimaxApiKey()
        {
            var irrimaxApiKeyGetApi = new IrrimaxApiKeyGetApi();
            irrimaxApiKeyGetApi.Run();

            var result = irrimaxApiKeyGetApi.ResponseBody["Body"];
            Assert.IsTrue(irrimaxApiKeyGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrimaxApiKeyGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsNotEmpty(result.GetStringValueFromPath("ApiKey"));
        }

        [Test]
        [Category("Irrimax")]
        [Category("Save")]
        public void Save()
        {
            var savePostApi = new SavePostApi();
            var changedPayload = savePostApi.payload.ToJObject();
            var newKey = "KeyCheck";
            changedPayload["ApiKey"] = newKey;
            savePostApi.RequestPayload = changedPayload.ToString();
            savePostApi.Run();

            Assert.IsTrue(savePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(savePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var irrimaxApiKeyGetApi = new IrrimaxApiKeyGetApi();
            irrimaxApiKeyGetApi.Run();

            var result = irrimaxApiKeyGetApi.ResponseBody["Body"];
            Assert.AreEqual(newKey, result.GetStringValueFromPath("ApiKey"), "ApiKey");

        }

        [Test]
        [Category("Irrimax")]
        [Category("Encrypted token")]
        public void EncryptedToken()
        {
            var encryptedtokenGetApi = new EncryptedTokenGetApi();
            encryptedtokenGetApi.Run();

            var result = encryptedtokenGetApi.ResponseBody["Body"];
            Assert.IsTrue(encryptedtokenGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(encryptedtokenGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsNotEmpty(result.GetStringValueFromPath("EncryptedToken"));


        }

    }
}