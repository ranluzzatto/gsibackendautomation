﻿using GSITestInfrastructure.GSIApis.Admin;
using GSITestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class AdminTests
    {
        [TestCase("ShlomiO", "123456", true)]
        [TestCase("ShlomiO", "1234567", false)]
        [Test]
        [Category("Admin")]
        [Category("Login")]
        public void Login(string userName, string password, bool passOrFail)
        {
            var loginPostApi = new LoginPostApi();
            var res = loginPostApi.payload.ToJObject();
            res["UserName"] = userName;
            res["Password"] = password;
            loginPostApi.RequestPayload = res.ToString();
            loginPostApi.Run();

            var getResponse = loginPostApi.ResponseBody;
            Assert.IsTrue(loginPostApi.ResponseStatusCode == System.Net.HttpStatusCode.OK, "HTTP request error");
            Assert.AreEqual(passOrFail, getResponse.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = getResponse["Body"];
                if(body.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(getResponse.GetStringValueFromPath("Body.AccountToken"), "AccountToken");
                    Assert.IsNotEmpty(getResponse.GetStringValueFromPath("Body.Werranty"), "Werranty");
                    Assert.IsNotEmpty(getResponse.GetStringValueFromPath("Body.WerrantyVersion"), "WerrantyVersion");
                }

                if (getResponse["Messages"].Type != JTokenType.Null)
                {
                    Assert.IsTrue(getResponse.GetStringValueFromPath("Messages[0].Message") == "ERROR_IN_PASSWORD_OR_USERNAME");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Reset self password")]
        public void ResetSelfPassword()
        {
            var resetSelfPasswordPostApi = new ResetSelfPasswordPostApi();
            var res = resetSelfPasswordPostApi.payload.ToJObject();
            var originalPassword = "123456";
            var newPassword = "111111";
            res["OldPassword"] = originalPassword;
            res["NewPassword"] = newPassword;
            resetSelfPasswordPostApi.RequestPayload = res.ToString();
            resetSelfPasswordPostApi.AsUser(UserRoles.Project_Admin).Run();

            var getResponse = resetSelfPasswordPostApi.ResponseBody;
            Assert.IsTrue(resetSelfPasswordPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getResponse.GetBoolValueFromPath("Result"), "Service result error");

            res["OldPassword"] = newPassword;
            res["NewPassword"] = originalPassword;
            resetSelfPasswordPostApi.RequestPayload = res.ToString();
            resetSelfPasswordPostApi.AsUser(UserRoles.Project_Admin).Run();
        }

        [TestCase("AutomationSubUser", "AutomationSubUser2", true)]
        [TestCase("AutomationSubUser", "Fake", false)]
        [Test]
        [Category("Admin")]
        [Category("Reset self user name")]
        public void ResetSelfUserName(string currentName, string newName, bool passOrFail)
        {
            var resetSelfUserNamePostApi = new ResetSelfUserNamePostApi();
            var res = resetSelfUserNamePostApi.payload.ToJObject();
            res["OldUserName"] = currentName;
            res["NewUserName"] = newName;
            resetSelfUserNamePostApi.RequestPayload = res.ToString();
            resetSelfUserNamePostApi.AsUser(UserRoles.Project_Admin).Run();

            var getResponse = resetSelfUserNamePostApi.ResponseBody;
            if (passOrFail)
            {
                Assert.IsTrue(resetSelfUserNamePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            } else {
                Assert.IsTrue(resetSelfUserNamePostApi.ResponseStatusCode == HttpStatusCode.Forbidden, "HTTP request error");
            }

            Assert.AreEqual(passOrFail, getResponse.GetBoolValueFromPath("Result"), "Service result error");

            if(getResponse["Messages"].Type != JTokenType.Null)
            {
                Assert.IsTrue(getResponse.GetStringValueFromPath("Messages[0].Message") == "USERNAME_ALREADY_EXISTS");
            }

            // Return to original data
            res["OldUserName"] = newName;
            res["NewUserName"] = currentName;
            resetSelfUserNamePostApi.RequestPayload = res.ToString();
            resetSelfUserNamePostApi.AsUser(UserRoles.Project_Admin).Run();
        }

        [Test]
        [Category("Admin")]
        [Category("Terms of service")]
        public void TermsOfService()
        {
            var termsOfServicePostApi = new TermsOfServicePostApi(new Dictionary<string, string> { { "Werranty", "true" }, { "WerrantyVersion", "2.0" } });
            termsOfServicePostApi.Run();

            var getResponse = termsOfServicePostApi.ResponseBody;
            Assert.IsTrue(termsOfServicePostApi.ResponseStatusCode == System.Net.HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getResponse.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Admin")]
        [Category("User info Get")]
        public void UserInfoGet()
        {
            var userInfoGetApi = new UserInfoGetApi();
            userInfoGetApi.Run();

            var getResponse = userInfoGetApi.ResponseBody;
            Assert.IsTrue(userInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getResponse.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = getResponse["Body"];
                var userInfo = body["UserInfo"];
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("CreationDate"), "UserInfo.CreationDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UpdateDate"), "UserInfo.UpdateDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("IsEnabled"), "UserInfo.IsEnabled");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("AlwaysValid"), "UserInfo.AlwaysValid");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("IsUserLink"), "UserInfo.IsUserLink");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("GlobalizationZoneID"), "UserInfo.GlobalizationZoneID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ValidFromDate"), "UserInfo.ValidFromDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ValidToDate"), "UserInfo.ValidToDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Email"), "UserInfo.Email");

                // Maybe Null/Empty/Full - Not checked
                //Assert.IsNotEmpty(userInfo.GetStringValueFromPath("MobileNumber"), "UserInfo.MobileNumber");
                //Assert.IsNotEmpty(userInfo.GetStringValueFromPath("OfficeNumber"), "UserInfo.OfficeNumber");
                
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UIFormatID"), "UserInfo.UIFormatID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Flag_EnableSMSNotify"), "UserInfo.Flag_EnableSMSNotify");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Flag_EnableEmailNotify"), "UserInfo.Flag_EnableEmailNotify");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ConcurrentSessions"), "UserInfo.ConcurrentSessions");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("CultureCode"), "UserInfo.CultureCode");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Werranty"), "UserInfo.Werranty");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("WerrantyVersion"), "UserInfo.WerrantyVersion");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ReadSystemMessage"), "UserInfo.ReadSystemMessage");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UserPaymentExpirationDate"), "UserInfo.UserPaymentExpirationDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UserPaymentStatus"), "UserInfo.UserPaymentStatus");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("DaysLefttoExpired"), "UserInfo.DaysLefttoExpired");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UnpaidUser"), "UserInfo.UnpaidUser");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UserID"), "UserInfo.UserID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("FirstName"), "UserInfo.FirstName");
                Assert.IsNotNull(userInfo.GetStringValueFromPath("LastName"), "UserInfo.LastName");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("RoleTypeID"), "UserInfo.RoleTypeID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ParentUserID"), "UserInfo.ParentUserID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Username"), "UserInfo.Username");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ParentUserDisplayName"), "UserInfo.ParentUserDisplayName");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("LastActivityDate"), "UserInfo.LastActivityDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("LastLoginDate"), "UserInfo.LastLoginDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("IsUserWatcher"), "UserInfo.IsUserWatcher");

                foreach (var project in body["Projects"])
                {
                    Assert.IsNotEmpty(project.GetStringValueFromPath("ProjectID"), "Projects.ProjectID");
                    Assert.IsNotEmpty(project.GetStringValueFromPath("ProjectName"), "Projects.ProjectName");
                }

                foreach (var customRoleItem in body["CustomRoleItems"])
                {
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("UserID"), "CustomRoleItems.UserID");
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("isMatch"), "CustomRoleItems.isMatch");
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("RoleItemTypeID"), "CustomRoleItems.RoleItemTypeID");
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("Name"), "CustomRoleItems.Name");
                }

                foreach (var listRoleItem in body["ListRoleItems"])
                {
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("UserID"), "ListRoleItems.UserID");
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("isMatch"), "ListRoleItems.isMatch");
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("RoleItemTypeID"), "ListRoleItems.RoleItemTypeID");
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("Name"), "ListRoleItems.Name");
                }

                foreach (var alertsSetting in body["AlertsSetting"])
                {
                    Assert.IsNotEmpty(alertsSetting.GetStringValueFromPath("AlertCode"), "AlertsSetting.AlertCode");
                    Assert.IsNotEmpty(alertsSetting.GetStringValueFromPath("SendEmail"), "AlertsSetting.SendEmail");
                }

                foreach (var subUser in body["SubUsers"])
                {
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("UserID"), "SubUsers.UserID");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("FirstName"), "SubUsers.FirstName");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("LastName"), "SubUsers.LastName");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("AlwaysValid"), "SubUsers.AlwaysValid");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("LastActivityDate"), "SubUsers.LastActivityDate");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("RoleTypeID"), "SubUsers.RoleTypeID");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("RoleTypeName"), "SubUsers.RoleTypeName");
                }

                foreach (var directUser in body["DirectUsers"])
                {
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("UserID"), "DirectUsers.UserID");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("FirstName"), "DirectUsers.FirstName");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("LastName"), "DirectUsers.LastName");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("AlwaysValid"), "DirectUsers.AlwaysValid");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("LastActivityDate"), "DirectUsers.LastActivityDate");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("RoleTypeID"), "DirectUsers.RoleTypeID");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("RoleTypeName"), "DirectUsers.RoleTypeName");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("User info Post")]
        public void UserInfoPost()
        {
            var userInfoPostApi = new UserInfoPostApi();
            var resPost = userInfoPostApi.payload.ToJObject();
            resPost["UserInfo"]["RoleTypeID"] = "500";

            userInfoPostApi.RequestPayload = resPost.ToString();
            userInfoPostApi.Run();

            var responsePost = userInfoPostApi.ResponseBody;
            Assert.IsTrue(userInfoPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            //Get api to check Post method
            var userInfoGetApi = new UserInfoGetApi();
            userInfoGetApi.Run();

            var response = userInfoGetApi.ResponseBody;
            Assert.IsTrue(userInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(userInfoGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var userInfo = response["Body"]["UserInfo"];
            Assert.AreEqual("Admin", userInfo.GetStringValueFromPath("RoleTypeID"));

            // Return to original data
            var resOriginal = userInfoPostApi.payload.ToJObject();
            resOriginal["UserInfo"]["RoleTypeID"] = "50";
            userInfoPostApi.RequestPayload = resOriginal.ToString();
            userInfoPostApi.Run();
        }

        [Test]
        [Category("Admin")]
        [Category("Sub Users")]
        public void SubUsers()
        {
            var subUsersPostApi = new SubUsersPostApi(new Dictionary<string, string> { { "projectID", ConfigurationManagerWrapper.GetParameterValue("projectID") } });
            subUsersPostApi.RequestPayload = subUsersPostApi.payload;
            subUsersPostApi.Run();

            var response = subUsersPostApi.ResponseBody;
            Assert.IsTrue(subUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var item in body["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserID"), "Response.UserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FirstName"), "Response.FirstName");
                    Assert.IsNotNull(item.GetStringValueFromPath("LastName"), "Response.LastName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleTypeID"), "Response.RoleTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ParentUserID"), "Response.ParentUserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Username"), "Response.Username");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ParentUserDisplayName"), "Response.ParentUserDisplayName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastActivityDate"), "Response.LastActivityDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastLoginDate"), "Response.LastLoginDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsUserWatcher"), "Response.IsUserWatcher");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Sub Users Post Api with search")]
        public void SubUsersWithSearch()
        {
            var subUsersPostApi = new SubUsersPostApi(new Dictionary<string, string> { { "Project", ConfigurationManagerWrapper.GetParameterValue("projectID") } });
            var res = subUsersPostApi.payload.ToJObject();
            var searchName = "ShlomiO";
            res["Search"] = searchName;
            subUsersPostApi.RequestPayload = res.ToString();
            subUsersPostApi.Run();

            var response = subUsersPostApi.ResponseBody;
            Assert.IsTrue(subUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(searchName, response.GetStringValueFromPath("Body.Response[0].Username"), "UserName");
        }

        [Test]
        [Category("Admin")]
        [Category("Child subUsers")]
        public void ChildSubUsers()
        {
            var childSubUsersPostApi = new ChildSubUsersPostApi(new Dictionary<string, string> { { "userID", ConfigurationManagerWrapper.GetParameterValue("userID") } });
            childSubUsersPostApi.RequestPayload = childSubUsersPostApi.payload;
            childSubUsersPostApi.Run();

            var response = childSubUsersPostApi.ResponseBody;
            Assert.IsTrue(childSubUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var item in body["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserID"), "Response.UserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FirstName"), "Response.FirstName");
                    Assert.IsNotNull(item.GetStringValueFromPath("LastName"), "Response.LastName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AlwaysValid"), "Response.AlwaysValid");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastActivityDate"), "Response.LastActivityDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleTypeID"), "Response.RoleTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleTypeName"), "Response.RoleTypeName");
                }
            });

        }

        [Test]
        [Category("Admin")]
        [Category("Login info")]
        public void LoginInfo()
        {
            var loginInfoGetApi = new LoginInfoGetApi();
            loginInfoGetApi.Run();

            var response = loginInfoGetApi.ResponseBody;
            Assert.IsTrue(loginInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("CreationDate"), "CreationDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UpdateDate"), "UpdateDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("IsEnabled"), "IsEnabled");
                Assert.IsNotEmpty(body.GetStringValueFromPath("AlwaysValid"), "AlwaysValid");
                Assert.IsNotEmpty(body.GetStringValueFromPath("IsUserLink"), "IsUserLink");
                Assert.IsNotEmpty(body.GetStringValueFromPath("GlobalizationZoneID"), "GlobalizationZoneID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ValidFromDate"), "ValidFromDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ValidToDate"), "ValidToDate");
                Assert.IsNotNull(body.GetStringValueFromPath("Email"), "Email");
                Assert.IsNotNull(body.GetStringValueFromPath("MobileNumber"), "MobileNumber");
                Assert.IsNotNull(body.GetStringValueFromPath("OfficeNumber"), "OfficeNumber");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UIFormatID"), "UIFormatID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Flag_EnableSMSNotify"), "Flag_EnableSMSNotify");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Flag_EnableEmailNotify"), "Flag_EnableEmailNotify");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ConcurrentSessions"), "ConcurrentSessions");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Werranty"), "Werranty");
                Assert.IsNotEmpty(body.GetStringValueFromPath("WerrantyVersion"), "WerrantyVersion");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ReadSystemMessage"), "ReadSystemMessage");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UserPaymentExpirationDate"), "UserPaymentExpirationDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UserPaymentStatus"), "UserPaymentStatus");
                Assert.IsNotEmpty(body.GetStringValueFromPath("DaysLefttoExpired"), "DaysLefttoExpired");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UnpaidUser"), "UnpaidUser");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UserID"), "UserID");
                Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("userID"), body.GetStringValueFromPath("UserID"), "UserID - compare");
                Assert.IsNotNull(body.GetStringValueFromPath("FirstName"), "FirstName");
                Assert.IsNotNull(body.GetStringValueFromPath("LastName"), "LastName");
                Assert.IsNotEmpty(body.GetStringValueFromPath("RoleTypeID"), "RoleTypeID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ParentUserID"), "ParentUserID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Username"), "Username");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ParentUserDisplayName"), "ParentUserDisplayName");
                Assert.IsNotEmpty(body.GetStringValueFromPath("LastActivityDate"), "LastActivityDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("LastLoginDate"), "LastLoginDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("IsUserWatcher"), "IsUserWatcher");
            });
        }

        [Test]
        [Category("Admin")]
        [Category("AppUser info - Only for app")]
        public void AppUserInfo()
        {
            var appUserInfoGetApi = new AppUserInfoGetApi();
            appUserInfoGetApi.Run();

            var response = appUserInfoGetApi.ResponseBody;
            Assert.IsTrue(appUserInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                var userInfo = body["UserInfo"];
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("CreationDate"), "UserInfo.CreationDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UpdateDate"), "UserInfo.UpdateDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("IsEnabled"), "UserInfo.IsEnabled");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("AlwaysValid"), "UserInfo.AlwaysValid");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("IsUserLink"), "UserInfo.IsUserLink");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("GlobalizationZoneID"), "UserInfo.GlobalizationZoneID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ValidFromDate"), "UserInfo.ValidFromDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ValidToDate"), "UserInfo.ValidToDate");
                Assert.IsNotNull(userInfo.GetStringValueFromPath("Email"), "UserInfo.Email");
                Assert.IsNotNull(userInfo.GetStringValueFromPath("MobileNumber"), "UserInfo.MobileNumber");
                Assert.IsNotNull(userInfo.GetStringValueFromPath("OfficeNumber"), "UserInfo.OfficeNumber");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UIFormatID"), "UserInfo.UIFormatID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Flag_EnableSMSNotify"), "UserInfo.Flag_EnableSMSNotify");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Flag_EnableEmailNotify"), "UserInfo.Flag_EnableEmailNotify");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ConcurrentSessions"), "UserInfo.ConcurrentSessions");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("CultureCode"), "UserInfo.CultureCode");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Werranty"), "UserInfo.Werranty");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("WerrantyVersion"), "UserInfo.WerrantyVersion");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ReadSystemMessage"), "UserInfo.ReadSystemMessage");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UserPaymentExpirationDate"), "UserInfo.UserPaymentExpirationDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UserPaymentStatus"), "UserInfo.UserPaymentStatus");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("DaysLefttoExpired"), "UserInfo.DaysLefttoExpired");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UnpaidUser"), "UserInfo.UnpaidUser");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("UserID"), "UserInfo.UserID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("FirstName"), "UserInfo.FirstName");
                Assert.IsNotNull(userInfo.GetStringValueFromPath("LastName"), "UserInfo.LastName");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("RoleTypeID"), "UserInfo.RoleTypeID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ParentUserID"), "UserInfo.ParentUserID");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("Username"), "UserInfo.Username");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("ParentUserDisplayName"), "UserInfo.ParentUserDisplayName");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("LastActivityDate"), "UserInfo.LastActivityDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("LastLoginDate"), "UserInfo.LastLoginDate");
                Assert.IsNotEmpty(userInfo.GetStringValueFromPath("IsUserWatcher"), "UserInfo.IsUserWatcher");

                foreach (var project in body["Projects"])
                {
                    Assert.IsNotEmpty(project.GetStringValueFromPath("ProjectID"), "Projects.ProjectID");
                    Assert.IsNotEmpty(project.GetStringValueFromPath("ProjectName"), "Projects.ProjectName");
                }

                foreach (var customRoleItem in body["CustomRoleItems"])
                {
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("UserID"), "CustomRoleItems.UserID");
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("isMatch"), "CustomRoleItems.isMatch");
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("RoleItemTypeID"), "CustomRoleItems.RoleItemTypeID");
                    Assert.IsNotEmpty(customRoleItem.GetStringValueFromPath("Name"), "CustomRoleItems.Name");
                }

                foreach (var listRoleItem in body["ListRoleItems"])
                {
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("UserID"), "ListRoleItems.UserID");
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("isMatch"), "ListRoleItems.isMatch");
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("RoleItemTypeID"), "ListRoleItems.RoleItemTypeID");
                    Assert.IsNotEmpty(listRoleItem.GetStringValueFromPath("Name"), "ListRoleItems.Name");
                }

                foreach (var alertsSetting in body["AlertsSetting"])
                {
                    Assert.IsNotEmpty(alertsSetting.GetStringValueFromPath("AlertCode"), "AlertsSetting.AlertCode");
                    Assert.IsNotEmpty(alertsSetting.GetStringValueFromPath("SendEmail"), "AlertsSetting.SendEmail");
                }

                foreach (var subUser in body["SubUsers"])
                {
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("UserID"), "SubUsers.UserID");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("FirstName"), "SubUsers.FirstName");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("LastName"), "SubUsers.LastName");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("AlwaysValid"), "SubUsers.AlwaysValid");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("LastActivityDate"), "SubUsers.LastActivityDate");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("RoleTypeID"), "SubUsers.RoleTypeID");
                    Assert.IsNotEmpty(subUser.GetStringValueFromPath("RoleTypeName"), "SubUsers.RoleTypeName");
                }

                foreach (var directUser in body["DirectUsers"])
                {
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("UserID"), "DirectUsers.UserID");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("FirstName"), "DirectUsers.FirstName");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("LastName"), "DirectUsers.LastName");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("AlwaysValid"), "DirectUsers.AlwaysValid");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("LastActivityDate"), "DirectUsers.LastActivityDate");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("RoleTypeID"), "DirectUsers.RoleTypeID");
                    Assert.IsNotEmpty(directUser.GetStringValueFromPath("RoleTypeName"), "DirectUsers.RoleTypeName");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("User notify - Only for app")]
        public void UserNotify()
        {
            var userNotifyPostApi = new UserNotifyPostApi(new Dictionary<string, string> { { "Status", "true" }, { "AppID", "123e8690-68c4-11ec-8eba-e286c4ba849c" } });
            userNotifyPostApi.Run();

            Assert.IsTrue(userNotifyPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(userNotifyPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Admin")]
        [Category("Password")]
        public void Password()
        {
            var passwordPostApi = new PasswordPostApi();
            var res = passwordPostApi.payload.ToJObject();
            var oldUserName = ConfigurationManagerWrapper.GetParameterValue("GSIUserDemo");
            var oldPassword = ConfigurationManagerWrapper.GetParameterValue("GSIPasswordDemo");
            var newUserName = oldUserName + "2";
            var newPassword = "111111";
            res["UserName"] = newUserName;
            res["Password"] = newPassword;
            passwordPostApi.RequestPayload = res.ToString();
            passwordPostApi.Run();

            var getResponse = passwordPostApi.ResponseBody;
            Assert.IsTrue(passwordPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getResponse.GetBoolValueFromPath("Result"), "Service result error");

            //change back user name and password for next test
            var resBack = passwordPostApi.payload.ToJObject();
            resBack["UserName"] = oldUserName;
            resBack["Password"] = oldPassword;
            passwordPostApi.RequestPayload = resBack.ToString();
            passwordPostApi.Run();
        }

        [Test]
        [Category("Admin")]
        [Category("Delete user")]
        public void DeleteUser()
        {
            var addUserPostApi = new AddUserPostApi();
            var changedPayloadAdd = addUserPostApi.payload.ToJObject();
            var NewUser = "addUserTest";
            changedPayloadAdd["userName"] = NewUser;
            addUserPostApi.RequestPayload = changedPayloadAdd.ToString();
            addUserPostApi.Run();

            Assert.IsTrue(addUserPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(addUserPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Confirm user created
            var subUsersPostApi = new SubUsersPostApi();
            var changedPayloadSearch = subUsersPostApi.payload.ToJObject();
            changedPayloadSearch["Search"] = NewUser;
            subUsersPostApi.RequestPayload = changedPayloadSearch.ToString();
            subUsersPostApi.Run();

            var responseSearch = subUsersPostApi.ResponseBody;
            Assert.IsTrue(addUserPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseSearch.GetBoolValueFromPath("Result"), "Service result error");

            var tempParam = ConfigurationManagerWrapper.GetParameterValue("GSIIDDemo");
            foreach (var item in responseSearch["Body"]["Response"])
            {
                if (item.GetStringValueFromPath("Username") == NewUser)
                {
                    ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", item.GetStringValueFromPath("UserID"));
                    break;
                }
            }

            // Delete user
            var deleteUserDeleteApi = new DeleteUserDeleteApi();
            deleteUserDeleteApi.Run();

            Assert.IsTrue(deleteUserDeleteApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(deleteUserDeleteApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Confirm user deleted
            subUsersPostApi.Run();

            var responseSearchAfterDelete = subUsersPostApi.ResponseBody;
            Assert.IsTrue(addUserPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseSearchAfterDelete.GetBoolValueFromPath("Result"), "Service result error");

            var result = true;
            foreach (var item in responseSearchAfterDelete["Body"]["Response"])
            {
                if (item.GetStringValueFromPath("Username") == NewUser)
                {
                    result = false;
                    break;
                }
            }

            Assert.IsTrue(result);

            ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", tempParam);
        }

        [Test]
        [Category("Admin")]
        [Category("Add user - Success")]
        public void AddUserSuccess()
        {
            var addUserPostApi = new AddUserPostApi();
            var changedPayloadAdd = addUserPostApi.payload.ToJObject();
            var NewUser = "addUserTest";
            changedPayloadAdd["userName"] = NewUser;
            addUserPostApi.RequestPayload = changedPayloadAdd.ToString();
            addUserPostApi.Run();

            Assert.IsTrue(addUserPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(addUserPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Confirm user created
            var subUsersPostApi = new SubUsersPostApi();
            var changedPayloadSearch = subUsersPostApi.payload.ToJObject();
            changedPayloadSearch["Search"] = NewUser;
            subUsersPostApi.RequestPayload = changedPayloadSearch.ToString();
            subUsersPostApi.Run();

            var responseSearch = subUsersPostApi.ResponseBody;
            Assert.IsTrue(addUserPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseSearch.GetBoolValueFromPath("Result"), "Service result error");

            var tempParam = ConfigurationManagerWrapper.GetParameterValue("GSIIDDemo");
            foreach (var item in responseSearch["Body"]["Response"])
            {
                if(item.GetStringValueFromPath("Username") == NewUser)
                {
                    ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", item.GetStringValueFromPath("UserID"));
                }
            }

            // Delete user
            var deleteUserDeleteApi = new DeleteUserDeleteApi();
            deleteUserDeleteApi.Run();

            Assert.IsTrue(deleteUserDeleteApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(deleteUserDeleteApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", tempParam);
        }

        [Test]
        [Category("Admin")]
        [Category("Add user - Fail")]
        public void AddUserFail()
        {
            var addUserPostApi = new AddUserPostApi();
            var changedPayload = addUserPostApi.payload.ToJObject();
            changedPayload["userName"] = ConfigurationManagerWrapper.GetParameterValue("GSIUserDemo");
            addUserPostApi.RequestPayload = changedPayload.ToString();
            addUserPostApi.Run();

            var getResponse = addUserPostApi.ResponseBody;
            Assert.IsTrue(addUserPostApi.ResponseStatusCode == HttpStatusCode.Forbidden, "HTTP request error");
            Assert.IsFalse(getResponse.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Admin")]
        [Category("User types")]
        public void UserTypes()
        {
            var userTypesGetApi = new UserTypesGetApi();
            userTypesGetApi.Run();

            var response = userTypesGetApi.ResponseBody;
            Assert.IsTrue(userTypesGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                foreach (var item in body["RolesTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleID"), "RolesTypes.RoleID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleID"), "RolesTypes.RoleID");
                }

                foreach (var item in body["RoleItemType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserID"), "RoleItemType.UserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleItemTypeID"), "RoleItemType.RoleItemTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "RoleItemType.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsMatch"), "RoleItemType.IsMatch");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleTypeID"), "RoleItemType.RoleTypeID");
                }

                foreach (var item in body["KeyRoleItem"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleID"), "KeyRoleItem.RoleID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RoleItemTypeID"), "KeyRoleItem.RoleItemTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ItemID"), "KeyRoleItem.ItemID");
                }

                foreach (var item in body["TimesZone"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ZoneID"), "TimesZone.ZoneID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DisplayName"), "TimesZone.DisplayName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GMTOffset"), "TimesZone.GMTOffset");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualOffset"), "TimesZone.ActualOffset");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ManualOffset"), "TimesZone.ManualOffset");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DaylightName"), "TimesZone.DaylightName");
                }

                foreach (var item in body["UIFormat"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UIFormatID"), "UIFormat.UIFormatID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LongDatePattern"), "UIFormat.LongDatePattern");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ShortDatePattern"), "UIFormat.ShortDatePattern");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ShortTimePattern"), "UIFormat.ShortTimePattern");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LongTimePattern"), "UIFormat.LongTimePattern");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CultureCode"), "UIFormat.CultureCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DisplayName"), "UIFormat.DisplayName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("YearMonthPattern"), "UIFormat.YearMonthPattern");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsDefault"), "UIFormat.IsDefault");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsRightToLeft"), "UIFormat.IsRightToLeft");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDayOfWeek"), "UIFormat.StartDayOfWeek");
                }

                foreach (var item in body["AlertSettingBits"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath(""), "AlertSettingBits");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Role type")]
        public void RoleType()
        {
            var roleTypePostApi = new RoleTypePostApi(new Dictionary<string, string> { { "NewRoleTypeID", "500" } });
            roleTypePostApi.Run();

            var responsePost = roleTypePostApi.ResponseBody;
            Assert.IsTrue(roleTypePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var userInfoGetApi = new UserInfoGetApi();
            userInfoGetApi.Run();

            var responseGet = userInfoGetApi.ResponseBody;
            Assert.IsTrue(roleTypePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            var body = responseGet["Body"];
            var userInfo = body["UserInfo"];
            Assert.AreEqual(userInfo.GetStringValueFromPath("RoleTypeID"), "Admin");

            // Change for another role
            var roleTypeChange = new RoleTypePostApi(new Dictionary<string, string> { { "NewRoleTypeID", "20" } });
            roleTypeChange.Run();
        }

        [Test]
        [Category("Admin")]
        [Category("Add subUsers")]
        public void AddSubUsers()
        {
            var addSubUsersPostApi = new AddSubUsersPostApi();
            var changedPayloadAdd = addSubUsersPostApi.payload.ToJObject();
            var user = "538";
            changedPayloadAdd["UserID"][0] = user;
            addSubUsersPostApi.RequestPayload = changedPayloadAdd.ToString();
            addSubUsersPostApi.Run();

            var responseAdd = addSubUsersPostApi.ResponseBody;
            Assert.IsTrue(addSubUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseAdd.GetBoolValueFromPath("Result"), "Service result error");

            //Confirm subuser added
            var userInfoGetApi = new UserInfoGetApi();
            userInfoGetApi.Run();

            var response = userInfoGetApi.ResponseBody;
            Assert.IsTrue(userInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(user, response.GetStringValueFromPath("Body.SubUsers[0].UserID"));

            // Remove subUser
            var removeSubUsersPostApi = new RemoveSubUsersPostApi();
            var changedPayloadRemove = removeSubUsersPostApi.payload.ToJObject();
            changedPayloadRemove["UserID"][0] = user;
            removeSubUsersPostApi.RequestPayload = changedPayloadRemove.ToString();
            removeSubUsersPostApi.Run();

            Assert.IsTrue(removeSubUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(removeSubUsersPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Admin")]
        [Category("Remove subUsers")]
        public void RemoveSubUsers()
        {
            var addSubUsersPostApi = new AddSubUsersPostApi();
            var changedPayloadAdd = addSubUsersPostApi.payload.ToJObject();
            var user = "538";
            changedPayloadAdd["UserID"][0] = user;
            addSubUsersPostApi.RequestPayload = changedPayloadAdd.ToString();
            addSubUsersPostApi.Run();

            var responseAdd = addSubUsersPostApi.ResponseBody;
            Assert.IsTrue(addSubUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseAdd.GetBoolValueFromPath("Result"), "Service result error");

            //Confirm subuser added
            var userInfoGetApi = new UserInfoGetApi();
            userInfoGetApi.Run();

            var response = userInfoGetApi.ResponseBody;
            Assert.IsTrue(userInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(user, response.GetStringValueFromPath("Body.SubUsers[0].UserID"));

            // Remove subUser
            var removeSubUsersPostApi = new RemoveSubUsersPostApi();
            var changedPayloadRemove = removeSubUsersPostApi.payload.ToJObject();
            changedPayloadRemove["UserID"][0] = user;
            removeSubUsersPostApi.RequestPayload = changedPayloadRemove.ToString();
            removeSubUsersPostApi.Run();

            Assert.IsTrue(removeSubUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(removeSubUsersPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            //confirm subuser deleted
            userInfoGetApi.Run();

            var responseUserInfo = userInfoGetApi.ResponseBody;
            Assert.IsTrue(userInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseUserInfo.GetBoolValueFromPath("Result"), "Service result error");

            var subUsers = userInfoGetApi.ResponseBody["Body"]["SubUsers"];
            var result = true;
            foreach(var item in subUsers)
            {
                if(item.GetStringValueFromPath("UserID") == user)
                {
                    result = false;
                }
            }

            Assert.IsTrue(result);
        }

        [Test]
        [Category("Admin")]
        [Category("User project tree")]
        public void UserProjectTree()
        {
            var userProjectTreeGetApi = new UserProjectTreeGetApi();
            userProjectTreeGetApi.Run();

            var result = userProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(userProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var body in result["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                    Assert.IsNotNull(body.GetObjectValueFromPath("child"), "child");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("User project tree child")]
        public void UserProjectTreeChild()
        {
            var userProjectTreeChildGetApi = new UserProjectTreeChildGetApi(new Dictionary<string, string> { { "LogicAreaID", "92" } });
            userProjectTreeChildGetApi.Run();

            var result = userProjectTreeChildGetApi.ResponseBody;
            Assert.IsTrue(userProjectTreeChildGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach (var body in result["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                    Assert.IsNotNull(body.GetObjectValueFromPath("child"), "child");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("User project tree child")]
        public void ProjectProjectTreeChild()
        {
            var userProjectTreeChildGetApi = new ProjectProjectTreeChildGetApi(new Dictionary<string, string> { { "LogicAreaID", "92" } });
            userProjectTreeChildGetApi.Run();

            var result = userProjectTreeChildGetApi.ResponseBody;
            Assert.IsTrue(userProjectTreeChildGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach (var body in result["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                    Assert.IsNotNull(body.GetObjectValueFromPath("child"), "child");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("User update project tree")]
        public void UserUpdateProjectTree()
        {
            var userUpdateProjectTreePostApi = new UserUpdateProjectTreePostApi();
            var res = userUpdateProjectTreePostApi.payload.ToJObject();
            res["Add"][0]["logicAreaID"] = "427";
            res["Remove"][0]["logicAreaID"] = "";

            userUpdateProjectTreePostApi.RequestPayload = res.ToString();
            userUpdateProjectTreePostApi.Run();

            var responsePost = userUpdateProjectTreePostApi.ResponseBody;
            Assert.IsTrue(userUpdateProjectTreePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            //confirm project added
            var userProjectTreeGetApi = new UserProjectTreeGetApi();
            userProjectTreeGetApi.Run();

            var responseGet = userProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(userProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseGet.GetBoolValueFromPath("Result"), "Service result error"); ;

            var result = false;
            foreach (var item in responseGet.GetStringValueFromPath("Body").ToJObject())
            {
                if (item.GetStringValueFromPath("ID") == "427")
                {
                    result = true;
                    break;
                }
            }

            Assert.IsTrue(result);
        }

        [Test]
        [Category("Admin")]
        [Category("Units list")]
        public void UnitsList()
        {
            var unitsListPostApi = new UnitsListPostApi(new Dictionary<string, string> { { "LogicAreaId", "0" }, { "IncludeOrphanedUnits", "true" }, { "includeSubAreas", "true" }, { "OnlySharedUnits", "true" } });
            unitsListPostApi.RequestPayload = unitsListPostApi.payload;
            unitsListPostApi.Run();

            var response = unitsListPostApi.ResponseBody;
            Assert.IsTrue(unitsListPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach(var response in body["Response"])
                {
                    Assert.IsNotEmpty(response.GetStringValueFromPath("CreationDate"), "Response.CreationDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("UpdateDate"), "Response.UpdateDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("LastSyncDate"), "Response.LastSyncDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("LastConnection"), "Response.LastConnection");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("EstimatedNextConnection"), "Response.EstimatedNextConnection");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ControlUnitID"), "Response.ControlUnitID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("Name"), "Response.Name");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("CurrentConfigID"), "Response.CurrentConfigID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ModelID"), "Response.ModelID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("SN"), "Response.SN");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ModelName"), "Response.ModelName");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ModelVersion"), "Response.ModelVersion");
                    Assert.IsNotNull(response.GetStringValueFromPath("CommUnit_AppVersion"), "Response.CommUnit_AppVersion");
                    Assert.IsNotNull(response.GetStringValueFromPath("CommUnit_SubAppVerion"), "Response.CommUnit_SubAppVerion");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("Flag_AdvancedFert"), "Response.Flag_AdvancedFert");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("StatusID"), "Response.StatusID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("AreasLinkes"), "Response.AreasLinkes");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("IsDirectLink"), "Response.IsDirectLink");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("IsUserLink"), "Response.IsUserLink");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("isPro"), "Response.isPro");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("CorrectVersion"), "Response.CorrectVersion");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ExpiredNoEntry"), "Response.ExpiredNoEntry");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("NewHardware"), "Response.NewHardware");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ConnectionStatus"), "Response.ConnectionStatus");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("UnitPaymentExpirationDate"), "Response.UnitPaymentExpirationDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("UnitPaymentStatus"), "Response.UnitPaymentStatus");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("DaysLefttoExpired"), "Response.DaysLefttoExpired");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("UnpaidUnit"), "Response.UnpaidUnit");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Unit types")]
        public void UnitTypes()
        {
            var unitTypesGetApi = new UnitTypesGetApi();
            unitTypesGetApi.Run();

            var response = unitTypesGetApi.ResponseBody;
            Assert.IsTrue(unitTypesGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(unitTypesGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var timesZone in response["Body"]["TimesZone"])
                {
                    Assert.IsNotEmpty(timesZone.GetStringValueFromPath("ZoneID"), "TimesZone.ZoneID");
                    Assert.IsNotEmpty(timesZone.GetStringValueFromPath("DisplayName"), "TimesZone.DisplayName");
                    Assert.IsNotEmpty(timesZone.GetStringValueFromPath("GMTOffset"), "TimesZone.GMTOffset");
                    Assert.IsNotEmpty(timesZone.GetStringValueFromPath("ActualOffset"), "TimesZone.ActualOffset");
                    Assert.IsNotEmpty(timesZone.GetStringValueFromPath("ManualOffset"), "TimesZone.ManualOffset");
                    Assert.IsNotEmpty(timesZone.GetStringValueFromPath("DaylightName"), "TimesZone.DaylightName");
                }

                foreach (var commType in response["Body"]["CommType"])
                {
                    Assert.IsNotEmpty(commType.GetStringValueFromPath("CommID"), "CommType.CommID");
                    Assert.IsNotEmpty(commType.GetStringValueFromPath("CommType"), "CommType.CommType");
                    Assert.IsNotEmpty(commType.GetStringValueFromPath("IsDefult"), "CommType.IsDefult");
                }

                foreach (var modelsUnit in response["Body"]["ModelsUnit"])
                {
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("ModelID"), "ModelsUnit.ModelID");
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("Name"), "ModelsUnit.Name");
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("Version"), "ModelsUnit.Version");
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("DefaultProgramNumber"), "ModelsUnit.DefaultProgramNumber");
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("ValveNumbers"), "ModelsUnit.ValveNumbers");
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("MasterValveNumber"), "ModelsUnit.MasterValveNumber");
                    Assert.IsNotEmpty(modelsUnit.GetStringValueFromPath("InputConditionCount"), "ModelsUnit.InputConditionCount");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Unit project tree")]
        public void UnitProjectTree()
        {
            var unitProjectTreeGetApi = new UnitProjectTreeGetApi();
            unitProjectTreeGetApi.Run();

            var response = unitProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(unitProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var body in response["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Unit project tree child")]
        public void UnitProjectTreeChild()
        {
            var unitProjectTreeChildGetApi = new UnitProjectTreeChildGetApi(new Dictionary<string, string> { { "LogicAreaID", "121" } });
            unitProjectTreeChildGetApi.Run();

            var response = unitProjectTreeChildGetApi.ResponseBody;
            Assert.IsTrue(unitProjectTreeChildGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach (var body in response["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Unit info Get")]
        public void UnitInfoGet()
        {
            var unitInfoGetApi = new UnitInfoGetApi();
            unitInfoGetApi.Run();

            var getResponse = unitInfoGetApi.ResponseBody;
            Assert.IsTrue(unitInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getResponse.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = getResponse["Body"];
                var unitInfo = body["UnitInfo"];
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("Name"), "UnitInfo.Name");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("Address"), "UnitInfo.Address");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("ControlUnitID"), "UnitInfo.ControlUnitID");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("CurrentConfigID"), "UnitInfo.CurrentConfigID");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("CustomTimeZoneID"), "UnitInfo.CustomTimeZoneID");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("ModelVersion"), "UnitInfo.ModelVersion");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("ModelID"), "UnitInfo.ModelID");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("ModelName"), "UnitInfo.ModelName");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("Description"), "UnitInfo.Description");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("SN"), "UnitInfo.SN");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("CommTypeID"), "UnitInfo.CommTypeID");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("FreeText1"), "UnitInfo.FreeText1");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("FreeText2"), "UnitInfo.FreeText2");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("FreeText3"), "UnitInfo.FreeText3");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("FreeText4"), "UnitInfo.FreeText4");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("IsGSIAg"), "UnitInfo.IsGSIAg");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("ProgramNumber"), "UnitInfo.ProgramNumber");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("ValveNumbers"), "UnitInfo.ValveNumbers");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("MasterValveNumber"), "UnitInfo.MasterValveNumber");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("InputConditionCount"), "UnitInfo.InputConditionCount");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("FlagET"), "UnitInfo.FlagET");
                Assert.IsNotEmpty(unitInfo.GetStringValueFromPath("UnpaidUnit"), "UnitInfo.UnpaidUnit");


                foreach (var keyValue in body["KeyValue"])
                {
                    Assert.IsNotEmpty(keyValue.GetStringValueFromPath("KeyN"), "KeyValue.KeyN");
                    Assert.IsNotEmpty(keyValue.GetStringValueFromPath("ValueV"), "KeyValue.ValueV");
                    Assert.IsNotEmpty(keyValue.GetStringValueFromPath("UpdateDate"), "KeyValue.UpdateDate");
                }

                foreach (var projects in body["Projects"])
                {
                    Assert.IsNotEmpty(projects.GetStringValueFromPath("LogicAreaID"), "Projects.LogicAreaID");
                    Assert.IsNotEmpty(projects.GetStringValueFromPath("CreationDate"), "Projects.CreationDate");
                    Assert.IsNotEmpty(projects.GetStringValueFromPath("UpdateDate"), "Projects.UpdateDate");
                    Assert.IsNotEmpty(projects.GetStringValueFromPath("ReceiveReports"), "Projects.ReceiveReports");
                    Assert.IsNotEmpty(projects.GetStringValueFromPath("ProjectID"), "Projects.ProjectID");
                    Assert.IsNotEmpty(projects.GetStringValueFromPath("ProjectName"), "Projects.ProjectName");
                }

                var debug_Data = body["Debug_Data"];
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("CreationDate"), "Debug_Data.CreationDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("UpdateDate"), "Debug_Data.UpdateDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("ControlUnitID"), "Debug_Data.ControlUnitID");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("LastCommunicationDelayFailureDate"), "Debug_Data.LastCommunicationDelayFailureDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Temp_LastIrrigationRecordDate"), "Debug_Data.Temp_LastIrrigationRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Temp0_LastFailureRecordDate"), "Debug_Data.Temp0_LastFailureRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Temp1_LastFailureRecordDate"), "Debug_Data.Temp1_LastFailureRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Temp0_LastEventRecordDate"), "Debug_Data.Temp0_LastEventRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Temp1_LastEventRecordDate"), "Debug_Data.Temp1_LastEventRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS_LastIrrigationRecordDate"), "Debug_Data.HHS_LastIrrigationRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS0_LastGenRecordDate"), "Debug_Data.HHS0_LastGenRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS1_LastGenRecordDate"), "Debug_Data.HHS1_LastGenRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS0_LastAlertRecordDate"), "Debug_Data.HHS0_LastAlertRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS1_LastAlertRecordDate"), "Debug_Data.HHS1_LastAlertRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("DWH_LastUpdateIrr"), "Debug_Data.DWH_LastUpdateIrr");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("DWH0_LastUpdateGen"), "Debug_Data.DWH0_LastUpdateGen");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("DWH1_LastUpdateAlert"), "Debug_Data.DWH1_LastUpdateAlert");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("DWH0_LastUpdateAlert"), "Debug_Data.DWH0_LastUpdateAlert");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("DWH1_LastUpdateGen"), "Debug_Data.DWH1_LastUpdateGen");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Temp2_LastFailureRecordDate"), "Debug_Data.Temp2_LastFailureRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS2_LastAlertRecordDate"), "Debug_Data.HHS2_LastAlertRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("DWH2_LastUpdateAlert"), "Debug_Data.DWH2_LastUpdateAlert");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("AlertMaxArrivalServerDate"), "Debug_Data.AlertMaxArrivalServerDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("LastAlertReportServerDate"), "Debug_Data.LastAlertReportServerDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("IsStartEmailSeries"), "Debug_Data.IsStartEmailSeries");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("LastDisconnectionRecordDate"), "Debug_Data.LastDisconnectionRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("Last_Cached_GenRecordsDeletionRecordDate"), "Debug_Data.Last_Cached_GenRecordsDeletionRecordDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("TotalLiters_TotalNotCalculated"), "Debug_Data.TotalLiters_TotalNotCalculated");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("HHS_LastEndIrrigationDate"), "Debug_Data.HHS_LastEndIrrigationDate");
                Assert.IsNotEmpty(debug_Data.GetStringValueFromPath("ResetManualDate"), "Debug_Data.ResetManualDate");

                foreach (var unitModelsChange in body["UnitModelsChange"])
                {
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("ModelID"), "UnitModelsChange.ModelID");
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("Name"), "UnitModelsChange.Name");
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("Version"), "UnitModelsChange.Version");
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("DefaultProgramNumber"), "UnitModelsChange.DefaultProgramNumber");
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("ValveNumbers"), "UnitModelsChange.ValveNumbers");
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("MasterValveNumber"), "UnitModelsChange.MasterValveNumber");
                    Assert.IsNotEmpty(unitModelsChange.GetStringValueFromPath("InputConditionCount"), "UnitModelsChange.InputConditionCount");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Unit info Post")]
        public void UnitInfoPost()
        {
            var unitInfoPostApi = new UnitInfoPostApi();
            var changedPayload = unitInfoPostApi.payload.ToJObject();
            changedPayload["IsGSIAg"] = false;
            unitInfoPostApi.RequestPayload = changedPayload.ToString();

            //Redis for AG/non AG & timezone & offline/online
            unitInfoPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            unitInfoPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 7,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 7,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            unitInfoPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 8,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 8,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": 2,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            unitInfoPostApi.Run();

            var responsePost = unitInfoPostApi.ResponseBody;
            Assert.IsTrue(unitInfoPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(unitInfoPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            //confirm value changed
            var unitInfoGetApi = new UnitInfoGetApi();
            unitInfoGetApi.Run();

            var response = unitInfoGetApi.ResponseBody;
            Assert.IsTrue(unitInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.AreEqual(response.GetStringValueFromPath("Body.UnitInfo.IsGSIAg"), "False");
        }

        [Test]
        [Category("Admin")]
        [Category("Unit exist")]
        public void UnitExist()
        {
            var unitExistGetApi = new UnitExistGetApi();
            unitExistGetApi.Run();

            var response = unitExistGetApi.ResponseBody;
            Assert.IsTrue(unitExistGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsFalse(response.GetBoolValueFromPath("Result"), "Service result error");
            Assert.AreEqual(response.GetStringValueFromPath("Messages[0].Message"), " RecreateUnit ");
        }

        [Test]
        [Category("Admin")]
        [Category("Unit hardware")]
        public void UnitHardware()
        {
            var unitHardwareGetApi = new UnitHardwareGetApi();
            unitHardwareGetApi.Run();

            var response = unitHardwareGetApi.ResponseBody;
            Assert.IsTrue(unitHardwareGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("NewHardware"), "NewHardware");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ModelVersion"), "ModelVersion");
            });
        }

        [Test]
        [Category("Admin")]
        [Category("CommUnits")]
        public void CommUnits()
        {
            var commUnitsPostApi = new CommUnitsPostApi();
            commUnitsPostApi.RequestPayload = commUnitsPostApi.payload;
            commUnitsPostApi.Run();

            var result = commUnitsPostApi.ResponseBody;
            Assert.IsTrue(commUnitsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach(var response in body["Response"])
                {
                    Assert.IsNotEmpty(response.GetStringValueFromPath("CommUnitID"), "Response.CommUnitID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("CreationDate"), "Response.CreationDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("UpdateDate"), "Response.UpdateDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("Name"), "Response.Name");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("SN"), "Response.SN");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("IsActive"), "Response.IsActive");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("LastConnection"), "Response.LastConnection");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("EstimatedNextConnection"), "Response.EstimatedNextConnection");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("AppVersion"), "Response.AppVersion");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("SubAppVersion"), "Response.SubAppVersion");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("Model"), "Response.Model");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("LinkedControlUnitID"), "Response.LinkedControlUnitID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("Compability"), "Response.Compability");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("IsPro"), "Response.IsPro");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("CommUnits with search")]
        public void CommUnitsWithSearch()
        {
            var commUnitsPostApi = new CommUnitsPostApi();
            var changedPayload = commUnitsPostApi.payload.ToJObject();
            var searchUnit = ConfigurationManagerWrapper.GetParameterValue("SerialNumber");
            changedPayload["Requetst"]["Search"] = searchUnit;
            changedPayload["Body"]["IncludeUnlinkedUnits"] = true;
            commUnitsPostApi.RequestPayload = changedPayload.ToString();
            commUnitsPostApi.Run();

            var response = commUnitsPostApi.ResponseBody;
            Assert.IsTrue(commUnitsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(searchUnit, response.GetStringValueFromPath("Body.Response[0].SN"));
        }
        
        [Test]
        [Category("Admin")]
        [Category("Change model")]
        public void ChangeModel()
        {
            var changeModelPostApi = new ChangeModelPostApi();

            changeModelPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 6,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 6,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            changeModelPostApi.Run();

            var responsePost = changeModelPostApi.ResponseBody;
            Assert.IsTrue(changeModelPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(changeModelPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            //confirm model changed
            var unitInfoGetApi = new UnitInfoGetApi();
            unitInfoGetApi.Run();

            var responseGet = unitInfoGetApi.ResponseBody;
            Assert.IsTrue(unitInfoGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            var unitInfo = responseGet["Body"]["UnitInfo"];
            Assert.AreEqual(unitInfo.GetStringValueFromPath("ModelID"), ConfigurationManagerWrapper.GetParameterValue("modelID"));
        }

        [Test]
        [Category("Admin")]
        [Category("Reset unit")]
        public void ResetUnit()
        {
            var resetUnitPostApi = new ResetUnitPostApi();
            resetUnitPostApi.RequestPayload = resetUnitPostApi.payload;
            resetUnitPostApi.Run();

            Assert.IsTrue(resetUnitPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(resetUnitPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Admin")]
        [Category("Add project")]
        public void AddProject()
        {
            var addProjectPostApi = new AddProjectPostApi();
            var changedPayload = addProjectPostApi.payload.ToJObject();
            var newProjectName = "project Test";
            changedPayload["Name"] = newProjectName;
            addProjectPostApi.RequestPayload = changedPayload.ToString();
            addProjectPostApi.Run();

            Assert.IsTrue(addProjectPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(addProjectPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            //Confirm project added to project list
            var projectProjectTreeGetApi = new ProjectProjectTreeGetApi();
            projectProjectTreeGetApi.Run();

            var response = projectProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(projectProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(projectProjectTreeGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsNotEmpty(response.GetStringValueFromPath("Body"));

            var tempParam = ConfigurationManagerWrapper.GetParameterValue("GSIIDDemo");
            foreach (var item in response["Body"])
            {
                if (item.GetStringValueFromPath("Name") == newProjectName)
                {
                    ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", item.GetStringValueFromPath("ID"));
                    break;
                }
            }

            // Delete project
            var deleteProjectPostApi = new DeleteProjectPostApi();
            deleteProjectPostApi.RequestPayload = deleteProjectPostApi.payload;
            deleteProjectPostApi.Run();

            var result = deleteProjectPostApi.ResponseBody;
            Assert.IsTrue(deleteProjectPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", tempParam);
        }

        [Test]
        [Category("Admin")]
        [Category("Update project")]
        public void UpdateProject()
        {
            var updateProjectPostApi = new UpdateProjectPostApi();
            var res = updateProjectPostApi.payload.ToJObject();
            var random = new Random();
            int randomNumber = random.Next(0, 1000);
            var name = "Auto test update " + randomNumber;
            res["Name"] = name;
            updateProjectPostApi.RequestPayload = res.ToString();
            updateProjectPostApi.Run();

            var responsePost = updateProjectPostApi.ResponseBody;
            Assert.IsTrue(updateProjectPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responsePost.GetBoolValueFromPath("Result"), "Service result error");

            //confirm project updated
            var projectProjectTreeChildGetApi = new ProjectProjectTreeChildGetApi(new Dictionary<string, string>
                                                { { "LogicAreaID", ConfigurationManagerWrapper.GetParameterValue("LogicAreaID") } });
            projectProjectTreeChildGetApi.Run();

            var responseGetProjects = projectProjectTreeChildGetApi.ResponseBody;
            Assert.IsTrue(projectProjectTreeChildGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseGetProjects.GetBoolValueFromPath("Result"), "Service result error");

            var result = false;
            foreach (var item in responseGetProjects["Body"])
            {
                if (item.GetStringValueFromPath("Name") == name)
                {
                    result = true;
                    break;
                }
            }

            Assert.IsTrue(result);

            // Return to original data
            res["Name"] = ConfigurationManagerWrapper.GetParameterValue("projectName");
            updateProjectPostApi.RequestPayload = res.ToString();
            updateProjectPostApi.Run();
        }

        [Test]
        [Category("Admin")]
        [Category("Link users to project")]
        public void LinkUsers()
        {
            var linkUsersPostApi = new LinkUsersPostApi();
            var res = linkUsersPostApi.payload.ToJObject();
            var userID = ConfigurationManagerWrapper.GetParameterValue("GSIIDDemo");
            res["UserID"][0] = userID;
            linkUsersPostApi.RequestPayload = res.ToString();
            linkUsersPostApi.Run();

            var responseLink = linkUsersPostApi.ResponseBody;
            Assert.IsTrue(linkUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseLink.GetBoolValueFromPath("Result"), "Service result error");

            //confirm user added to project
            var subUsersPostApi = new SubUsersPostApi(new Dictionary<string, string> { { "projectID", ConfigurationManagerWrapper.GetParameterValue("projectID") } });
            subUsersPostApi.RequestPayload = subUsersPostApi.payload;
            subUsersPostApi.Run();

            var responseSubUsers = subUsersPostApi.ResponseBody;
            Assert.IsTrue(subUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseSubUsers.GetBoolValueFromPath("Result"), "Service result error");

            var response = subUsersPostApi.ResponseBody;
            var result = false;
            foreach (var item in response.GetStringValueFromPath("Body.Response").ToJObject())
            {
                if (item.GetStringValueFromPath("UserID") == userID)
                {
                    result = true;
                    break;
                }
            }

            Assert.IsTrue(result);

            var unLinkUsersPostApi = new UnLinkUsersPostApi();
            var newPayload = unLinkUsersPostApi.payload.ToJObject();
            newPayload["UserID"][0] = userID;
            unLinkUsersPostApi.RequestPayload = newPayload.ToString();
            unLinkUsersPostApi.Run();

            var responseUnLink = unLinkUsersPostApi.ResponseBody;
            Assert.IsTrue(unLinkUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseUnLink.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Admin")]
        [Category("UnLink Users from project")]
        public void UnLinkUsers()
        {
            var linkUsersPostApi = new LinkUsersPostApi();
            var resLink = linkUsersPostApi.payload.ToJObject();
            var userID = ConfigurationManagerWrapper.GetParameterValue("GSIIDDemo");
            resLink["UserID"][0] = userID;
            linkUsersPostApi.RequestPayload = resLink.ToString();
            linkUsersPostApi.Run();

            var responseLink = linkUsersPostApi.ResponseBody;
            Assert.IsTrue(linkUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseLink.GetBoolValueFromPath("Result"), "Service result error");

            //confirm user added to project
            var subUsersPostApi = new SubUsersPostApi(new Dictionary<string, string> { { "projectID", ConfigurationManagerWrapper.GetParameterValue("projectID") } });
            subUsersPostApi.RequestPayload = subUsersPostApi.payload;
            subUsersPostApi.Run();

            var responseSubUsersLink = subUsersPostApi.ResponseBody;
            var resultLink = false;
            foreach (var item in responseSubUsersLink.GetStringValueFromPath("Body.Response").ToJObject())
            {
                if (item.GetStringValueFromPath("UserID") == userID)
                {
                    resultLink = true;
                    break;
                }
            }

            Assert.IsTrue(resultLink);

            // Unlink user from project
            var unLinkUsersPostApi = new UnLinkUsersPostApi();
            var resUnLink = unLinkUsersPostApi.payload.ToJObject();
            resUnLink["UserID"][0] = userID;
            unLinkUsersPostApi.RequestPayload = resUnLink.ToString();
            unLinkUsersPostApi.Run();

            var response = unLinkUsersPostApi.ResponseBody;
            Assert.IsTrue(unLinkUsersPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            //confirm user deleted to project
            subUsersPostApi.RequestPayload = subUsersPostApi.payload;
            subUsersPostApi.Run();

            var responseSubUsersUnLink = subUsersPostApi.ResponseBody;
            var resultUnLink = true;
            foreach (var item in responseSubUsersUnLink.GetStringValueFromPath("Body.Response").ToJObject())
            {
                if (item.GetStringValueFromPath("UserID") == userID)
                {
                    resultUnLink = false;
                    break;
                }
            }

            Assert.IsTrue(resultUnLink);
        }

        [Test]
        [Category("Admin")]
        [Category("Project ProjectTree")]
        public void ProjectProjectTree()
        {
            var projectProjectTreeGetApi = new ProjectProjectTreeGetApi();
            projectProjectTreeGetApi.Run();

            var response = projectProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(projectProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(projectProjectTreeGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var body in response["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("Serch project tree")]
        public void SerchProjectTree()
        {
            var serchProjectTreeGetApi = new SerchProjectTreeGetApi(new Dictionary<string, string> { { "SearchString", "auto" } });
            serchProjectTreeGetApi.Run();

            var response = serchProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(serchProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(serchProjectTreeGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var body in response["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("rank"), "rank");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Linked"), "Linked");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("havechild"), "havechild");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("System message list")]
        public void SystemMessageList()
        {
            var systemMessageListGetApi = new SystemMessageListGetApi();
            systemMessageListGetApi.Run();

            var result = systemMessageListGetApi.ResponseBody;
            Assert.IsTrue(systemMessageListGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var body in result["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("NameFileMassage"), "NameFileMassage");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("Flag"), "Flag");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ToAll"), "ToAll");
                }
            });
        }

        [Test]
        [Category("Admin")]
        [Category("User read message")]
        public void UserReadMassage()
        {
            var userReadMessagePostApi = new UserReadMessagePostApi(new Dictionary<string, string> { { "ReadMassage", "true" } });
            userReadMessagePostApi.Run();

            Assert.IsTrue(userReadMessagePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(userReadMessagePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test][Category("Admin")]
        [Category("Admin")]
        [Category("Delete project")]
        public void DeleteProject()
        {
            var addProjectPostApi = new AddProjectPostApi();
            var changedPayload = addProjectPostApi.payload.ToJObject();
            var newProjectName = "project Test";
            changedPayload["Name"] = newProjectName;
            addProjectPostApi.RequestPayload = changedPayload.ToString();
            addProjectPostApi.Run();

            Assert.IsTrue(addProjectPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(addProjectPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            //Confirm project added to project list
            var projectProjectTreeGetApi = new ProjectProjectTreeGetApi();
            projectProjectTreeGetApi.Run();

            var response = projectProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(projectProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(projectProjectTreeGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var tempParam = ConfigurationManagerWrapper.GetParameterValue("GSIIDDemo");
            foreach (var item in response["Body"])
            {
                if (item.GetStringValueFromPath("Name") == newProjectName)
                {
                    ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", item.GetStringValueFromPath("ID"));
                    break;
                }
            }

            // Delete project
            var deleteProjectPostApi = new DeleteProjectPostApi();
            deleteProjectPostApi.RequestPayload = deleteProjectPostApi.payload;
            deleteProjectPostApi.Run();

            var responseDelete = deleteProjectPostApi.ResponseBody;
            Assert.IsTrue(deleteProjectPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseDelete.GetBoolValueFromPath("Result"), "Service result error");

            ConfigurationManagerWrapper.SetParameterValue("GSIIDDemo", tempParam);

            //Confirm project deleted
            projectProjectTreeGetApi.Run();

            var responseGetDelete = projectProjectTreeGetApi.ResponseBody;
            Assert.IsTrue(projectProjectTreeGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(projectProjectTreeGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = true;
            foreach (var item in responseGetDelete["Body"])
            {
                if (item.GetStringValueFromPath("Name") == newProjectName)
                {
                    result = false;
                    break;
                }
            }

            Assert.IsTrue(result);
        }
    }
}
