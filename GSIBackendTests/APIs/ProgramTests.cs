﻿using GSITestInfrastructure.GSIApis.Program;
using GSITestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class ProgramTests
    {
        [Test]
        [Category("Program")]
        [Category("Settings Get")]
        public void SettingsGet()
        {
            var settingsGetApi = new SettingsGetApi();
            settingsGetApi.Run();

            var result = settingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(settingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var mainSettings = result["MainSettings"];
                var config = mainSettings["Config"];
                Assert.IsNotEmpty(config.GetStringValueFromPath("SN"), "Config.SN");
                Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("SerialNumber"), config.GetStringValueFromPath("SN"), "Config.SN - compare");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ControlUnitID"), "Config.ControlUnitID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitName"), "Config.UnitName");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelID"), "Config.ModelID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelView"), "Config.ModelView");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelVersion"), "Config.ModelVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("WaterFactor"), "Config.WaterFactor");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Signal"), "Config.Signal");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ValveNum"), "Config.ValveNum");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CommTypeID"), "Config.CommTypeID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Status"), "Config.Status");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CurrentConfigID"), "Config.CurrentConfigID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CommUnit_AppVersion"), "Config.CommUnit_AppVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("BootLoaderVersion"), "Config.BootLoaderVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LandTypeID"), "Config.LandTypeID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonBudget"), "Config.SeasonBudget");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsGSIAg"), "Config.IsGSIAg");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Description"), "Config.Description");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Address"), "Config.Address");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Phone"), "Config.Phone");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonEndDate"), "Config.SeasonEndDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonStartDate"), "Config.SeasonStartDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonEndDateTicks"), "Config.SeasonEndDateTicks");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonStartDateTicks"), "Config.SeasonStartDateTicks");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ConnectionStatus"), "Config.ConnectionStatus");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ControllerState"), "Config.ControllerState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("InputsState"), "Config.InputsState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("OutputState"), "Config.OutputState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ShortedValve"), "Config.ShortedValve");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsSync"), "Config.IsSync");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DeviceAlarm"), "Config.DeviceAlarm");
                Assert.IsNotEmpty(config.GetStringValueFromPath("RainChance"), "Config.RainChance");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Map_Latitude"), "Config.Map_Latitude");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Map_Longitude"), "Config.Map_Longitude");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ETmanual"), "Config.ETmanual");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ET0"), "Config.ET0");
                Assert.IsNotEmpty(config.GetStringValueFromPath("FlagET"), "Config.FlagET");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsPRO"), "Config.IsPRO");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LineNum"), "Config.LineNum");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DigitalAnalog1"), "Config.DigitalAnalog1");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DigitalAnalog2"), "Config.DigitalAnalog2");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DigitalAnalog3"), "Config.DigitalAnalog3");
                Assert.IsNotEmpty(config.GetStringValueFromPath("TimeZoneID"), "Config.TimeZoneID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsMetric"), "Config.IsMetric");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LandType"), "Config.LandType");
                Assert.IsNotEmpty(config.GetStringValueFromPath("NewHardware"), "Config.NewHardware");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitPulseView"), "Config.UnitPulseView");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CorrectVersion"), "Config.CorrectVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LocalProgramWaterUnit"), "Config.LocalProgramWaterUnit");
                Assert.IsNotEmpty(config.GetStringValueFromPath("WaterFactorUnit"), "Config.WaterFactorUnit");
                Assert.IsNotEmpty(config.GetStringValueFromPath("PaymentAlert"), "Config.PaymentAlert");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitPaymentExpirationDate"), "Config.UnitPaymentExpirationDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitLastPaymentDate"), "Config.UnitLastPaymentDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitPaymentStatus"), "Config.UnitPaymentStatus");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnpaidUnit"), "Config.UnpaidUnit");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IrrimaxApiKey"), "Config.IrrimaxApiKey");
                Assert.IsNotEmpty(config.GetStringValueFromPath("NewVer"), "Config.NewVer");
                foreach (var listOfVersions in config["ListOfVersions"])
                {
                    Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("VersionType"), "Config.ListOfVersions.VersionType");
                    Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("Version"), "Config.ListOfVersions.Version");
                    Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("TypeOfController"), "Config.ListOfVersions.NewVer");
                }

                foreach (var valve in mainSettings["Valves"])
                {
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveName"), "Valves.ValveName");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveColor"), "Valves.ValveColor");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Precipitation_AllowDuration"), "Valves.Precipitation_AllowDuration");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Precipitation_AllowQuantity"), "Valves.Precipitation_AllowQuantity");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveID"), "Valves.ValveID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LandTypeID"), "Valves.LandTypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("TypeID"), "Valves.TypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Status"), "Valves.Status");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("OutputNumber"), "Valves.OutputNumber");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("PrecipitationRate"), "Valves.PrecipitationRate");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IrrigrationArea"), "Valves.IrrigrationArea");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveAlarm"), "Valves.ValveAlarm");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveSetup"), "Valves.ValveSetup");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LineNum"), "Valves.LineNum");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterMeter"), "Valves.WaterMeter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreWaterCounter"), "Valves.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreMasterValve"), "Valves.IgnoreMasterValve");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("AuxiliaryOutput"), "Valves.AuxiliaryOutput");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterMeterType"), "Valves.WaterMeterType");
                }

                var validDays = mainSettings["ValidDays"];
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSundayState"), "ValidDays.DSundayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DMondayState"), "ValidDays.DMondayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DTuesdayState"), "ValidDays.DTuesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DWednesdayState"), "ValidDays.DWednesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DThursdayState"), "ValidDays.DThursdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DFridayState"), "ValidDays.DFridayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSaturdayState"), "ValidDays.DSaturdayState");

                var waterMeter = mainSettings["WaterMeter"];
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseSize"), "WaterMeter.PulseSize");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("MeterType"), "WaterMeter.MeterType");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("IsEnabled"), "WaterMeter.IsEnabled");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseTypeID"), "WaterMeter.PulseTypeID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("FlowTypeID"), "WaterMeter.FlowTypeID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("NoWaterPulseDelay"), "WaterMeter.NoWaterPulseDelay");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LeakageLimit"), "WaterMeter.LeakageLimit");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("InputNumber"), "WaterMeter.InputNumber");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterType"), "WaterMeter.WaterMeterType");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseViewTypeID"), "WaterMeter.PulseViewTypeID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterNumber"), "WaterMeter.WaterMeterNumber");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LineNum"), "WaterMeter.LineNum");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterID"), "WaterMeter.WaterMeterID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("EnableAutoCancleAlarm"), "WaterMeter.EnableAutoCancleAlarm");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("AlertsAutoResetTime"), "WaterMeter.AlertsAutoResetTime");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("ReleaseWaterLeakageFault"), "WaterMeter.ReleaseWaterLeakageFault");

                var fertilizer = mainSettings["Fertilizer"];
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseCustomFertilizerPulse"), "Fertilizer.UseCustomFertilizerPulse");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumber"), "Fertilizer.OutputNumber");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("ContinuousFert"), "Fertilizer.ContinuousFert");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseSize"), "Fertilizer.PulseSize");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseViewTypeID"), "Fertilizer.PulseViewTypeID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertPulseDuration"), "Fertilizer.FertPulseDuration");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FerlizerFaillureTime"), "Fertilizer.FerlizerFaillureTime");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("Leakage"), "Fertilizer.Leakage");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerType"), "Fertilizer.FertilizerType");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.FertPulseType"), "Fertilizer.PulseTypeID.FertPulseType");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.Quant"), "Fertilizer.PulseTypeID.Quant");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FlowTypeID"), "Fertilizer.FlowTypeID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("IsEnabled"), "Fertilizer.IsEnabled");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("NominalFlow"), "Fertilizer.NominalFlow");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerID"), "Fertilizer.FertilizerID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertCounterInputNum"), "Fertilizer.FertCounterInputNum");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseFertMeter4Error"), "Fertilizer.UseFertMeter4Error");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertLeakDetectActive"), "Fertilizer.FertLeakDetectActive");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseRelay"), "Fertilizer.UseRelay");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseMaster"), "Fertilizer.UseMaster");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumberMaster"), "Fertilizer.OutputNumberMaster");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertNumber"), "Fertilizer.FertNumber");

                var rainSensor = mainSettings["RainSensor"];
                if(rainSensor.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("IsEnabled"), "RainSensor.IsEnabled");
                    Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("DaysStopper"), "RainSensor.DaysStopper");
                    Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("RainDetectorNC"), "RainSensor.RainDetectorNC");
                    Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("ConditionNumber"), "RainSensor.ConditionNumber");
                }

                var fertPrograms = mainSettings["FertPrograms"];
                if (fertPrograms.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(fertPrograms.GetStringValueFromPath("ID"), "FertPrograms.ID");
                    Assert.IsNotEmpty(fertPrograms.GetStringValueFromPath("Name"), "FertPrograms.Name");
                }
                
                foreach(var condition in mainSettings["Condition"])
                {
                    Assert.IsNotEmpty(condition.GetStringValueFromPath("TypeID"), "Condition.TypeID");
                    Assert.IsNotEmpty(condition.GetStringValueFromPath("Name"), "Condition.Name");
                }

                var program = result["Program"];
                Assert.IsNotEmpty(program.GetStringValueFromPath("Name"), "Program.Name");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramID"), "Program.ProgramID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramNumber"), "Program.ProgramNumber");
                Assert.IsNotEmpty(program.GetStringValueFromPath("Priority"), "Program.Priority");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ConfigID"), "Program.ConfigID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("IsFertilizerON"), "Program.IsFertilizerON");
                Assert.IsNotEmpty(program.GetStringValueFromPath("PulseViewTypeID"), "Program.PulseViewTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("UnitPulseViewTypeID"), "Program.UnitPulseViewTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramSetup"), "Program.ProgramSetup");
                Assert.IsNotEmpty(program.GetStringValueFromPath("NextIrrigationDay"), "Program.NextIrrigationDay");
                Assert.IsNotEmpty(program.GetStringValueFromPath("NextIrrigationTime"), "Program.NextIrrigationTime");
                Assert.IsNotEmpty(program.GetStringValueFromPath("LineNum"), "Program.LineNum");
                Assert.IsNotEmpty(program.GetStringValueFromPath("MainSecondaryValveID"), "Program.MainSecondaryValveID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramStatus"), "Program.ProgramStatus");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramWaterUnit"), "Program.ProgramWaterUnit");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ClassificationTypeID"), "Program.ClassificationTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("Status"), "Program.Status");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramType"), "Program.ProgramType");
                Assert.IsNotEmpty(program.GetStringValueFromPath("FertExecutionType"), "Program.FertExecutionType");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WaterFactor"), "Program.WaterFactor");
                Assert.IsNotEmpty(program.GetStringValueFromPath("PlantTypeID"), "Program.PlantTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DSundayState"), "Program.WeeklyDaysProgram.DSundayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DMondayState"), "Program.WeeklyDaysProgram.DMondayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DTuesdayState"), "Program.WeeklyDaysProgram.DTuesdayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DWednesdayState"), "Program.WeeklyDaysProgram.DWednesdayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DThursdayState"), "Program.WeeklyDaysProgram.DThursdayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DFridayState"), "Program.WeeklyDaysProgram.DFridayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DSaturdayState"), "Program.WeeklyDaysProgram.DSaturdayState");

                foreach (var executionHours in program["ExecutionHours"])
                {
                    Assert.IsNotEmpty(executionHours.GetStringValueFromPath(""), "Program.executionHours");
                }

                Assert.IsNotEmpty(program.GetStringValueFromPath("HoursExecution"), "Program.HoursExecution");
                Assert.IsNotEmpty(program.GetStringValueFromPath("DaysExecutionType"), "Program.DaysExecutionType");

                var cyclicDayProgram = program["CyclicDayProgram"];
                Assert.IsNotEmpty(cyclicDayProgram.GetStringValueFromPath("StartDate"), "Program.CyclicDayProgram.StartDate");
                Assert.IsNotEmpty(cyclicDayProgram.GetStringValueFromPath("DaysInterval"), "Program.CyclicDayProgram.DaysInterval");

                var hourlyCycle = program["HourlyCycle"];
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesPerDay"), "Program.HourlyCycle.HourlyCyclesPerDay");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesStartTime"), "Program.HourlyCycle.HourlyCyclesStartTime");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCycleTime"), "Program.HourlyCycle.HourlyCycleTime");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesStopTime"), "Program.HourlyCycle.HourlyCyclesStopTime");

                var advanced = program["Advanced"];
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStartDateLimit"), "Program.Advanced.FinalStartDateLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalEndDateLimit"), "Program.Advanced.FinalEndDateLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStartHoursLimit"), "Program.Advanced.FinalStartHoursLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStopHoursLimit"), "Program.Advanced.FinalStopHoursLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("RunAndFinish"), "Program.Advanced.RunAndFinish");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("RunWhenActive"), "Program.Advanced.RunWhenActive");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("TerminateProgram"), "Program.Advanced.TerminateProgram");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("PauseWhenActive"), "Program.Advanced.PauseWhenActive");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("MaxWaitForStartCond"), "Program.Advanced.MaxWaitForStartCond");

                foreach(var valveInProgram in program["ValveInProgram"])
                {
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ProgramID"), "Program.ValveInProgram.ProgramID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OutputNumber"), "Program.ValveInProgram.OutputNumber");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OrderNumber"), "Program.ValveInProgram.OrderNumber");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ID"), "Program.ValveInProgram.ID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterQuantity"), "Program.ValveInProgram.WaterQuantity");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertQuant"), "Program.ValveInProgram.FertQuant");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertTime"), "Program.ValveInProgram.FertTime");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("Name"), "Program.ValveInProgram.Name");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterBefore"), "Program.ValveInProgram.WaterBefore");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterAfter"), "Program.ValveInProgram.WaterAfter");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterDuration"), "Program.ValveInProgram.WaterDuration");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterPrecipitation"), "Program.ValveInProgram.WaterPrecipitation");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("SecondaryValveID"), "Program.ValveInProgram.SecondaryValveID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveID"), "Program.ValveInProgram.ValveID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveType"), "Program.ValveInProgram.ValveType");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ET"), "Program.ValveInProgram.ET");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IsDeleted"), "Program.ValveInProgram.IsDeleted");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertProgram"), "Program.ValveInProgram.FertProgram");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("PulseViewTypeID"), "Program.ValveInProgram.PulseViewTypeID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IgnoreWaterCounter"), "Program.ValveInProgram.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IgnoreMasterValve"), "Program.ValveInProgram.IgnoreMasterValve");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("AuxiliaryOutput"), "Program.ValveInProgram.AuxiliaryOutput");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("LineNum"), "Program.ValveInProgram.LineNum");

                    var unitPulseType = program["UnitPulseType"];
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("PulseTypeViewID"), "Program.UnitPulseType.PulseTypeViewID");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("PulseTypeID"), "Program.UnitPulseType.PulseTypeID");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("Name"), "Program.UnitPulseType.Name");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("IsMetric"), "Program.UnitPulseType.IsMetric");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("IsDefault"), "Program.UnitPulseType.IsDefault");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("ViewFormat"), "Program.UnitPulseType.ViewFormat");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("DecimalPlaces"), "Program.UnitPulseType.DecimalPlaces");

                    foreach (var condition in program["Conditions"])
                    {
                        Assert.IsNotEmpty(condition.GetStringValueFromPath(""), "Program.Conditions");
                    }

                    foreach (var classificationType in result["ClassificationType"])
                    {
                        Assert.IsNotEmpty(classificationType.GetStringValueFromPath("TypeID"), "ClassificationType.TypeID");
                        Assert.IsNotEmpty(classificationType.GetStringValueFromPath("Name"), "ClassificationType.Name");
                        Assert.IsNotEmpty(classificationType.GetStringValueFromPath("Permanent"), "ClassificationType.Permanent");
                    }
                }

            });

        }

        [TestCase("TryChangingName")]
        [TestCase("Program1")]
        [Test]
        [Category("Program")]
        [Category("Settings Post")]
        public void SettingsPost(string newProgramName)
        {
            var settingsPostApi = new SettingsPostApi();
            var changedPayload = settingsPostApi.payload.ToJObject();
            changedPayload["Name"] = newProgramName;
            settingsPostApi.RequestPayload = changedPayload.ToString();


            settingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));



            settingsPostApi.Run();
            
            Assert.IsTrue(settingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(settingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            //Get api to check Post methode
            var settingsGetApi = new SettingsGetApi();
            settingsGetApi.Run();

            var result = settingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(settingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(newProgramName, result.GetStringValueFromPath("Program.Name"), "Program.Name");
        } 

        [Test]
        [Category("Program")]
        [Category("Settings pro Get")]
        public void SettingsProGet()
        {
            var settingsProGetApi = new SettingsProGetApi();
            settingsProGetApi.Run();

            var result = settingsProGetApi.ResponseBody["Body"];
            Assert.IsTrue(settingsProGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsProGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var mainSettings = result["MainSettings"];
                var deviceInfo = mainSettings["DeviceInfo"];
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SN"), "DeviceInfo.SN");
                Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("ProSerialNumber"), deviceInfo.GetStringValueFromPath("SN"), "DeviceInfo.SN - compare");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ControlUnitID"), "DeviceInfo.ControlUnitID");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitName"), "DeviceInfo.UnitName");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelID"), "DeviceInfo.ModelID");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Status"), "DeviceInfo.Status");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LandTypeID"), "DeviceInfo.LandTypeID");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsGSIAg"), "DeviceInfo.IsGSIAg");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsPRO"), "DeviceInfo.IsPRO");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LineNum"), "DeviceInfo.LineNum");
                Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsMetric"), "DeviceInfo.IsMetric");

                foreach (var valve in mainSettings["Valves"])
                {
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveName"), "Valves.ValveName");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("OutputNumber"), "Valves.OutputNumber");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveColor"), "Valves.ValveColor");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("StatusID"), "Valves.StatusID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Status"), "Valves.Status");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("TypeID"), "Valves.TypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IrrigrationArea"), "Valves.IrrigrationArea");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LandTypeID"), "Valves.LandTypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("PrecipitationRate"), "Valves.PrecipitationRate");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("FertilizerConnected"), "Valves.FertilizerConnected");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveID"), "Valves.ValveID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveAlarm"), "Valves.ValveAlarm");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LineNum"), "Valves.LineNum");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("FlowViewTypeID"), "Valves.FlowViewTypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterFactor"), "Valves.WaterFactor");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterCounterNum"), "Valves.WaterCounterNum");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("PulseViewTypeID"), "Valves.PulseViewTypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreWaterCounter"), "Valves.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreMasterValve"), "Valves.IgnoreMasterValve");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Precipitation_AllowDuration"), "Valves.Precipitation_AllowDuration");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Precipitation_AllowQuantity"), "Valves.Precipitation_AllowQuantity");
                }

                var validDays = mainSettings["ValidDays"];
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSundayState"), "ValidDays.DSundayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DMondayState"), "ValidDays.DMondayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DTuesdayState"), "ValidDays.DTuesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DWednesdayState"), "ValidDays.DWednesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DThursdayState"), "ValidDays.DThursdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DFridayState"), "ValidDays.DFridayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSaturdayState"), "ValidDays.DSaturdayState");

                foreach (var waterMeter in mainSettings["WaterMeter"])
                {
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseSize"), "WaterMeter.PulseSize");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("MeterType"), "WaterMeter.MeterType");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("IsEnabled"), "WaterMeter.IsEnabled");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseTypeID"), "WaterMeter.PulseTypeID");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("FlowTypeID"), "WaterMeter.FlowTypeID");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("NoWaterPulseDelay"), "WaterMeter.NoWaterPulseDelay");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LeakageLimit"), "WaterMeter.LeakageLimit");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("InputNumber"), "WaterMeter.InputNumber");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterType"), "WaterMeter.WaterMeterType");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseViewTypeID"), "WaterMeter.PulseViewTypeID");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterNumber"), "WaterMeter.WaterMeterNumber");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LineNum"), "WaterMeter.LineNum");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterID"), "WaterMeter.WaterMeterID");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("EnableAutoCancleAlarm"), "WaterMeter.EnableAutoCancleAlarm");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("AlertsAutoResetTime"), "WaterMeter.AlertsAutoResetTime");
                    Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("ReleaseWaterLeakageFault"), "WaterMeter.ReleaseWaterLeakageFault");
                }

                foreach(var fertPrograms in mainSettings["FertPrograms"])
                {
                    Assert.IsNotEmpty(fertPrograms.GetStringValueFromPath("FertCenterNum"), "FertPrograms.FertCenterNum");
                    Assert.IsNotEmpty(fertPrograms.GetStringValueFromPath("ProgramNum"), "FertPrograms.ProgramNum");
                    Assert.IsNotEmpty(fertPrograms.GetStringValueFromPath("Name"), "FertPrograms.Name");
                    Assert.IsNotEmpty(fertPrograms.GetStringValueFromPath("FertProgramID"), "FertPrograms.FertProgramID");
                }

                foreach (var condition in mainSettings["Condition"])
                {
                    Assert.IsNotEmpty(condition.GetStringValueFromPath("TypeID"), "Condition.TypeID");
                    Assert.IsNotEmpty(condition.GetStringValueFromPath("Name"), "Condition.Name");
                }

                Assert.IsNotEmpty(mainSettings.GetStringValueFromPath("EnableFertilizer"), "MainSettings.EnableFertilizer");

                var program = result["Program"];
                Assert.IsNotEmpty(program.GetStringValueFromPath("Name"), "Program.Name");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramID"), "Program.ProgramID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramNumber"), "Program.ProgramNumber");
                Assert.IsNotEmpty(program.GetStringValueFromPath("Priority"), "Program.Priority");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ConfigID"), "Program.ConfigID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("IsFertilizerON"), "Program.IsFertilizerON");
                Assert.IsNotEmpty(program.GetStringValueFromPath("PulseViewTypeID"), "Program.PulseViewTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("UnitPulseViewTypeID"), "Program.UnitPulseViewTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramSetup"), "Program.ProgramSetup");
                Assert.IsNotEmpty(program.GetStringValueFromPath("NextIrrigationDay"), "Program.NextIrrigationDay");
                Assert.IsNotEmpty(program.GetStringValueFromPath("NextIrrigationTime"), "Program.NextIrrigationTime");
                Assert.IsNotEmpty(program.GetStringValueFromPath("LineNum"), "Program.LineNum");
                Assert.IsNotEmpty(program.GetStringValueFromPath("MainSecondaryValveID"), "Program.MainSecondaryValveID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramStatus"), "Program.ProgramStatus");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramWaterUnit"), "Program.ProgramWaterUnit");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ClassificationTypeID"), "Program.ClassificationTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("Status"), "Program.Status");
                Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramType"), "Program.ProgramType");
                Assert.IsNotEmpty(program.GetStringValueFromPath("FertExecutionType"), "Program.FertExecutionType");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WaterFactor"), "Program.WaterFactor");
                Assert.IsNotEmpty(program.GetStringValueFromPath("PlantTypeID"), "Program.PlantTypeID");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DSundayState"), "Program.WeeklyDaysProgram.DSundayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DMondayState"), "Program.WeeklyDaysProgram.DMondayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DTuesdayState"), "Program.WeeklyDaysProgram.DTuesdayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DWednesdayState"), "Program.WeeklyDaysProgram.DWednesdayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DThursdayState"), "Program.WeeklyDaysProgram.DThursdayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DFridayState"), "Program.WeeklyDaysProgram.DFridayState");
                Assert.IsNotEmpty(program.GetStringValueFromPath("WeeklyDaysProgram.DSaturdayState"), "Program.WeeklyDaysProgram.DSaturdayState");

                foreach (var executionHours in program["ExecutionHours"])
                {
                    Assert.IsNotEmpty(executionHours.GetStringValueFromPath(""), "Program.executionHours");
                }

                Assert.IsNotEmpty(program.GetStringValueFromPath("HoursExecution"), "Program.HoursExecution");
                Assert.IsNotEmpty(program.GetStringValueFromPath("DaysExecutionType"), "Program.DaysExecutionType");

                var cyclicDayProgram = program["CyclicDayProgram"];
                Assert.IsNotEmpty(cyclicDayProgram.GetStringValueFromPath("StartDate"), "Program.CyclicDayProgram.StartDate");
                Assert.IsNotEmpty(cyclicDayProgram.GetStringValueFromPath("DaysInterval"), "Program.CyclicDayProgram.DaysInterval");

                var hourlyCycle = program["HourlyCycle"];
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesPerDay"), "Program.HourlyCycle.HourlyCyclesPerDay");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesStartTime"), "Program.HourlyCycle.HourlyCyclesStartTime");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCycleTime"), "Program.HourlyCycle.HourlyCycleTime");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesStopTime"), "Program.HourlyCycle.HourlyCyclesStopTime");

                var advanced = program["Advanced"];
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStartDateLimit"), "Program.Advanced.FinalStartDateLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalEndDateLimit"), "Program.Advanced.FinalEndDateLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStartHoursLimit"), "Program.Advanced.FinalStartHoursLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStopHoursLimit"), "Program.Advanced.FinalStopHoursLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("RunAndFinish"), "Program.Advanced.RunAndFinish");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("RunWhenActive"), "Program.Advanced.RunWhenActive");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("TerminateProgram"), "Program.Advanced.TerminateProgram");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("PauseWhenActive"), "Program.Advanced.PauseWhenActive");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("MaxWaitForStartCond"), "Program.Advanced.MaxWaitForStartCond");

                foreach (var valveInProgram in program["ValveInProgram"])
                {
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ProgramID"), "Program.ValveInProgram.ProgramID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OutputNumber"), "Program.ValveInProgram.OutputNumber");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OrderNumber"), "Program.ValveInProgram.OrderNumber");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ID"), "Program.ValveInProgram.ID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterQuantity"), "Program.ValveInProgram.WaterQuantity");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertQuant"), "Program.ValveInProgram.FertQuant");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertTime"), "Program.ValveInProgram.FertTime");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("Name"), "Program.ValveInProgram.Name");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterBefore"), "Program.ValveInProgram.WaterBefore");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterAfter"), "Program.ValveInProgram.WaterAfter");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterDuration"), "Program.ValveInProgram.WaterDuration");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterPrecipitation"), "Program.ValveInProgram.WaterPrecipitation");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("SecondaryValveID"), "Program.ValveInProgram.SecondaryValveID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveID"), "Program.ValveInProgram.ValveID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveType"), "Program.ValveInProgram.ValveType");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ET"), "Program.ValveInProgram.ET");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IsDeleted"), "Program.ValveInProgram.IsDeleted");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertProgram"), "Program.ValveInProgram.FertProgram");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("PulseViewTypeID"), "Program.ValveInProgram.PulseViewTypeID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IgnoreWaterCounter"), "Program.ValveInProgram.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IgnoreMasterValve"), "Program.ValveInProgram.IgnoreMasterValve");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("AuxiliaryOutput"), "Program.ValveInProgram.AuxiliaryOutput");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("LineNum"), "Program.ValveInProgram.LineNum");
                }

                var unitPulseType = program["UnitPulseType"];
                if(unitPulseType.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("PulseTypeViewID"), "Program.UnitPulseType.PulseTypeViewID");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("PulseTypeID"), "Program.UnitPulseType.PulseTypeID");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("Name"), "Program.UnitPulseType.Name");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("IsMetric"), "Program.UnitPulseType.IsMetric");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("IsDefault"), "Program.UnitPulseType.IsDefault");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("ViewFormat"), "Program.UnitPulseType.ViewFormat");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("DecimalPlaces"), "Program.UnitPulseType.DecimalPlaces");
                }
                
                var programPulseType = program["ProgramPulseType"];
                if (programPulseType.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("PulseTypeViewID"), "Program.ProgramPulseType.PulseTypeViewID");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("PulseTypeID"), "Program.ProgramPulseType.PulseTypeID");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("Name"), "Program.ProgramPulseType.Name");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("IsMetric"), "Program.ProgramPulseType.IsMetric");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("IsDefault"), "Program.ProgramPulseType.IsDefault");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("ViewFormat"), "Program.ProgramPulseType.ViewFormat");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("DecimalPlaces"), "Program.ProgramPulseType.DecimalPlaces");
                }

                foreach (var condition in program["Conditions"])
                {
                    Assert.IsNotEmpty(condition.GetStringValueFromPath(""), "Program.Conditions");
                }

                foreach (var classificationType in result["ClassificationType"])
                {
                    Assert.IsNotEmpty(classificationType.GetStringValueFromPath("TypeID"), "ClassificationType.TypeID");
                    Assert.IsNotEmpty(classificationType.GetStringValueFromPath("Name"), "ClassificationType.Name");
                    Assert.IsNotEmpty(classificationType.GetStringValueFromPath("Permanent"), "ClassificationType.Permanent");
                }
            });
        }

        [TestCase("TryChangingName")]
        [TestCase("Program1")]
        [Test]
        [Category("Program")]
        [Category("Settings pro Post")]
        public void SettingsProPost(string newProgramName)
        {
            var settingsProPostApi = new SettingsProPostApi();
            var changedPayload = settingsProPostApi.payload.ToJObject();
            changedPayload["Name"] = newProgramName;
            settingsProPostApi.RequestPayload = changedPayload.ToString();

            settingsProPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("ProSerialNumber")));

            settingsProPostApi.Run();
            
            Assert.IsTrue(settingsProPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(settingsProPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsProPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            //Get api to check Post methode
            var settingsProGetApi = new SettingsProGetApi();
            settingsProGetApi.Run();

            var result = settingsProGetApi.ResponseBody["Body"];
            Assert.IsTrue(settingsProGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsProGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(newProgramName, result.GetStringValueFromPath("Program.Name"), "Program.Name");
        }

        [Test]
        [Category("Program")]
        [Category("Schedule")]
        public void Schedule()
        {
            var scheduledGetApi = new ScheduledGetApi(new Dictionary<string, string> { { "StartTicks", "1643407200000" }, { "EndTicks", "164409839999" } });
            scheduledGetApi.Run();

            var result = scheduledGetApi.ResponseBody;
            Assert.IsTrue(scheduledGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UnitName"), "UnitName");

                var validDays = body["ValidDays"];
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSundayState"), "DSundayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DMondayState"), "DMondayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DTuesdayState"), "DTuesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DWednesdayState"), "DWednesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DThursdayState"), "DThursdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DFridayState"), "DFridayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSaturdayState"), "DSaturdayState");

                foreach(var exceptionalDates in body["ExceptionalDates"])
                {
                    Assert.IsNotEmpty(exceptionalDates.GetStringValueFromPath(""), "ExceptionalDates");
                }

                foreach (var instances in body["Instances"])
                {
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("Name"), "Instances.Name");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("ProgramID"), "Instances.ProgramID");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("ProgramNumber"), "Instances.ProgramNumber");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("ConfigID"), "Instances.ConfigID");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("IsFertilizerON"), "Instances.IsFertilizerON");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("FertExecutionType"), "Instances.FertExecutionType");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("StartTicks"), "Instances.StartTicks");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("StartDate"), "Instances.StartDate");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("EndTicks"), "Instances.EndTicks");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("EndDate"), "Instances.EndDate");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("Duration"), "Instances.Duration");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("Quantity"), "Instances.Quantity");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("ProgramType"), "Instances.ProgramType");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("HoursExecutionType"), "Instances.HoursExecutionType");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("DaysExecutionType"), "Instances.DaysExecutionType");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("Interval"), "Instances.Interval");
                    Assert.IsNotEmpty(instances.GetStringValueFromPath("Interval_total"), "Instances.Interval_total");
                    
                    foreach(var valves in instances["Valves"])
                    {
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("StartTime"), "Valves.StartTime");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("EndTime"), "Valves.EndTime");

                        foreach (var valve in valves["Valve"])
                        {
                            Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveNum"), "Valve.ValveNum");
                            Assert.IsNotEmpty(valve.GetStringValueFromPath("Name"), "Valve.Name");
                            Assert.IsNotEmpty(valve.GetStringValueFromPath("Color"), "Valve.Color");
                            Assert.IsNotEmpty(valve.GetStringValueFromPath("OrderNumber"), "Valve.OrderNumber");
                            Assert.IsNotEmpty(valve.GetStringValueFromPath("LineNum"), "Valve.LineNum");
                            Assert.IsNotEmpty(valve.GetStringValueFromPath("FertProgram"), "Valve.FertProgram");
                        }

                        Assert.IsNotEmpty(valves.GetStringValueFromPath("Duration"), "Valves.Duration");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("ValveProportion"), "Valves.ValveProportion");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("FertQuant"), "Valves.FertQuant");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("FertTime"), "Valves.FertTime");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("WaterBefore"), "Valves.WaterBefore");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("WaterAfter"), "Valves.WaterAfter");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("LineNum"), "Valves.LineNum");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("SecondaryOutputNumber"), "Valves.SecondaryOutputNumber");
                        Assert.IsNotEmpty(valves.GetStringValueFromPath("FertProgram"), "Valves.FertProgram");
                    }
                }
            });
        }
    }
}
