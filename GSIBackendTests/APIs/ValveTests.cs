﻿using GSITestInfrastructure.GSIApis.Valve;
using GSITestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class ValveTests
    {
        [Test]
        [Category("Valve")]
        [Category("Settings Get")]
        public void SettingsGet()
        {
            var settingsGetApi = new SettingsGetApi();
            settingsGetApi.Run();

            var result = settingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(settingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsNotEmpty(result.GetStringValueFromPath("Name"), "Name");
            Assert.IsNotEmpty(result.GetStringValueFromPath("OutputNumber"), "OutputNumber");
            Assert.IsNotEmpty(result.GetStringValueFromPath("Status"), "Status");
            Assert.IsNotEmpty(result.GetStringValueFromPath("LowFlowDeviation"), "LowFlowDeviation");
            Assert.IsNotEmpty(result.GetStringValueFromPath("LowFlowDelay"), "LowFlowDelay");
            Assert.IsNotEmpty(result.GetStringValueFromPath("HighFlowDeviation"), "HighFlowDeviation");
            Assert.IsNotEmpty(result.GetStringValueFromPath("HighFlowDelay"), "HighFlowDelay");
            Assert.IsNotEmpty(result.GetStringValueFromPath("LineFillTime"), "LineFillTime");
            Assert.IsNotEmpty(result.GetStringValueFromPath("IrrigrationArea"), "IrrigrationArea");
            Assert.IsNotEmpty(result.GetStringValueFromPath("ValveColor"), "ValveColor");
            Assert.IsNotEmpty(result.GetStringValueFromPath("PrecipitationRate"), "PrecipitationRate");
            Assert.IsNotEmpty(result.GetStringValueFromPath("LastFlow"), "LastFlow");
            Assert.IsNotEmpty(result.GetStringValueFromPath("LastFlow_Date"), "LastFlow_Date");
            Assert.IsNotEmpty(result.GetStringValueFromPath("LastFlow_FlowTypeID"), "LastFlow_FlowTypeID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("TypeID"), "TypeID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("FertilizerConnected"), "FertilizerConnected");
            Assert.IsNotEmpty(result.GetStringValueFromPath("SetupNominalFlow"), "SetupNominalFlow");
            Assert.IsNotEmpty(result.GetStringValueFromPath("StopOnFertFailure"), "StopOnFertFailure");
            Assert.IsNotEmpty(result.GetStringValueFromPath("Kc"), "Kc");
            Assert.IsNotEmpty(result.GetStringValueFromPath("PlantTypeID"), "PlantTypeID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("SubCategoryID"), "SubCategoryID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("WaterFactor"), "WaterFactor");
            Assert.IsNotEmpty(result.GetStringValueFromPath("ValveID"), "ValveID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("ValveAlarm"), "ValveAlarm");
            Assert.IsNotEmpty(result.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("IgnoreWaterCounter"), "IgnoreWaterCounter");
            Assert.IsNotEmpty(result.GetStringValueFromPath("IgnoreMasterValve"), "IgnoreMasterValve");
            Assert.IsNotEmpty(result.GetStringValueFromPath("AuxiliaryOutput"), "AuxiliaryOutput");
            Assert.IsNotEmpty(result.GetStringValueFromPath("CropTypeID"), "CropTypeID");
        }
    }
}
