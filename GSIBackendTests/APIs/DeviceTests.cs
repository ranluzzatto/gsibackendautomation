﻿using GSITestInfrastructure.GSIApis.Device;
using GSITestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class DeviceTests
    {
        [Test]
        [Category("Device")]
        [Category("Change state")]
        public void ChangeState()
        {
            var changeStatePostApi = new ChangeStatePostApi(new Dictionary<string, string> { { "StateID", "1" } });

            changeStatePostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            changeStatePostApi.Run();

            Assert.IsTrue(changeStatePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(changeStatePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(changeStatePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Settings Get")]
        public void SettingsGet()
        {
            var settingsGetApi = new SettingsGetApi();
            settingsGetApi.Run();

            Assert.IsTrue(settingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = settingsGetApi.ResponseBody["Body"];
            var categorySetting = result["CategorySetting"];
            Assert.IsNotEmpty(categorySetting.GetStringValueFromPath("ContractorID"), "CategorySetting.ContractorID");
            Assert.IsNotEmpty(categorySetting.GetStringValueFromPath("DefinitionID"), "CategorySetting.DefinitionID");
            Assert.IsNotEmpty(categorySetting.GetStringValueFromPath("InspectorID"), "CategorySetting.InspectorID");
            Assert.IsNotEmpty(categorySetting.GetStringValueFromPath("RegionID"), "CategorySetting.RegionID");

            var types = result["Types"];
            foreach (var waterPulseType in types["WaterPulseTypes"])
            {
                Assert.IsNotEmpty(waterPulseType.GetStringValueFromPath("TypeID"), "WaterPulseTypes.TypeID");
                Assert.IsNotEmpty(waterPulseType.GetStringValueFromPath("Name"), "WaterPulseTypes.Name");
            }

            foreach (var fertPulseType in types["FertPulseTypes"])
            {
                Assert.IsNotEmpty(fertPulseType.GetStringValueFromPath("FertPulseType"), "FertPulseType.FertPulseType");
                Assert.IsNotEmpty(fertPulseType.GetStringValueFromPath("Quant"), "FertPulseType.Quant");
            }

            foreach (var landSizeType in types["LandSizeTypes"])
            {
                Assert.IsNotEmpty(landSizeType.GetStringValueFromPath("TypeID"), "LandSizeTypes.TypeID");
                Assert.IsNotEmpty(landSizeType.GetStringValueFromPath("Name"), "LandSizeTypes.Name");
            }

            foreach (var inputType in types["InputTypes"])
            {
                Assert.IsNotEmpty(inputType.GetStringValueFromPath("TypeID"), "InputTypes.TypeID");
                Assert.IsNotEmpty(inputType.GetStringValueFromPath("Name"), "InputTypes.Name");
            }

            foreach (var timesZone in types["TimesZone"])
            {
                Assert.IsNotEmpty(timesZone.GetStringValueFromPath("ZoneID"), "TimesZone.ZoneID");
                Assert.IsNotEmpty(timesZone.GetStringValueFromPath("DisplayName"), "TimesZone.DisplayName");
                Assert.IsNotEmpty(timesZone.GetStringValueFromPath("GMTOffset"), "TimesZone.GMTOffset");
                Assert.IsNotEmpty(timesZone.GetStringValueFromPath("ActualOffset"), "TimesZone.ActualOffset");
                Assert.IsNotEmpty(timesZone.GetStringValueFromPath("ManualOffset"), "TimesZone.ManualOffset");
                Assert.IsNotEmpty(timesZone.GetStringValueFromPath("DaylightName"), "TimesZone.DaylightName");
            }

            foreach (var connectionInterval in types["ConnectionIntervals"])
            {
                Assert.IsNotEmpty(connectionInterval.GetStringValueFromPath("TotalSeconds"), "ConnectionIntervals.TotalSeconds");
                Assert.IsNotEmpty(connectionInterval.GetStringValueFromPath("Factor"), "ConnectionIntervals.Factor");
                Assert.IsNotEmpty(connectionInterval.GetStringValueFromPath("Interval"), "ConnectionIntervals.Interval");
            }

            foreach (var batteryType in types["BatteryTypes"])
            {
                Assert.IsNotEmpty(batteryType.GetStringValueFromPath("ID"), "BatteryTypes.ID");
                Assert.IsNotEmpty(batteryType.GetStringValueFromPath("Name"), "BatteryTypes.Name");
            }

            var deviceInfo = result["DeviceInfo"];
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SN"), "DeviceInfo.SN");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ControlUnitID"), "DeviceInfo.ControlUnitID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitName"), "DeviceInfo.UnitName");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelID"), "DeviceInfo.ModelID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelView"), "DeviceInfo.ModelView");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelVersion"), "DeviceInfo.ModelVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("WaterFactor"), "DeviceInfo.WaterFactor");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Signal"), "DeviceInfo.Signal");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ValveNum"), "DeviceInfo.ValveNum");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CommTypeID"), "DeviceInfo.CommTypeID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Status"), "DeviceInfo.Status");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CurrentConfigID"), "DeviceInfo.CurrentConfigID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CommUnit_AppVersion"), "DeviceInfo.CommUnit_AppVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("BootLoaderVersion"), "DeviceInfo.BootLoaderVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LandTypeID"), "DeviceInfo.LandTypeID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonBudget"), "DeviceInfo.SeasonBudget");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsGSIAg"), "DeviceInfo.IsGSIAg");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Description"), "DeviceInfo.Description");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Address"), "DeviceInfo.Address");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Phone"), "DeviceInfo.Phone");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonEndDate"), "DeviceInfo.SeasonEndDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonStartDate"), "DeviceInfo.SeasonStartDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonEndDateTicks"), "DeviceInfo.SeasonEndDateTicks");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonStartDateTicks"), "DeviceInfo.SeasonStartDateTicks");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ConnectionStatus"), "DeviceInfo.ConnectionStatus");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ControllerState"), "DeviceInfo.ControllerState");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("InputsState"), "DeviceInfo.InputsState");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("OutputState"), "DeviceInfo.OutputState");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ShortedValve"), "DeviceInfo.ShortedValve");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsSync"), "DeviceInfo.IsSync");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DeviceAlarm"), "DeviceInfo.DeviceAlarm");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("RainChance"), "DeviceInfo.RainChance");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Map_Latitude"), "DeviceInfo.Map_Latitude");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Map_Longitude"), "DeviceInfo.Map_Longitude");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ETmanual"), "DeviceInfo.ETmanual");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ET0"), "DeviceInfo.ET0");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("FlagET"), "DeviceInfo.FlagET");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsPRO"), "DeviceInfo.IsPRO");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LineNum"), "DeviceInfo.LineNum");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog1"), "DeviceInfo.DigitalAnalog1");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog2"), "DeviceInfo.DigitalAnalog2");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog3"), "DeviceInfo.DigitalAnalog3");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("TimeZoneID"), "DeviceInfo.TimeZoneID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsMetric"), "DeviceInfo.IsMetric");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LandType"), "DeviceInfo.LandType");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("NewHardware"), "DeviceInfo.NewHardware");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitPulseView"), "DeviceInfo.UnitPulseView");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CorrectVersion"), "DeviceInfo.CorrectVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LocalProgramWaterUnit"), "DeviceInfo.LocalProgramWaterUnit");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("WaterFactorUnit"), "DeviceInfo.WaterFactorUnit");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("PaymentAlert"), "DeviceInfo.PaymentAlert");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitPaymentExpirationDate"), "DeviceInfo.UnitPaymentExpirationDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitLastPaymentDate"), "DeviceInfo.UnitLastPaymentDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitPaymentStatus"), "DeviceInfo.UnitPaymentStatus");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnpaidUnit"), "DeviceInfo.UnpaidUnit");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IrrimaxApiKey"), "DeviceInfo.IrrimaxApiKey");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("NewVer"), "DeviceInfo.NewVer");
            foreach(var listOfVersions in deviceInfo["ListOfVersions"])
            {
                Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("VersionType"), "DeviceInfo.ListOfVersions.VersionType");
                Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("Version"), "DeviceInfo.ListOfVersions.Version");
                Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("TypeOfController"), "DeviceInfo.ListOfVersions.NewVer");
            }

            var generalSetting = result["GeneralSetting"];
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("Status"), "GeneralSetting.Status");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("UseMaster"), "GeneralSetting.UseMaster");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("TimeZoneID"), "GeneralSetting.TimeZoneID");
            
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DSundayState"), "GeneralSetting.ValidDays.DSundayState");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DMondayState"), "GeneralSetting.ValidDays.DMondayState");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DTuesdayState"), "GeneralSetting.ValidDays.DTuesdayState");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DWednesdayState"), "GeneralSetting.ValidDays.DWednesdayState");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DThursdayState"), "GeneralSetting.ValidDays.DThursdayState");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DFridayState"), "GeneralSetting.ValidDays.DFridayState");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ValidDays.DSaturdayState"), "GeneralSetting.ValidDays.DSaturdayState");

            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("PauseInputConditionNumber"), "GeneralSetting.PauseInputConditionNumber");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ExistingBackupBattery"), "GeneralSetting.ExistingBackupBattery");
            Assert.IsNotEmpty(generalSetting.GetStringValueFromPath("ProgramHFilter"), "GeneralSetting.ProgramHFilter");

            var irrgationSetting = result["IrrgationSetting"];
            foreach (var restrictedDates in irrgationSetting["RestrictedDates"])
            {
                Assert.IsNotEmpty(restrictedDates.GetStringValueFromPath(""), "IrrgationSetting");
            }

            Assert.IsNotEmpty(irrgationSetting.GetStringValueFromPath("IsLocalSequenceActive"), "IrrgationSetting.IsLocalSequenceActive");
            Assert.IsNotEmpty(irrgationSetting.GetStringValueFromPath("NotProgramsAsQueue"), "IrrgationSetting.NotProgramsAsQueue");

            var waterMeter = result["WaterMeter"];
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseSize"), "WaterMeter.PulseSize");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("MeterType"), "WaterMeter.MeterType");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("IsEnabled"), "WaterMeter.IsEnabled");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseTypeID"), "WaterMeter.PulseTypeID");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("FlowTypeID"), "WaterMeter.FlowTypeID");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("NoWaterPulseDelay"), "WaterMeter.NoWaterPulseDelay");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LeakageLimit"), "WaterMeter.LeakageLimit");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("InputNumber"), "WaterMeter.InputNumber");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterType"), "WaterMeter.WaterMeterType");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseViewTypeID"), "WaterMeter.PulseViewTypeID");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterNumber"), "WaterMeter.WaterMeterNumber");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LineNum"), "WaterMeter.LineNum");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterID"), "WaterMeter.WaterMeterID");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("EnableAutoCancleAlarm"), "WaterMeter.EnableAutoCancleAlarm");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("AlertsAutoResetTime"), "WaterMeter.AlertsAutoResetTime");
            Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("ReleaseWaterLeakageFault"), "WaterMeter.ReleaseWaterLeakageFault");

            var rainSensor = result["RainSensor"];
            Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("IsEnabled"), "RainSensor.IsEnabled");
            Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("DaysStopper"), "RainSensor.DaysStopper");
            Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("NC"), "RainSensor.NC");
            Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("RainInputConditionNumber"), "RainSensor.RainInputConditionNumber");

            var fertilizer = result["Fertilizer"];
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseCustomFertilizerPulse"), "Fertilizer.UseCustomFertilizerPulse");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumber"), "Fertilizer.OutputNumber");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("ContinuousFert"), "Fertilizer.ContinuousFert");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseSize"), "Fertilizer.PulseSize");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseViewTypeID"), "Fertilizer.PulseViewTypeID");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertPulseDuration"), "Fertilizer.FertPulseDuration");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FerlizerFaillureTime"), "Fertilizer.FerlizerFaillureTime");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("Leakage"), "Fertilizer.Leakage");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerType"), "Fertilizer.FertilizerType");

            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.FertPulseType"), "Fertilizer.PulseTypeID.FertPulseType");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.Quant"), "Fertilizer.PulseTypeID.Quant");
            
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FlowTypeID"), "Fertilizer.FlowTypeID");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("IsEnabled"), "Fertilizer.IsEnabled");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("NominalFlow"), "Fertilizer.NominalFlow");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerID"), "Fertilizer.FertilizerID");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertCounterInputNum"), "Fertilizer.FertCounterInputNum");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseFertMeter4Error"), "Fertilizer.UseFertMeter4Error");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertLeakDetectActive"), "Fertilizer.FertLeakDetectActive");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseRelay"), "Fertilizer.UseRelay");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseMaster"), "Fertilizer.UseMaster");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumberMaster"), "Fertilizer.OutputNumberMaster");
            Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertNumber"), "Fertilizer.FertNumber");

            var advancedSettings = result["AdvancedSettings"];
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("ValveOpenningPattern"), "advancedSettings.ValveOpenningPattern");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("ValveClosingPattern"), "advancedSettings.ValveClosingPattern");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("IsMetric"), "advancedSettings.IsMetric");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("LandType"), "advancedSettings.LandType");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("ValveOverlapping"), "advancedSettings.ValveOverlapping");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("OverlapTime"), "advancedSettings.OverlapTime");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("ValveCloseDelay"), "advancedSettings.ValveCloseDelay");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("ValveOpenDelay"), "advancedSettings.ValveOpenDelay");
            Assert.IsNotEmpty(advancedSettings.GetStringValueFromPath("LineNum"), "advancedSettings.LineNum");

            foreach (var inputSetting in result["InputSetting"])
            {
                Assert.IsNotEmpty(inputSetting.GetStringValueFromPath("ConditionInputNumber"), "InputSetting.ConditionInputNumber");
                Assert.IsNotEmpty(inputSetting.GetStringValueFromPath("OnDelay"), "InputSetting.OnDelay");
                Assert.IsNotEmpty(inputSetting.GetStringValueFromPath("OffDelay"), "InputSetting.OffDelay");
                Assert.IsNotEmpty(inputSetting.GetStringValueFromPath("Name"), "InputSetting.Name");
                Assert.IsNotEmpty(inputSetting.GetStringValueFromPath("TypeID"), "InputSetting.TypeID");
            }

            var communicationTimes = result["CommunicationTimes"];
            if(communicationTimes.Type != JTokenType.Null)
            {
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ConnectionInterval.TotalSeconds"), "communicationTimes.ConnectionInterval.TotalSeconds");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ConnectionInterval.Factor"), "communicationTimes.ConnectionInterval.Factor");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ConnectionInterval.Interval"), "communicationTimes.ConnectionInterval.Interval");
                foreach (var timeInDay in communicationTimes["TimeInDay"])
                {
                    Assert.IsNotEmpty(timeInDay.GetStringValueFromPath(""), "TimeInDay");
                }
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemStartTime"), "communicationTimes.ModemStartTime");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemEndTime"), "communicationTimes.ModemEndTime");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemRetryNum"), "communicationTimes.ModemRetryNum");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemRetryDelay"), "communicationTimes.ModemRetryDelay");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ActiveDaliyCommunication"), "communicationTimes.ActiveDaliyCommunication");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("CycleForSensorLog"), "communicationTimes.CycleForSensorLog");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("CycleForSensorEvents"), "communicationTimes.CycleForSensorEvents");
            }

            var batterySettings = result["BatterySettings"];
            Assert.IsNotEmpty(batterySettings.GetStringValueFromPath("BattaryAlarmLevel"), "BatterySettings.BattaryAlarmLevel");
            Assert.IsNotEmpty(batterySettings.GetStringValueFromPath("BattaryPauseLevel"), "BatterySettings.BattaryPauseLevel");
            Assert.IsNotEmpty(batterySettings.GetStringValueFromPath("BattaryDataCycle"), "BatterySettings.BattaryDataCycle");
            Assert.IsNotEmpty(batterySettings.GetStringValueFromPath("BattaryGoodLevel"), "BatterySettings.BattaryGoodLevel");
            Assert.IsNotEmpty(batterySettings.GetStringValueFromPath("BatteryType"), "BatterySettings.BatteryType");

            Assert.IsNotEmpty(result.GetStringValueFromPath("ETtype"), "ETtype");
            Assert.IsNotEmpty(result.GetStringValueFromPath("ET0"), "ET0");
            Assert.IsNotEmpty(result.GetStringValueFromPath("FactorRain"), "FactorRain");
            Assert.IsNotEmpty(result.GetStringValueFromPath("EnableFactorRain"), "EnableFactorRain");
            Assert.IsNotEmpty(result.GetStringValueFromPath("ActualRainMM"), "ActualRainMM");
            foreach (var eTmanualSettigs in result["ETmanualSettigs"])
            {
                Assert.IsNotEmpty(eTmanualSettigs.GetStringValueFromPath("ET0"), "ETmanualSettigs.ET0");
                Assert.IsNotEmpty(eTmanualSettigs.GetStringValueFromPath("Rain"), "ETmanualSettigs.Rain");
                Assert.IsNotEmpty(eTmanualSettigs.GetStringValueFromPath("Month"), "ETmanualSettigs.Month");
                Assert.IsNotEmpty(eTmanualSettigs.GetStringValueFromPath("Week"), "ETmanualSettigs.Week");
            }
            Assert.IsNotEmpty(result.GetStringValueFromPath("InputFlash"), "InputFlash");
        }

        [TestCase(50)]
        [TestCase(100)]
        [Test]
        [Category("Device")]
        [Category("Settings Post")]
        public void SettingsPost(int factorRainData)
        {
            var settingsPostApi = new SettingsPostApi();
            var changedPayload = settingsPostApi.payload.ToJObject();
            changedPayload["FactorRain"] = factorRainData;
            settingsPostApi.RequestPayload = changedPayload.ToString();

            settingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            settingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 7,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 7,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            settingsPostApi.Run();

            Assert.IsTrue(settingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(settingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(settingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var SettingsGetApi = new SettingsGetApi();
            SettingsGetApi.Run();

            Assert.IsTrue(SettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(SettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(factorRainData, SettingsGetApi.ResponseBody.GetIntValueFromPath("Body.FactorRain"));
        }

        [Test]
        [Category("Device")]
        [Category("Unit")]
        public void Unit()
        {
            var unitGetApi = new UnitGetApi();
            
            unitGetApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 3,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 3,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            unitGetApi.Run();

            var result = unitGetApi.ResponseBody["Body"];
            Assert.IsTrue(unitGetApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(unitGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(unitGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var config = result["Config"];
                Assert.IsNotEmpty(config.GetStringValueFromPath("SN"), "Config.SN");
                Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("SerialNumber"), config.GetStringValueFromPath("SN"), "Config.SN - compare");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ControlUnitID"), "Config.ControlUnitID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitName"), "Config.UnitName");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelID"), "Config.ModelID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelView"), "Config.ModelView");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelVersion"), "Config.ModelVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("WaterFactor"), "Config.WaterFactor");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Signal"), "Config.Signal");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ValveNum"), "Config.ValveNum");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CommTypeID"), "Config.CommTypeID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Status"), "Config.Status");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CurrentConfigID"), "Config.CurrentConfigID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CommUnit_AppVersion"), "Config.CommUnit_AppVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("BootLoaderVersion"), "Config.BootLoaderVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LandTypeID"), "Config.LandTypeID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonBudget"), "Config.SeasonBudget");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsGSIAg"), "Config.IsGSIAg");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Description"), "Config.Description");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Address"), "Config.Address");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Phone"), "Config.Phone");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonEndDate"), "Config.SeasonEndDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonStartDate"), "Config.SeasonStartDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonEndDateTicks"), "Config.SeasonEndDateTicks");
                Assert.IsNotEmpty(config.GetStringValueFromPath("SeasonStartDateTicks"), "Config.SeasonStartDateTicks");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ConnectionStatus"), "Config.ConnectionStatus");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ControllerState"), "Config.ControllerState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("InputsState"), "Config.InputsState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("OutputState"), "Config.OutputState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ShortedValve"), "Config.ShortedValve");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsSync"), "Config.IsSync");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DeviceAlarm"), "Config.DeviceAlarm");
                Assert.IsNotEmpty(config.GetStringValueFromPath("RainChance"), "Config.RainChance");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Map_Latitude"), "Config.Map_Latitude");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Map_Longitude"), "Config.Map_Longitude");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ETmanual"), "Config.ETmanual");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ET0"), "Config.ET0");
                Assert.IsNotEmpty(config.GetStringValueFromPath("FlagET"), "Config.FlagET");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsPRO"), "Config.IsPRO");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LineNum"), "Config.LineNum");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DigitalAnalog1"), "Config.DigitalAnalog1");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DigitalAnalog2"), "Config.DigitalAnalog2");
                Assert.IsNotEmpty(config.GetStringValueFromPath("DigitalAnalog3"), "Config.DigitalAnalog3");
                Assert.IsNotEmpty(config.GetStringValueFromPath("TimeZoneID"), "Config.TimeZoneID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsMetric"), "Config.IsMetric");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LandType"), "Config.LandType");
                Assert.IsNotEmpty(config.GetStringValueFromPath("NewHardware"), "Config.NewHardware");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitPulseView"), "Config.UnitPulseView");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CorrectVersion"), "Config.CorrectVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LocalProgramWaterUnit"), "Config.LocalProgramWaterUnit");
                Assert.IsNotEmpty(config.GetStringValueFromPath("WaterFactorUnit"), "Config.WaterFactorUnit");
                Assert.IsNotEmpty(config.GetStringValueFromPath("PaymentAlert"), "Config.PaymentAlert");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitPaymentExpirationDate"), "Config.UnitPaymentExpirationDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitLastPaymentDate"), "Config.UnitLastPaymentDate");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitPaymentStatus"), "Config.UnitPaymentStatus");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnpaidUnit"), "Config.UnpaidUnit");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IrrimaxApiKey"), "Config.IrrimaxApiKey");
                Assert.IsNotEmpty(config.GetStringValueFromPath("NewVer"), "Config.NewVer");
                foreach (var listOfVersions in config["ListOfVersions"])
                {
                    Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("VersionType"), "Config.ListOfVersions.VersionType");
                    Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("Version"), "Config.ListOfVersions.Version");
                    Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("TypeOfController"), "Config.ListOfVersions.NewVer");
                }

                foreach(var valve in result["Valves"])
                {
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveName"), "Valves.ValveName");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveColor"), "Valves.ValveColor");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Precipitation_AllowDuration"), "Valves.Precipitation_AllowDuration");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Precipitation_AllowQuantity"), "Valves.Precipitation_AllowQuantity");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveID"), "Valves.ValveID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LandTypeID"), "Valves.LandTypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("TypeID"), "Valves.TypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Status"), "Valves.Status");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("OutputNumber"), "Valves.OutputNumber");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("PrecipitationRate"), "Valves.PrecipitationRate");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IrrigrationArea"), "Valves.IrrigrationArea");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveAlarm"), "Valves.ValveAlarm");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveSetup"), "Valves.ValveSetup");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LineNum"), "Valves.LineNum");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterMeter"), "Valves.WaterMeter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreWaterCounter"), "Valves.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreMasterValve"), "Valves.IgnoreMasterValve");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("AuxiliaryOutput"), "Valves.AuxiliaryOutput");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterMeterType"), "Valves.WaterMeterType");
                }

                foreach (var program in result["Programs"])
                {
                    Assert.IsNotEmpty(program.GetStringValueFromPath("Name"), "Programs.Name");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("ID"), "Programs.ID");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("OrderNum"), "Programs.OrderNum");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("Status"), "Programs.Status");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramName"), "Programs.ProgramName");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramID"), "Programs.ProgramID");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("Symbol"), "Programs.Symbol");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("FertMethodID"), "Programs.FertMethodID");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("WaterFactor"), "Programs.WaterFactor");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramNumber"), "Programs.ProgramNumber");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("Type"), "Programs.Type");
                    Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramSetup"), "Programs.ProgramSetup");
                }

                var validDays = result["ValidDays"];
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSundayState"), "validDays.DSundayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DMondayState"), "validDays.DMondayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DTuesdayState"), "validDays.DTuesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DWednesdayState"), "validDays.DWednesdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DThursdayState"), "validDays.DThursdayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DFridayState"), "validDays.DFridayState");
                Assert.IsNotEmpty(validDays.GetStringValueFromPath("DSaturdayState"), "validDays.DSaturdayState");

                var waterMeter = result["WaterMeter"];
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseSize"), "WaterMeter.PulseSize");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("MeterType"), "WaterMeter.MeterType");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("IsEnabled"), "WaterMeter.IsEnabled");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseTypeID"), "WaterMeter.PulseTypeID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("FlowTypeID"), "WaterMeter.FlowTypeID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("NoWaterPulseDelay"), "WaterMeter.NoWaterPulseDelay");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LeakageLimit"), "WaterMeter.LeakageLimit");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("InputNumber"), "WaterMeter.InputNumber");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterType"), "WaterMeter.WaterMeterType");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("PulseViewTypeID"), "WaterMeter.PulseViewTypeID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterNumber"), "WaterMeter.WaterMeterNumber");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("LineNum"), "WaterMeter.LineNum");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("WaterMeterID"), "WaterMeter.WaterMeterID");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("EnableAutoCancleAlarm"), "WaterMeter.EnableAutoCancleAlarm");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("AlertsAutoResetTime"), "WaterMeter.AlertsAutoResetTime");
                Assert.IsNotEmpty(waterMeter.GetStringValueFromPath("ReleaseWaterLeakageFault"), "WaterMeter.ReleaseWaterLeakageFault");

                var fertilizer = result["Fertilizer"];
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseCustomFertilizerPulse"), "Fertilizer.UseCustomFertilizerPulse");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumber"), "Fertilizer.OutputNumber");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("ContinuousFert"), "Fertilizer.ContinuousFert");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseSize"), "Fertilizer.PulseSize");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseViewTypeID"), "Fertilizer.PulseViewTypeID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertPulseDuration"), "Fertilizer.FertPulseDuration");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FerlizerFaillureTime"), "Fertilizer.FerlizerFaillureTime");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("Leakage"), "Fertilizer.Leakage");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerType"), "Fertilizer.FertilizerType");
                
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.FertPulseType"), "Fertilizer.PulseTypeID.FertPulseType");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.Quant"), "Fertilizer.PulseTypeID.Quant");

                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FlowTypeID"), "Fertilizer.FlowTypeID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("IsEnabled"), "Fertilizer.IsEnabled");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("NominalFlow"), "Fertilizer.NominalFlow");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerID"), "Fertilizer.FertilizerID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertCounterInputNum"), "Fertilizer.FertCounterInputNum");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseFertMeter4Error"), "Fertilizer.UseFertMeter4Error");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertLeakDetectActive"), "Fertilizer.FertLeakDetectActive");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseRelay"), "Fertilizer.UseRelay");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseMaster"), "Fertilizer.UseMaster");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumberMaster"), "Fertilizer.OutputNumberMaster");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertNumber"), "Fertilizer.FertNumber");

                foreach(var accumulators in result["Accumulators"])
                {
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("TotalSeason"), "Accumulators.TotalSeason");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("TotalMonth"), "Accumulators.TotalMonth");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("TotalUnexpectedFlowSeason"), "Accumulators.TotalUnexpectedFlowSeason");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("TotalUnexpectedFlowMonth"), "Accumulators.TotalUnexpectedFlowMonth");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("MonthDate"), "Accumulators.MonthDate");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("ActiveAlerts"), "Accumulators.ActiveAlerts");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("PulseViewTypeID"), "Accumulators.PulseViewTypeID");
                    Assert.IsNotEmpty(accumulators.GetStringValueFromPath("percentage"), "Accumulators.percentage");
                }

                var rainSensor = result["RainSensor"];
                Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("IsEnabled"), "RainSensor.IsEnabled");
                Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("DaysStopper"), "RainSensor.DaysStopper");
                Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("RainDetectorNC"), "RainSensor.RainDetectorNC");
                Assert.IsNotEmpty(rainSensor.GetStringValueFromPath("ConditionNumber"), "RainSensor.ConditionNumber");

                var onlinePro = result["OnlinePro"];
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("Line1State"), "OnlinePro.Line1State");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("Line2State"), "OnlinePro.Line2State");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("FertStatusCodeLine1"), "OnlinePro.FertStatusCodeLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("FertStatusCodeLine1"), "OnlinePro.FertStatusCodeLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainInputNumberLine1"), "OnlinePro.RainInputNumberLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainInputNumberLine2"), "OnlinePro.RainInputNumberLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainStateCodeLine1"), "OnlinePro.RainStateCodeLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainStateCodeLine2"), "OnlinePro.RainStateCodeLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainStatusCodeLine1"), "OnlinePro.RainStatusCodeLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainStatusCodeLine2"), "OnlinePro.RainStatusCodeLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainOffDelayLine1"), "OnlinePro.RainOffDelayLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("RainOffDelayLine2"), "OnlinePro.RainOffDelayLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("FilterFlushCodeLineA"), "OnlinePro.FilterFlushCodeLineA");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("FilterFlushCodeLineB"), "OnlinePro.FilterFlushCodeLineB");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("TempPauseLine1"), "OnlinePro.TempPauseLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("TempPauseLine2"), "OnlinePro.TempPauseLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("DailyCycleNumLineA"), "OnlinePro.DailyCycleNumLineA");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("DailyCycleNumLineB"), "OnlinePro.DailyCycleNumLineB");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("BatteryStatus"), "OnlinePro.BatteryStatus");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("BatteryValue"), "OnlinePro.BatteryValue");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("DailyTimeLine1"), "OnlinePro.DailyTimeLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("DailyTimeLine2"), "OnlinePro.DailyTimeLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("DailyQuantityLine1"), "OnlinePro.DailyQuantityLine1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("DailyQuantityLine2"), "OnlinePro.DailyQuantityLine2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("TestOutputSucsses"), "OnlinePro.TestOutputSucsses");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("TestOutputNotSucsses"), "OnlinePro.TestOutputNotSucsses");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("LastQuantityFilter1"), "OnlinePro.LastQuantityFilter1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("LastQuantityFilter2"), "OnlinePro.LastQuantityFilter2");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("LastTimeFilter1"), "OnlinePro.LastTimeFilter1");
                Assert.IsNotEmpty(onlinePro.GetStringValueFromPath("LastTimeFilter2"), "OnlinePro.LastTimeFilter2");

                foreach (var nextIrrigationOnline in result["NextIrrigationOnline"])
                {
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("ControlUnitID"), "NextIrrigationOnline.ControlUnitID");
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("PipeNumber"), "NextIrrigationOnline.PipeNumber");
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("NextStartTime"), "NextIrrigationOnline.NextStartTime");
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("DayToNextStart"), "NextIrrigationOnline.DayToNextStart");
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("ProgramNumber"), "NextIrrigationOnline.ProgramNumber");
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("ValveNumber"), "NextIrrigationOnline.ValveNumber");
                    Assert.IsNotEmpty(nextIrrigationOnline.GetStringValueFromPath("CyclesLeft"), "NextIrrigationOnline.CyclesLeft");
                }

                foreach (var fertOnStatus in result["FertOnStatus"])
                {
                    Assert.IsNotEmpty(fertOnStatus.GetStringValueFromPath("FertOutputNum"), "FertOnStatus.FertOutputNum");
                    Assert.IsNotEmpty(fertOnStatus.GetStringValueFromPath("FertAlarm"), "FertOnStatus.FertAlarm");
                }

                foreach (var fertCenterOnStatus in result["FertCenterOnStatus"])
                {
                    Assert.IsNotEmpty(fertCenterOnStatus.GetStringValueFromPath("FertCenterNum"), "FertCenterOnStatus.FertCenterNum");
                    Assert.IsNotEmpty(fertCenterOnStatus.GetStringValueFromPath("FertCenterAlarm"), "FertCenterOnStatus.FertCenterAlarm");
                }

                foreach (var fertACC in result["FertACC"])
                {
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("ID"), "FertACC.ID");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("ControlUnitID"), "FertACC.ControlUnitID");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert1Quantity"), "FertACC.DailyFert1Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert2Quantity"), "FertACC.DailyFert2Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert3Quantity"), "FertACC.DailyFert3Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert4Quantity"), "FertACC.DailyFert4Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert5Quantity"), "FertACC.DailyFert5Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert6Quantity"), "FertACC.DailyFert6Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert7Quantity"), "FertACC.DailyFert7Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("DailyFert8Quantity"), "FertACC.DailyFert8Quantity");
                    Assert.IsNotEmpty(fertACC.GetStringValueFromPath("FertUnit"), "FertACC.FertUnit");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Filter program Get")]
        public void FilterProgramGet()
        {
            var filterProgramGetApi = new FilterProgramGetApi();
            filterProgramGetApi.Run();

            Assert.IsTrue(filterProgramGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(filterProgramGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = filterProgramGetApi.ResponseBody["Body"];
            var program = result["Program"];
            Assert.IsNotEmpty(program.GetStringValueFromPath("Name"), "Program.Name");
            Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramID"), "Program.ProgramID");
            Assert.IsNotEmpty(program.GetStringValueFromPath("ProgramNumber"), "Program.ProgramNumber");
            Assert.IsNotEmpty(program.GetStringValueFromPath("Priority"), "Program.Priority");
            Assert.IsNotEmpty(program.GetStringValueFromPath("ConfigID"), "Program.ConfigID");
            Assert.IsNotEmpty(program.GetStringValueFromPath("Status"), "Program.Status");

            var filterSetting = program["FilterSetting"];
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("FilterFlashID"), "FilterSetting.FilterFlashID");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("WaterForFlash"), "FilterSetting.WaterForFlash");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("TimeForFlash"), "FilterSetting.TimeForFlash");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("PipeFillDelay"), "FilterSetting.PipeFillDelay");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("Continuous"), "FilterSetting.Continuous");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("UseInputCondition"), "FilterSetting.UseInputCondition");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("UnitOperTime"), "FilterSetting.UnitOperTime");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("UnitWaitTime"), "FilterSetting.UnitWaitTime");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("OpenMaster"), "FilterSetting.OpenMaster");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("PauseWhenFault"), "FilterSetting.PauseWhenFault");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("InputFlash"), "FilterSetting.InputFlash");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("MaxParallelRequests"), "FilterSetting.MaxParallelRequests");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("StopIrrigation"), "FilterSetting.StopIrrigation");
            foreach (var condition in filterSetting["Conditions"])
            {
                Assert.IsNotEmpty(condition.GetStringValueFromPath(""), "Conditions");
            }
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("SustainOutput"), "FilterSetting.SustainOutput");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("SustainTime"), "FilterSetting.SustainTime");
            Assert.IsNotEmpty(filterSetting.GetStringValueFromPath("FlushStandAlone"), "FilterSetting.FlushStandAlone");

            foreach (var valveInProgram in program["ValveInProgram"])
            {
                Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ProgramID"), "valveInProgram.ProgramID");
                Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveID"), "valveInProgram.ValveID");
                Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OutputNumber"), "valveInProgram.OutputNumber");
                Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("Name"), "valveInProgram.Name");
                Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OrderNumber"), "valveInProgram.OrderNumber");
                Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ID"), "valveInProgram.ID");
            }
        }

        [Test]
        [Category("Device")]
        [Category("Filter program Post")]
        public void FilterProgramPost()
        {
            var filterProgramPostApi = new FilterProgramPostApi();
            var changedPayload = filterProgramPostApi.payload.ToJObject();
            var timeForFlash = 30;
            var generalData = 5;
            changedPayload["Status"] = "NotActive";
            changedPayload["FilterSetting"]["TimeForFlash"] = timeForFlash;
            changedPayload["FilterSetting"]["TimeForFlash"] = timeForFlash;
            changedPayload["FilterSetting"]["PipeFillDelay"] = generalData;
            changedPayload["FilterSetting"]["PauseWhenFault"] = true;
            filterProgramPostApi.RequestPayload = changedPayload.ToString();

            filterProgramPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            filterProgramPostApi.Run();
            
            Assert.IsTrue(filterProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(filterProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(filterProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check of post method
            var filterProgramGetApi = new FilterProgramGetApi();
            filterProgramGetApi.Run();

            Assert.IsTrue(filterProgramGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(filterProgramGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = filterProgramGetApi.ResponseBody["Body"]["Program"]["FilterSetting"];
            Assert.AreEqual(timeForFlash, result.GetIntValueFromPath("TimeForFlash"), "FilterSetting.TimeForFlash");
            Assert.AreEqual(generalData, result.GetIntValueFromPath("PipeFillDelay"), "FilterSetting.PipeFillDelay");
            Assert.IsTrue(result.GetBoolValueFromPath("PauseWhenFault"), "FilterSetting.PauseWhenFault");
        }

        [Test]
        [Category("Device")]
        [Category("Reset unit totals")]
        public void ResetUnitTotals()
        {
            var resetUnitTotalsPostApi = new ResetUnitTotalsPostApi(new Dictionary<string, string> { { "OptionsBits", "63" } });
            resetUnitTotalsPostApi.Run();

            Assert.IsTrue(resetUnitTotalsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(resetUnitTotalsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Output settings Get")]
        public void OutputSettingsGet()
        {
            var outputSettingsGetApi = new OutputSettingsGetApi();
            outputSettingsGetApi.Run();

            Assert.IsTrue(outputSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(outputSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = outputSettingsGetApi.ResponseBody["Body"];

            Assert.Multiple(() =>
            {
                foreach (var output in result["OutputSetting"])
                {
                    Assert.IsNotEmpty(output.GetStringValueFromPath("Name"));
                    Assert.IsNotEmpty(output.GetStringValueFromPath("TypeID"));
                    Assert.IsNotEmpty(output.GetStringValueFromPath("UseRelay"));
                    Assert.IsNotEmpty(output.GetStringValueFromPath("OutputComandDuration"));
                }

                foreach (var input in result["InputSetting"])
                {
                    Assert.IsNotEmpty(input.GetStringValueFromPath("ConditionInputNumber"));
                    Assert.IsNotEmpty(input.GetStringValueFromPath("OnDelay"));
                    Assert.IsNotEmpty(input.GetStringValueFromPath("OffDelay"));
                    Assert.IsNotEmpty(input.GetStringValueFromPath("Name"));
                    Assert.IsNotEmpty(input.GetStringValueFromPath("TypeID"));
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Output settings Post")]
        public void OutputSettingsPost()
        {
            var outputSettingsPostApi = new OutputSettingsPostApi();
            var changedPayload = outputSettingsPostApi.payload.ToJObject();
            changedPayload["OutputSetting"][0]["Name"] = "O0";

            outputSettingsPostApi.RequestPayload = changedPayload.ToString();
            outputSettingsPostApi.Run();

            Assert.IsTrue(outputSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(outputSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check of the post method
            var outputSettingsGetApi = new OutputSettingsGetApi();
            outputSettingsGetApi.Run();

            Assert.IsTrue(outputSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(outputSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = outputSettingsGetApi.ResponseBody["Body"]["OutputSetting"][0];
            Assert.AreEqual(result.GetStringValueFromPath("Name"), "O0");
        }

        [Test]
        [Category("Device")]
        [Category("Chance Of Rain")]
        public void ChanceOfRain()
        {
            var chanceOfRainPostApi = new ChanceOfRainPostApi(new Dictionary<string, string> { { "Status", "false" } });

            chanceOfRainPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            chanceOfRainPostApi.Run();

            Assert.IsTrue(chanceOfRainPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(chanceOfRainPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(chanceOfRainPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Valves settings Get")]
        public void ValvesSettingsGet()
        {
            var valvesSettingsGetApi = new ValvesSettingsGetApi();
            valvesSettingsGetApi.Run();

            var response = valvesSettingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(valvesSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valvesSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {

                var typeIdNum = 0;
                foreach (var cropTypes in response["Types"]["LineTypeNum"])
                {
                    Assert.AreEqual(cropTypes.GetIntValueFromPath("TypeID"), typeIdNum++, "Types.LinetypeNum.TypeID");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Name"), "Types.LinetypeNum.Name");
                }

                foreach (var cropTypes in response["Types"]["Condition"])
                {
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("TypeID"), "Types.Condition.TypeID");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Name"), "Types.Condition.Name");
                }

                foreach (var cropTypes in response["Types"]["CropType"])
                {
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("TypeID"), "Types.CropType.TypeID");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Name"), "Types.CondiCropTypetion.Name");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Permanent"), "Types.CondiCropTypetion.Permanent");
                }

                foreach (var item in response["ValveSetting"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ValveSetting.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OutputNumber"), "ValveSetting.OutputNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "ValveSetting.Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LowFlowDeviation"), "ValveSetting.LowFlowDeviation");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LowFlowDelay"), "ValveSetting.LowFlowDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("HighFlowDeviation"), "ValveSetting.HighFlowDeviation");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("HighFlowDelay"), "ValveSetting.HighFlowDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineFillTime"), "ValveSetting.LineFillTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigrationArea"), "ValveSetting.IrrigrationArea");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveColor"), "ValveSetting.ValveColor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PrecipitationRate"), "ValveSetting.PrecipitationRate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastFlow"), "ValveSetting.LastFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastFlow_Date"), "ValveSetting.LastFlow_Date");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastFlow_FlowTypeID"), "ValveSetting.LastFlow_FlowTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "ValveSetting.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertilizerConnected"), "ValveSetting.FertilizerConnected");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SetupNominalFlow"), "ValveSetting.SetupNominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StopOnFertFailure"), "ValveSetting.StopOnFertFailure");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Kc"), "ValveSetting.Kc");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlantTypeID"), "ValveSetting.PlantTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SubCategoryID"), "ValveSetting.SubCategoryID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"), "ValveSetting.WaterFactor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveID"), "ValveSetting.ValveID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveAlarm"), "ValveSetting.ValveAlarm");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "ValveSetting.FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IgnoreWaterCounter"), "ValveSetting.IgnoreWaterCounter");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IgnoreMasterValve"), "ValveSetting.IgnoreMasterValve");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AuxiliaryOutput"), "ValveSetting.AuxiliaryOutput");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CropTypeID"), "ValveSetting.CropTypeID");
                }
            });
        }

        [TestCase("S1Check")]
        [TestCase("S1")]
        [Test]
        [Category("Device")]
        [Category("Valves settings Post")]
        public void ValvesSettingsPost(string name)
        { 
            var valvesSettingsPostApi = new ValvesSettingsPostApi();
            var changedPayload = valvesSettingsPostApi.payload.ToJObject();
            changedPayload[0]["Name"] = name;
            valvesSettingsPostApi.RequestPayload = changedPayload.ToString();
            
            valvesSettingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            valvesSettingsPostApi.Run();

            Assert.IsTrue(valvesSettingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(valvesSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valvesSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var valvesSettingGetApi = new ValvesSettingsGetApi();
            valvesSettingGetApi.Run();

            var response = valvesSettingGetApi.ResponseBody["Body"]["ValveSetting"][0];
            Assert.IsTrue(valvesSettingGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valvesSettingGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(name, response.GetStringValueFromPath("Name"), "ValveSetting.Name - Check");
            Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("ValveNumber"), response.GetStringValueFromPath("OutputNumber"), "ValveSetting.OutputNumber - Check");
        }

        [Test]
        [Category("Device")]
        [Category("Valves settings pro Get")]
        public void ValvesSettingsProGet()
        {
            var valvesSettingsProGetApi = new ValvesSettingsProGetApi();
            valvesSettingsProGetApi.Run();

            var response = valvesSettingsProGetApi.ResponseBody["Body"];
            Assert.IsTrue(valvesSettingsProGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valvesSettingsProGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var typeIdNum = 0;
                foreach (var cropTypes in response["Types"]["LineTypeNum"])
                {
                    Assert.AreEqual(cropTypes.GetIntValueFromPath("TypeID"), typeIdNum++, "Types.LinetypeNum.TypeID");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Name"), "Types.LinetypeNum.Name");
                }

                foreach (var cropTypes in response["Types"]["Condition"])
                {
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("TypeID"), "Types.Condition.TypeID");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Name"), "Types.Condition.Name");
                }

                foreach (var cropTypes in response["Types"]["CropType"])
                {
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("TypeID"), "Types.CropType.TypeID");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Name"), "Types.CondiCropTypetion.Name");
                    Assert.IsNotEmpty(cropTypes.GetStringValueFromPath("Permanent"), "Types.CondiCropTypetion.Permanent");
                }

                foreach (var item in response["ValveSetting"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ValveSetting.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OutputNumber"), "ValveSetting.OutputNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "ValveSetting.Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LowFlowDeviation"), "ValveSetting.LowFlowDeviation");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LowFlowDelay"), "ValveSetting.LowFlowDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("HighFlowDeviation"), "ValveSetting.HighFlowDeviation");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("HighFlowDelay"), "ValveSetting.HighFlowDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineFillTime"), "ValveSetting.LineFillTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigrationArea"), "ValveSetting.IrrigrationArea");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveColor"), "ValveSetting.ValveColor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PrecipitationRate"), "ValveSetting.PrecipitationRate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastFlow"), "ValveSetting.LastFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastFlow_Date"), "ValveSetting.LastFlow_Date");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastFlow_FlowTypeID"), "ValveSetting.LastFlow_FlowTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "ValveSetting.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertilizerConnected"), "ValveSetting.FertilizerConnected");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SetupNominalFlow"), "ValveSetting.SetupNominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StopOnFertFailure"), "ValveSetting.StopOnFertFailure");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Kc"), "ValveSetting.Kc");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlantTypeID"), "ValveSetting.PlantTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SubCategoryID"), "ValveSetting.SubCategoryID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"), "ValveSetting.WaterFactor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveID"), "ValveSetting.ValveID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveAlarm"), "ValveSetting.ValveAlarm");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "ValveSetting.FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IgnoreWaterCounter"), "ValveSetting.IgnoreWaterCounter");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IgnoreMasterValve"), "ValveSetting.IgnoreMasterValve");
                    foreach (var condition in item["Condition"])
                    {
                        Assert.IsNotEmpty(condition.GetStringValueFromPath(""), "ValveSetting.Condition");
                    }
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CropTypeID"), "ValveSetting.CropTypeID");
                }
            });
        }

        [TestCase("S3Check")]
        [TestCase("S3")]
        [Test]
        [Category("Device")]
        [Category("Valves settings pro Post")]
        public void ValvesSettingsProPost(string name)
        {
            var valvesSettingsProPostApi = new ValvesSettingsProPostApi();
            var changedPayload = valvesSettingsProPostApi.payload.ToJObject();
            changedPayload[0]["Name"] = name;
            valvesSettingsProPostApi.RequestPayload = changedPayload.ToString();

            valvesSettingsProPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                                  .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("ProSerialNumber")));
            valvesSettingsProPostApi.Run();
            
            //Assert.IsTrue(valvesSettingsProPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(valvesSettingsProPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valvesSettingsProPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var valvesSettingsProGetApi = new ValvesSettingsProGetApi();
            valvesSettingsProGetApi.Run();

            var response = valvesSettingsProGetApi.ResponseBody["Body"]["ValveSetting"][1];
            Assert.IsTrue(valvesSettingsProGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valvesSettingsProGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.AreEqual(name, response.GetStringValueFromPath("Name"), "ValveSetting.Name - Check");
            Assert.AreEqual(3, response.GetIntValueFromPath("OutputNumber"), "ValveSetting.OutputNumber - Check");

        }

        [Test]
        [Category("Device")]
        [Category("Alert settings Get")]
        public void AlertSettingsGet()
        {
            var alertSettingsGetApi = new AlertSettingsGetApi(new Dictionary<string, string> { { "SendDefaults", "true" }, { "Type", "0" } });
            alertSettingsGetApi.Run();

            Assert.IsTrue(alertSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(alertSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = alertSettingsGetApi.ResponseBody;
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.EnableEmailNotify"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.ResumeAfterFailure"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertAutoCancel"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertsAutoResetTime"));

                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.AlertCode"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.SendMessage"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.ValveStopIrrigation"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.SeriesStopIrrigation"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.AlertAutoCancel"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.RetryNextIrrigation"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[0].Device_Settings.ForceCommunication"));

                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.AlertCode"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.SendMessage"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.ValveStopIrrigation"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.SeriesStopIrrigation"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.AlertAutoCancel"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.RetryNextIrrigation"));
                Assert.IsNotEmpty(result.GetStringValueFromPath("Body.AlertSetting[1].Default_Settings.ForceCommunication"));
            });


        }

        [Test]
        [Category("Device")]
        [Category("Alert settings Post")]
        public void AlertSettingsPost()
        {
            var alertSettingsPostApi = new AlertSettingsPostApi(new Dictionary<string, string> { { "ProjectID", "418" } });
            alertSettingsPostApi.RequestPayload = alertSettingsPostApi.payload;
            
            alertSettingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            alertSettingsPostApi.Run();

            Assert.IsTrue(alertSettingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(alertSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(alertSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var alertSettingsGetApi = new AlertSettingsGetApi(new Dictionary<string, string> { { "SendDefaults", "true" }, { "Type", "0" } });
            alertSettingsGetApi.Run();

            var result = alertSettingsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                Assert.IsTrue(result.GetBoolValueFromPath("EnableEmailNotify"), "EnableEmailNotify");
                Assert.IsTrue(result.GetBoolValueFromPath("ResumeAfterFailure"), "ResumeAfterFailure");
                Assert.IsFalse(result.GetBoolValueFromPath("AlertAutoCancel"), "AlertAutoCancel");
                Assert.AreEqual(0, result.GetIntValueFromPath("AlertsAutoResetTime"), "AlertsAutoResetTime");

                Assert.AreEqual(0, result.GetIntValueFromPath("AlertSetting[0].Device_Settings.AlertCode"), "Device_Settings.AlertCode");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[0].Device_Settings.SendMessage"), "Device_Settings.SendMessage");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[0].Device_Settings.ValveStopIrrigation"), "Device_Settings.ValveStopIrrigation");
                Assert.IsFalse(result.GetBoolValueFromPath("AlertSetting[0].Device_Settings.SeriesStopIrrigation"), "Device_Settings.SeriesStopIrrigation");
                Assert.IsFalse(result.GetBoolValueFromPath("AlertSetting[0].Device_Settings.AlertAutoCancel"), "Device_Settings.AlertAutoCancel");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[0].Device_Settings.RetryNextIrrigation"), "Device_Settings.RetryNextIrrigation");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[0].Device_Settings.ForceCommunication"), "Device_Settings.ForceCommunication");

                Assert.AreEqual(1, result.GetIntValueFromPath("AlertSetting[1].Default_Settings.AlertCode"), "Default_Settings.AlertCode");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[1].Default_Settings.SendMessage"), "Default_Settings.SendMessage");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[1].Default_Settings.ValveStopIrrigation"), "Default_Settings.ValveStopIrrigation");
                Assert.IsFalse(result.GetBoolValueFromPath("AlertSetting[1].Default_Settings.SeriesStopIrrigation"), "Default_Settings.SeriesStopIrrigation");
                Assert.IsFalse(result.GetBoolValueFromPath("AlertSetting[1].Default_Settings.AlertAutoCancel"), "Default_Settings.AlertAutoCancel");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[1].Default_Settings.RetryNextIrrigation"), "Default_Settings.RetryNextIrrigation");
                Assert.IsTrue(result.GetBoolValueFromPath("AlertSetting[1].Default_Settings.ForceCommunication"), "Default_Settings.ForceCommunication");
            });


        }

        [Test]
        [Category("Device")]
        [Category("Programs list")]
        public void ProgramsList()
        {
            var programsListGetApi = new ProgramsListGetApi();
            programsListGetApi.Run();

            Assert.IsTrue(programsListGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(programsListGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = programsListGetApi.ResponseBody;
            Assert.Multiple(() =>
            {
                var count = 0;
                foreach (var item in result["Body"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OrderNum"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Type"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramSetup"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramWaterUnit"));
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramStatus"));
                    count++;
                }

                Assert.AreEqual(8, count);
            });
        }

        [Test]
        [Category("Device")]
        [Category("Alerts")]
        public void Alerts()
        {
            var alertsGetApi = new AlertsGetApi(new Dictionary<string, string> { { "Cols", "RecordDate_DESC" } });
            alertsGetApi.Run();

            Assert.IsTrue(alertsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(alertsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = alertsGetApi.ResponseBody["Body"];
                foreach(var item in body)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AlertCode"), "AlertCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Noticed"), "Noticed");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Comment"), "Comment");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("InputName"), "InputName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TimeInput"), "TimeInput");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "IsPro");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDateTicks"), "ReceivedDateTicks");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Mark as read")]
        public void MarkAsRead()
        {
            var markAsReadPostApi = new MarkAsReadPostApi();
            markAsReadPostApi.RequestPayload = markAsReadPostApi.payload;
            markAsReadPostApi.Run();

            Assert.IsTrue(markAsReadPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(markAsReadPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Message info")]
        public void MessageInfo()
        {
            var messageInfoPostApi = new MessageInfoPostApi();
            messageInfoPostApi.RequestPayload = messageInfoPostApi.payload;
            messageInfoPostApi.Run();

            Assert.IsTrue(messageInfoPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(messageInfoPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Irrigations")]
        public void Irrigations()
        {
            var irrigationsGetApi = new IrrigationsGetApi(new Dictionary<string, string> { { "Cols", "StartDate_DESC" }, { "ProgramLevel", "false" } });
            irrigationsGetApi.Run();

            Assert.IsTrue(irrigationsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrigationsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = irrigationsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeStart"), "IrrigationCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeEnd"), "IrrigationCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterTime"), "WaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterTime"), "PlannedWaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterQuant"), "PlannedWaterQuant");
                    Assert.IsNotNull(item.GetStringValueFromPath("FertQuant"), "FertQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ErrorCode"), "ErrorCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fertilizer_PulseTypeID"), "Fertilizer_PulseTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedPulseViewTypeID"), "PlannedPulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramNumber"), "FertProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramName"), "FertProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "LineNum");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Log expansion irr")]
        public void LogExpansionIrr()
        {
            var logExpansionIrrGetApi = new LogExpansionIrrGetApi(new Dictionary<string, string>
                                        { { "StartTicks", "0" }, { "EndTicks", "0" }, { "ProgramNum", "0" }, { "LineNum", "1" } });
            logExpansionIrrGetApi.Run();

            Assert.IsTrue(logExpansionIrrGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(logExpansionIrrGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = logExpansionIrrGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeStart"), "IrrigationCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeEnd"), "IrrigationCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterTime"), "WaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterTime"), "PlannedWaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterQuant"), "PlannedWaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertQuant"), "FertQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ErrorCode"), "ErrorCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fertilizer_PulseTypeID"), "Fertilizer_PulseTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedPulseViewTypeID"), "PlannedPulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramNumber"), "FertProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramName"), "FertProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "LineNum");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Gen log expansion")]
        public void GenLogExpansion()
        {
            var genLogExpansionGetApi = new GenLogExpansionGetApi(new Dictionary<string, string>
                                        { { "StartTicks", "0" }, { "EndTicks", "0" } });
            genLogExpansionGetApi.Run();

            Assert.IsTrue(genLogExpansionGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(genLogExpansionGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = genLogExpansionGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Code"), "Code");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Data"), "Data");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Duration"), "Duration");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConnectionStatus"), "ConnectionStatus");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RemoteRecordID"), "RemoteRecordID");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Cached logs")]
        public void CachedLogs()
        {
            var genLogExpansionGetApi = new CachedLogsPostApi(new Dictionary<string, string>
                                        { { "StartTicks", "0" }, { "EndTicks", "0" } });
            genLogExpansionGetApi.RequestPayload = genLogExpansionGetApi.payload;
            genLogExpansionGetApi.Run();

            Assert.IsTrue(genLogExpansionGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(genLogExpansionGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = genLogExpansionGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result.GetStringValueFromPath("TotalCount"), "TotalCount");
                foreach (var item in result["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Code"), "Code");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Data"), "Data");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Duration"), "Duration");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConnectionStatus"), "ConnectionStatus");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RemoteRecordID"), "RemoteRecordID");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Logs")]
        public void Logs()
        {
            var logsPostApi = new LogsPostApi(new Dictionary<string, string>
                                        { { "StartTicks", "0" }, { "EndTicks", "0" } });
            logsPostApi.RequestPayload = logsPostApi.payload;
            logsPostApi.Run();

            Assert.IsTrue(logsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(logsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = logsPostApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result.GetStringValueFromPath("TotalCount"), "TotalCount");
                foreach (var item in result["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Code"), "Code");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Data"), "Data");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Duration"), "Duration");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConnectionStatus"), "ConnectionStatus");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RemoteRecordID"), "RemoteRecordID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Log expansion irr DWH - Not used")]
        public void LogExpansionIrrDWH()
        {
            var logExpansionIrrDWHGetApi = new LogExpansionIrrDWHGetApi(new Dictionary<string, string>
                                        { { "StartTicks", "0" }, { "EndTicks", "0" }, { "ProgramNum", "0" } });
            logExpansionIrrDWHGetApi.Run();

            Assert.IsTrue(logExpansionIrrDWHGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(logExpansionIrrDWHGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = logExpansionIrrDWHGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeStart"), "IrrigationCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeEnd"), "IrrigationCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterTime"), "WaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterTime"), "PlannedWaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertQuant"), "FertQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ErrorCode"), "ErrorCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fertilizer_PulseTypeID"), "Fertilizer_PulseTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramNumber"), "FertProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramName"), "FertProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "LineNum");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Gen log expansion DWH - Not used")]
        public void GenLogExpansionDWH()
        {
            var logExpansionIrrDWHGetApi = new GenLogExpansionDWHGetApi(new Dictionary<string, string>
                                        { { "StartTicks", "0" }, { "EndTicks", "0" } });
            logExpansionIrrDWHGetApi.Run();

            Assert.IsTrue(logExpansionIrrDWHGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(logExpansionIrrDWHGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = logExpansionIrrDWHGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Code"), "Code");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Data"), "Data");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Duration"), "Duration");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConnectionStatus"), "ConnectionStatus");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RemoteRecordID"), "RemoteRecordID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Add device to map")]
        public void AddDeviceToMap()
        {
            var addDeviceToMapPostApi = new AddDeviceToMapPostApi();
            var addToMapPayload = addDeviceToMapPostApi.payload.ToJObject();
            addToMapPayload["Map_Latitude"] = 31.158050182802377;
            addToMapPayload["Map_Longitude"] = 34.772077526367944;
            addDeviceToMapPostApi.RequestPayload = addToMapPayload.ToString();

            addDeviceToMapPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            addDeviceToMapPostApi.Run();

            Assert.IsTrue(addDeviceToMapPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(addDeviceToMapPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(addDeviceToMapPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Remove device from map
            var removeDeviceFromMapPostApi = new RemoveDeviceFromMapPostApi();
            removeDeviceFromMapPostApi.Run();

            Assert.IsTrue(removeDeviceFromMapPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(removeDeviceFromMapPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Remove device from map")]
        public void RemoveDeviceFromMap()
        {
            // Add in case doesn't exist on map
            var addDeviceToMapPostApi = new AddDeviceToMapPostApi();
            var addToMapPayload = addDeviceToMapPostApi.payload.ToJObject();
            addToMapPayload["Map_Latitude"] = 31.158050182802377;
            addToMapPayload["Map_Longitude"] = 34.772077526367944;
            addDeviceToMapPostApi.RequestPayload = addToMapPayload.ToString();
            addDeviceToMapPostApi.Run();

            Assert.IsTrue(addDeviceToMapPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(addDeviceToMapPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Remove after add
            var removeDeviceFromMapPostApi = new RemoveDeviceFromMapPostApi();

            removeDeviceFromMapPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            removeDeviceFromMapPostApi.Run();

            Assert.IsTrue(removeDeviceFromMapPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(removeDeviceFromMapPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(removeDeviceFromMapPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Get weather unit")]
        public void GetWathearUnit()
        {
            var getWeatherUnitGetApi = new GetWeatherUnitGetApi(new Dictionary<string, string> { { "lat", "29.769631504380754" },
                                        { "lon", "38.09852647530769" } });
            getWeatherUnitGetApi.Run();

            Assert.IsTrue(getWeatherUnitGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getWeatherUnitGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = getWeatherUnitGetApi.ResponseBody["Body"];

            Assert.IsNotEmpty(result.GetStringValueFromPath("IsValid"), "IsValid");
            Assert.AreEqual("29.769631504380754", result.GetStringValueFromPath("location.lat"), "lat");
            Assert.AreEqual("38.09852647530769", result.GetStringValueFromPath("location.lon"), "lon");
            Assert.IsNotEmpty(result.GetStringValueFromPath("forecastDays"), "forecastDays");
            Assert.Multiple(() =>
            {
                foreach (var item in result["forecastList"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("date"), "date");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("dt"), "dt");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DayNum"), "DayNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("description"), "description");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("icon.Provider_Url"), "Provider_Url");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("icon.Code"), "Code");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("icon.Day_Url"), "Day_Url");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("icon.Night_Url"), "Night_Url");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WindSpeed.ValueType"), "WindSpeed.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WindSpeed.Max"), "WindSpeed.Max");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WindSpeed.Avg"), "WindSpeed.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WindSpeed.Min"), "WindSpeed.Min");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Humidity.ValueType"), "Humidity.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Humidity.Max"), "Humidity.Max");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Humidity.Avg"), "Humidity.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Humidity.Min"), "Humidity.Min");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Fahrenheit.ValueType"), "Temp_Fahrenheit.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Fahrenheit.Max"), "Temp_Fahrenheit.Max");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Fahrenheit.Avg"), "Temp_Fahrenheit.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Fahrenheit.Min"), "Temp_Fahrenheit.Min");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Celsius.ValueType"), "Temp_Celsius.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Celsius.Max"), "Temp_Celsius.Max");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Celsius.Avg"), "Temp_Celsius.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Temp_Celsius.Min"), "Temp_Celsius.Min");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Inch.ValueType"), "Prec_Inch.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Inch.Night"), "Prec_Inch.Night");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Inch.Day"), "Prec_Inch.Day");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Inch.Avg"), "Prec_Inch.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_mm.ValueType"), "Prec_mm.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_mm.Night"), "Prec_mm.Night");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_mm.Day"), "Prec_mm.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_mm.Avg"), "Prec_mm.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Percent.ValueType"), "Prec_Percent.ValueType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Percent.Night"), "Prec_Percent.Night");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Percent.Day"), "Prec_Percent.Day");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Prec_Percent.Avg"), "Prec_Percent.Avg");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ForecastID"), "ForecastID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ET0"), "ET0");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Info setting Get")]
        public void InfoSettingGet()
        {
            var infoSettingGetApi = new InfoSettingGetApi();
            infoSettingGetApi.Run();

            Assert.IsTrue(infoSettingGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(infoSettingGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = infoSettingGetApi.ResponseBody["Body"];
            Assert.IsNotEmpty(result.GetStringValueFromPath("CategorySetting.ContractorID"), "CategorySetting.ContractorID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("CategorySetting.DefinitionID"), "CategorySetting.DefinitionID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("CategorySetting.InspectorID"), "CategorySetting.InspectorID");
            Assert.IsNotEmpty(result.GetStringValueFromPath("CategorySetting.RegionID"), "CategorySetting.RegionID");

            var types = result["Types"];
            foreach (var item in types["LandSizeTypes"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "LandSizeTypes.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "LandSizeTypes.Name");
            }

            foreach (var item in types["TimesZone"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("ZoneID"), "TimesZone.ZoneID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("DisplayName"), "TimesZone.DisplayName");
                Assert.IsNotEmpty(item.GetStringValueFromPath("GMTOffset"), "TimesZone.GMTOffset");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ActualOffset"), "TimesZone.ActualOffset");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ManualOffset"), "TimesZone.ManualOffset");
                Assert.IsNotEmpty(item.GetStringValueFromPath("SystemZoneID"), "TimesZone.SystemZoneID");
            }

            foreach (var item in types["UnitPulseViewTypes"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "UnitPulseViewTypes.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "UnitPulseViewTypes.Name");
            }

            foreach (var item in types["BatteryTypes"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "BatteryTypes.ID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "BatteryTypes.Name");
            }

            var deviceInfo = result["DeviceInfo"];
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SN"), "DeviceInfo.SN");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ControlUnitID"), "DeviceInfo.ControlUnitID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitName"), "DeviceInfo.UnitName");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelID"), "DeviceInfo.ModelID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelView"), "DeviceInfo.ModelView");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ModelVersion"), "DeviceInfo.ModelVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("WaterFactor"), "DeviceInfo.WaterFactor");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Signal"), "DeviceInfo.Signal");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ValveNum"), "DeviceInfo.ValveNum");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CommTypeID"), "DeviceInfo.CommTypeID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Status"), "DeviceInfo.Status");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CurrentConfigID"), "DeviceInfo.CurrentConfigID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CommUnit_AppVersion"), "DeviceInfo.CommUnit_AppVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("BootLoaderVersion"), "DeviceInfo.BootLoaderVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LandTypeID"), "DeviceInfo.LandTypeID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonBudget"), "DeviceInfo.SeasonBudget");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsGSIAg"), "DeviceInfo.IsGSIAg");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Description"), "DeviceInfo.Description");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Address"), "DeviceInfo.Address");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Phone"), "DeviceInfo.Phone");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonEndDate"), "DeviceInfo.SeasonEndDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonStartDate"), "DeviceInfo.SeasonStartDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonEndDateTicks"), "DeviceInfo.SeasonEndDateTicks");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("SeasonStartDateTicks"), "DeviceInfo.SeasonStartDateTicks");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ConnectionStatus"), "DeviceInfo.ConnectionStatus");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ControllerState"), "DeviceInfo.ControllerState");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("InputsState"), "DeviceInfo.InputsState");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("OutputState"), "DeviceInfo.OutputState");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ShortedValve"), "DeviceInfo.ShortedValve");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsSync"), "DeviceInfo.IsSync");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DeviceAlarm"), "DeviceInfo.DeviceAlarm");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("RainChance"), "DeviceInfo.RainChance");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Map_Latitude"), "DeviceInfo.Map_Latitude");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("Map_Longitude"), "DeviceInfo.Map_Longitude");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ETmanual"), "DeviceInfo.ETmanual");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("ET0"), "DeviceInfo.ET0");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("FlagET"), "DeviceInfo.FlagET");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsPRO"), "DeviceInfo.IsPRO");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LineNum"), "DeviceInfo.LineNum");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog1"), "DeviceInfo.DigitalAnalog1");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog2"), "DeviceInfo.DigitalAnalog2");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog3"), "DeviceInfo.DigitalAnalog3");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("TimeZoneID"), "DeviceInfo.TimeZoneID");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IsMetric"), "DeviceInfo.IsMetric");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LandType"), "DeviceInfo.LandType");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("NewHardware"), "DeviceInfo.NewHardware");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitPulseView"), "DeviceInfo.UnitPulseView");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("CorrectVersion"), "DeviceInfo.CorrectVersion");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("DigitalAnalog3"), "DeviceInfo.DigitalAnalog3");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("LocalProgramWaterUnit"), "DeviceInfo.LocalProgramWaterUnit");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("WaterFactorUnit"), "DeviceInfo.WaterFactorUnit");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("PaymentAlert"), "DeviceInfo.PaymentAlert");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitPaymentExpirationDate"), "DeviceInfo.UnitPaymentExpirationDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitLastPaymentDate"), "DeviceInfo.UnitLastPaymentDate");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnitPaymentStatus"), "DeviceInfo.UnitPaymentStatus");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("UnpaidUnit"), "DeviceInfo.UnpaidUnit");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("IrrimaxApiKey"), "DeviceInfo.IrrimaxApiKey");
            Assert.IsNotEmpty(deviceInfo.GetStringValueFromPath("NewVer"), "DeviceInfo.NewVer");

            var listOfVersions = deviceInfo["ListOfVersions"];
            foreach (var item in listOfVersions)
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("VersionType"), "ListOfVersions.VersionType");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Version"), "ListOfVersions.Version");
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeOfController"), "ListOfVersions.TypeOfController");
            }

            var irrgationSetting = result["IrrgationSetting"];
            var restrictedDates = irrgationSetting["RestrictedDates"];
            foreach (var date in restrictedDates)
            {
                Assert.IsNotEmpty(date.GetStringValueFromPath(""), "IrrgationSetting.TypeOfController");
            }
            Assert.IsNotEmpty(irrgationSetting.GetStringValueFromPath("IsLocalSequenceActive"), "IrrgationSetting.IsLocalSequenceActive");
            Assert.IsNotEmpty(irrgationSetting.GetStringValueFromPath("NotProgramsAsQueue"), "IrrgationSetting.NotProgramsAsQueue");

            var communicationTimes = result["CommunicationTimes"];
            if (communicationTimes.Type != JTokenType.Null)
            {
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ConnectionInterval.TotalSeconds"), "ConnectionInterval.TotalSeconds");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ConnectionInterval.Factor"), "ConnectionInterval.Factor");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ConnectionInterval.Interval"), "ConnectionInterval.Interval");

                var timeInDay = communicationTimes["timeInDay"];
                foreach (var item in timeInDay)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath(""), "TimeInDay");

                }

                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemStartTime"), "ModemStartTime");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemEndTime"), "ModemEndTime");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemRetryNum"), "ModemRetryNum");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ModemRetryDelay"), "ModemRetryDelay");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("ActiveDaliyCommunication"), "ActiveDaliyCommunication");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("CycleForSensorLog"), "CycleForSensorLog");
                Assert.IsNotEmpty(communicationTimes.GetStringValueFromPath("CycleForSensorEvents"), "CycleForSensorEvents");
            }

            Assert.IsNotEmpty(result.GetStringValueFromPath("ETtype"), "ETtype");

            var eTmanualSettigs = result["ETmanualSettigs"];
            foreach (var item in eTmanualSettigs)
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("ET0"), "ETmanualSettigs.ET0");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Rain"), "ETmanualSettigs.Rain");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Month"), "ETmanualSettigs.Month");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Week"), "ETmanualSettigs.Week");
            }

            Assert.IsNotEmpty(result.GetStringValueFromPath("FactorRain"), "FactorRain");
            Assert.IsNotEmpty(result.GetStringValueFromPath("EnableFactorRain"), "EnableFactorRain");
            Assert.IsNotEmpty(result.GetStringValueFromPath("ActualRainMM"), "ActualRainMM");

            var systemSettingsView = result["SystemSettingsView"];
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("BattaryAlarmLevel"), "SystemSettingsView.BattaryAlarmLevel");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("BattaryPauseLevel"), "SystemSettingsView.BattaryPauseLevel");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("BattaryDataCycle"), "SystemSettingsView.BattaryDataCycle");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("CycleForSensorLog"), "SystemSettingsView.CycleForSensorLog");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("BattaryGoodLevel"), "SystemSettingsView.BattaryGoodLevel");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("CycleSensorEvents"), "SystemSettingsView.CycleSensorEvents");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("ExistingBackupBattery"), "SystemSettingsView.ExistingBackupBattery");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("SensorMeasureDelay"), "SystemSettingsView.SensorMeasureDelay");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("ControlSensorCycle"), "SystemSettingsView.ControlSensorCycle");
            Assert.IsNotEmpty(systemSettingsView.GetStringValueFromPath("BatteryType"), "SystemSettingsView.BatteryType");

        }

        [TestCase("30")]
        [Test]
        [Category("Device")]
        [Category("Info setting Post")]
        public void InfoSettingPost(string newData)
        {
            var infoSettingPostApi = new InfoSettingPostApi();
            var changedPayload = infoSettingPostApi.payload.ToJObject();
            changedPayload["DeviceInfo"]["Map_Latitude"] = newData;
            changedPayload["DeviceInfo"]["Map_Longitude"] = newData;
            infoSettingPostApi.RequestPayload = changedPayload.ToString();

            infoSettingPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            infoSettingPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 7,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 7,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            infoSettingPostApi.Run();

            Assert.IsTrue(infoSettingPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(infoSettingPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(infoSettingPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var infoSettingGetApi = new InfoSettingGetApi();
            infoSettingGetApi.Run();

            var result = infoSettingGetApi.ResponseBody["Body"]["DeviceInfo"];

            Assert.AreEqual(newData, result.GetStringValueFromPath("Map_Latitude"), "DeviceInfo.Map_Latitude");
            Assert.AreEqual(newData, result.GetStringValueFromPath("Map_Longitude"), "DeviceInfo.Map_Longitude");
        }

        [Test]
        [Category("Device")]
        [Category("Irrigation settings Get")]
        public void IrrigationSettingsGet()
        {
            var irrigationSettingsGetApi = new IrrigationSettingsGetApi();
            irrigationSettingsGetApi.Run();

            Assert.IsTrue(irrigationSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrigationSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = irrigationSettingsGetApi.ResponseBody["Body"];
            foreach (var item in result["Types"]["WaterPulseTypes"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "WaterPulseTypes.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "WaterPulseTypes.Name");
            }

            foreach (var item in result["Types"]["AvailableInputs"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "AvailableInputs.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "AvailableInputs.Name");
            }

            foreach (var item in result["Types"]["Condition"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Condition.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Condition.Name");
            }

            foreach (var item in result["Types"]["AvailableOutput"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "AvailableOutput.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "AvailableOutput.Name");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "AvailableOutput.LineNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("OutputName"), "AvailableOutput.OutputName");
            }

            foreach (var item in result["Types"]["LineStatus"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "LineStatus.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "LineStatus.Name");
            }

            foreach (var item in result["PipeLineSetting"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.WaterFactor"), "MainPipeSettings.WaterFactor");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.PauseInputConditionNumber"), "MainPipeSettings.PauseInputConditionNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.IsLocalSequenceActive"), "MainPipeSettings.IsLocalSequenceActive");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ProgramsAsQueue"), "MainPipeSettings.ProgramsAsQueue");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DSundayState"), "MainPipeSettings.DSundayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DMondayState"), "MainPipeSettings.DMondayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DTuesdayState"), "MainPipeSettings.DTuesdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DWednesdayState"), "MainPipeSettings.DWednesdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DThursdayState"), "MainPipeSettings.DThursdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DFridayState"), "MainPipeSettings.DFridayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.DSaturdayState"), "MainPipeSettings.DSaturdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.OverlapTime"), "MainPipeSettings.OverlapTime");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ProgramHFilter"), "MainPipeSettings.ProgramHFilter");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.UseMaster"), "MainPipeSettings.UseMaster");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ValveOpenningPattern"), "MainPipeSettings.ValveOpenningPattern");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ValveClosingPattern"), "MainPipeSettings.ValveClosingPattern");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ValveCloseDelay"), "MainPipeSettings.ValveCloseDelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ValveOpenDelay"), "MainPipeSettings.ValveOpenDelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.MasterOutputNumber"), "MainPipeSettings.MasterOutputNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.MasterOutputName"), "MainPipeSettings.MasterOutputName");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.MainPipNumber"), "MainPipeSettings.MainPipNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.ValveOverlapping"), "MainPipeSettings.ValveOverlapping");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.Define"), "MainPipeSettings.Define");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.Name"), "MainPipeSettings.Name");
                Assert.IsNotNull(item.GetObjectValueFromPath("MainPipeSettings.LogicCondition"), "MainPipeSettings.LogicCondition");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.EnableAutoCancleAlarm"), "MainPipeSettings.EnableAutoCancleAlarm");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.AlertsAutoResetTime"), "MainPipeSettings.AlertsAutoResetTime");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.EnableFertilizer"), "MainPipeSettings.EnableFertilizer");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.UpdateDataCycle"), "MainPipeSettings.UpdateDataCycle");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.EnableValveFertEventCycle"), "MainPipeSettings.EnableValveFertEventCycle");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainPipeSettings.FlushingDataCycle"), "MainPipeSettings.FlushingDataCycle");

                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DSundayState"), "ValidDays.DSundayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DMondayState"), "ValidDays.DMondayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DTuesdayState"), "ValidDays.DTuesdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DWednesdayState"), "ValidDays.DWednesdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DThursdayState"), "ValidDays.DThursdayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DFridayState"), "ValidDays.DFridayState");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ValidDays.DSaturdayState"), "ValidDays.DSaturdayState");


                Assert.IsNotEmpty(item.GetStringValueFromPath("RainSensor.IsEnabled"), "RainSensor.IsEnabled");
                Assert.IsNotEmpty(item.GetStringValueFromPath("RainSensor.DaysStopper"), "RainSensor.DaysStopper");
                Assert.IsNotEmpty(item.GetStringValueFromPath("RainSensor.NC"), "RainSensor.NC");
                Assert.IsNotEmpty(item.GetStringValueFromPath("RainSensor.RainInputConditionNumber"), "RainSensor.RainInputConditionNumber");
            }

            foreach (var item in result["WaterMeter"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("PulseSize"), "WaterMeter.PulseSize");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MeterType"), "WaterMeter.MeterType");
                Assert.IsNotEmpty(item.GetStringValueFromPath("IsEnabled"), "WaterMeter.IsEnabled");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PulseTypeID"), "WaterMeter.PulseTypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FlowTypeID"), "WaterMeter.FlowTypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("NoWaterPulseDelay"), "WaterMeter.NoWaterPulseDelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LeakageLimit"), "WaterMeter.LeakageLimit");
                Assert.IsNotEmpty(item.GetStringValueFromPath("InputNumber"), "WaterMeter.InputNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("WaterMeterType"), "WaterMeter.WaterMeterType");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "WaterMeter.PulseViewTypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("WaterMeterNumber"), "WaterMeter.WaterMeterNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "WaterMeter.LineNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("WaterMeterID"), "WaterMeter.WaterMeterID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("EnableAutoCancleAlarm"), "WaterMeter.EnableAutoCancleAlarm");
                Assert.IsNotEmpty(item.GetStringValueFromPath("AlertsAutoResetTime"), "WaterMeter.AlertsAutoResetTime");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ReleaseWaterLeakageFault"), "WaterMeter.ReleaseWaterLeakageFault");
            }
        }

        [TestCase(10)]
        [Test]
        [Category("Device")]
        [Category("Irrigation settings Post")]
        public void IrrigationSettingsPost(int newData)
        {
            var irrigationSettingsPostApi = new IrrigationSettingsPostApi();
            var changedPayload = irrigationSettingsPostApi.payload.ToJObject();
            changedPayload["WaterMeter"][0]["LeakageLimit"] = newData;
            irrigationSettingsPostApi.RequestPayload = changedPayload.ToString();

            irrigationSettingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            irrigationSettingsPostApi.Run();

            Assert.IsTrue(irrigationSettingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(irrigationSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrigationSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var irrigationSettingsGetApi = new IrrigationSettingsGetApi();
            irrigationSettingsGetApi.Run();

            Assert.IsTrue(irrigationSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(irrigationSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = irrigationSettingsGetApi.ResponseBody["Body"]["WaterMeter"][0];
            Assert.AreEqual(newData, result.GetIntValueFromPath("LeakageLimit"), "WaterMeter.LeakageLimit");
        }

        [Test]
        [Category("Device")]
        [Category("Update water quantity")]
        public void UpdateWaterQuantity()
        {
            var updateWaterQuantityPostApi = new UpdateWaterQuantityPostApi();
            updateWaterQuantityPostApi.RequestPayload = updateWaterQuantityPostApi.payload;
            updateWaterQuantityPostApi.Run();

            Assert.IsTrue(updateWaterQuantityPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(updateWaterQuantityPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Device")]
        [Category("Fert settings Get")]
        public void FertSettingsGet()
        {
            var fertsettingsGetApi = new FertSettingsGetApi();
            fertsettingsGetApi.Run();

            Assert.IsTrue(fertsettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertsettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertsettingsGetApi.ResponseBody["Body"];
            foreach (var item in result["Types"]["FertPulseType"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertPulseType"), "FertPulseType.FertPulseType");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Quant"), "FertPulseType.Quant");
            }

            foreach (var item in result["Types"]["AvailableInputs"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "AvailableInputs.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "AvailableInputs.Name");
            }

            foreach (var item in result["Types"]["AvailableOutput"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "AvailableOutput.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "AvailableOutput.Name");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "AvailableOutput.LineNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("OutputName"), "AvailableOutput.OutputName");
            }

            foreach (var item in result["Types"]["AnalogItems"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "AnalogItems.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "AnalogItems.Name");
            }

            foreach (var item in result["Types"]["FertTypes"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "FertTypes.TypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "FertTypes.Name");
            }

            foreach (var item in result["FertCenter"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertCenterNumber"), "FertCenter.FertCenterNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "FertCenter.Name");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertMaxPumpNumber"), "FertCenter.FertMaxPumpNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("StartDelay"), "FertCenter.StartDelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ECSensorNum"), "FertCenter.ECSensorNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PHSensorNum"), "FertCenter.PHSensorNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("DelayBefore"), "FertCenter.DelayBefore");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertAout"), "FertCenter.FertAout");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MaxECInc"), "FertCenter.MaxECInc");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MaxECDec"), "FertCenter.MaxECDec");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MaxPHInc"), "FertCenter.MaxPHInc");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MaxPHDec"), "FertCenter.MaxPHDec");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LowECAlert"), "FertCenter.LowECAlert");
                Assert.IsNotEmpty(item.GetStringValueFromPath("HighECAlert"), "FertCenter.HighECAlert");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LowPHAlert"), "FertCenter.LowPHAlert");
                Assert.IsNotEmpty(item.GetStringValueFromPath("HighPHAlert"), "FertCenter.HighPHAlert");
                Assert.IsNotEmpty(item.GetStringValueFromPath("AlertDelay"), "FertCenter.AlertDelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LowECStop"), "FertCenter.LowECStop");
                Assert.IsNotEmpty(item.GetStringValueFromPath("HighECStop"), "FertCenter.HighECStop");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LowPHStop"), "FertCenter.LowPHStop");
                Assert.IsNotEmpty(item.GetStringValueFromPath("HighPHStop"), "FertCenter.HighPHStop");
                Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationStopDelay"), "FertCenter.IrrigationStopDelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ECKp"), "FertCenter.ECKp");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ECKi"), "FertCenter.ECKi");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ECKd"), "FertCenter.ECKd");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PHKp"), "FertCenter.PHKp");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PHKd"), "FertCenter.PHKd");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PHKi"), "FertCenter.PHKi");
                Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "FertCenter.LineNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("MainFertDuringNoFert"), "FertCenter.MainFertDuringNoFert");
                Assert.IsNotEmpty(item.GetStringValueFromPath("StopInFertPumpFail"), "FertCenter.StopInFertPumpFail");
                Assert.IsNotEmpty(item.GetStringValueFromPath("AutoResetEnable"), "FertCenter.AutoResetEnable");
                Assert.IsNotEmpty(item.GetStringValueFromPath("OverlapLockChange"), "FertCenter.OverlapLockChange");
                Assert.IsNotEmpty(item.GetStringValueFromPath("OperationDueAverageFlow"), "FertCenter.OperationDueAverageFlow");
                Assert.IsNotNull(item.GetObjectValueFromPath("FertPumps"), "FertCenter.FertPumps");
                Assert.IsNotEmpty(item.GetStringValueFromPath("AutoFertMethod"), "FertCenter.AutoFertMethod");
                Assert.IsNotEmpty(item.GetStringValueFromPath("OutputNumberFertMaster"), "FertCenter.OutputNumberFertMaster");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Message_StartFertilizer"), "FertCenter.Message_StartFertilizer");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Message_StopFertilizer"), "FertCenter.Message_StopFertilizer");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PercentFromRequired"), "FertCenter.PercentFromRequired");
            }

            foreach (var item in result["Fertilizer"])
            {
                Assert.IsNotEmpty(item.GetStringValueFromPath("UseCustomFertilizerPulse"), "Fertilizer.UseCustomFertilizerPulse");
                Assert.IsNotEmpty(item.GetStringValueFromPath("OutputNumber"), "Fertilizer.OutputNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("ContinuousFert"), "Fertilizer.ContinuousFert");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PulseSize"), "Fertilizer.PulseSize");
                Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "Fertilizer.PulseViewTypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertPulseDuration"), "Fertilizer.FertPulseDuration");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FerlizerFaillureTime"), "Fertilizer.FerlizerFaillureTime");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Leakage"), "Fertilizer.Leakage");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertilizerType"), "Fertilizer.FertilizerType");

                var pulseTypeID = item["PulseTypeID"];
                if (pulseTypeID.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(pulseTypeID.GetStringValueFromPath("FertPulseType"), "Fertilizer.PulseTypeID.FertPulseType");
                    Assert.IsNotEmpty(pulseTypeID.GetStringValueFromPath("Quant"), "Fertilizer.PulseTypeID.Quant");
                }
                Assert.IsNotEmpty(item.GetStringValueFromPath("FlowTypeID"), "Fertilizer.FlowTypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("IsEnabled"), "Fertilizer.IsEnabled");
                Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "Fertilizer.NominalFlow");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertilizerID"), "Fertilizer.FertilizerID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertCounterInputNum"), "Fertilizer.FertCounterInputNum");
                Assert.IsNotEmpty(item.GetStringValueFromPath("UseFertMeter4Error"), "Fertilizer.UseFertMeter4Error");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertLeakDetectActive"), "Fertilizer.FertLeakDetectActive");
                Assert.IsNotEmpty(item.GetStringValueFromPath("UseRelay"), "Fertilizer.UseRelay");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertNumber"), "Fertilizer.FertNumber");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertTypeID"), "Fertilizer.FertTypeID");
                Assert.IsNotEmpty(item.GetStringValueFromPath("FertType"), "Fertilizer.FertType");
                Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Fertilizer.Name");
                Assert.IsNotEmpty(item.GetStringValueFromPath("RecommendedCycle"), "Fertilizer.RecommendedCycle");
                Assert.IsNotNull(item.GetObjectValueFromPath("ProgramNum"), "Fertilizer.ProgramNum");
            }
        }

        [Test]
        [Category("Device")]
        [Category("Fert settings Post - Pro unit")]
        public void FertSettingsPost()
        {
            var fertSettingsPostApi = new FertSettingsPostApi();
            var changedPayload = fertSettingsPostApi.payload.ToJObject();
            var newLeakData = 10;
            var PercentFromRequired = 80;
            changedPayload["FertCenter"][0]["PercentFromRequired"] = PercentFromRequired;
            changedPayload["Fertilizer"][0]["Leakage"] = newLeakData;
            fertSettingsPostApi.RequestPayload = changedPayload.ToString();
            
            fertSettingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            fertSettingsPostApi.Run();

            Assert.IsTrue(fertSettingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(fertSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var fertSettingsGetApi = new FertSettingsGetApi();
            fertSettingsGetApi.Run();

            Assert.IsTrue(fertSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertSettingsGetApi.ResponseBody["Body"];

            var fertCenter = result["FertCenter"][0];
            Assert.AreEqual(PercentFromRequired, fertCenter.GetIntValueFromPath("PercentFromRequired"), "FertCenter.PercentFromRequired");

            var fertilizer = result["Fertilizer"][0];
            Assert.AreEqual(newLeakData, fertilizer.GetIntValueFromPath("Leakage"), "Fertilizer.Leakage");
        }

        [Test]
        [Category("Device")]
        [Category("InOut settings Get")]
        public void InOutSettingsGet()
        {
            var inOutSettingsGetApi = new InOutSettingsGetApi();
            inOutSettingsGetApi.Run();

            Assert.IsTrue(inOutSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(inOutSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = inOutSettingsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result["InputSetting"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConditionInputNumber"), "InputSetting.ConditionInputNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OnDelay"), "InputSetting.OnDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OffDelay"), "InputSetting.OffDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "InputSetting.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "InputSetting.TypeID");
                }

                foreach (var item in result["OutputSetting"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "OutputSetting.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "OutputSetting.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UseRelay"), "OutputSetting.UseRelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OutputComandDuration"), "OutputSetting.OutputComandDuration");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("InOut settings Post")]
        public void InOutSettingsPost()
        {
            var inOutSettingsPostApi = new InOutSettingsPostApi();
            var changedPayload = inOutSettingsPostApi.payload.ToJObject();
            var conditionInputNumber = 1;
            var name = "Input Condition One";
            var onDelay = 3;
            var offDelay = 3;
            var typeID = 1;
            changedPayload["InputSetting"][0]["ConditionInputNumber"] = conditionInputNumber;
            changedPayload["InputSetting"][0]["Name"] = name;
            changedPayload["InputSetting"][0]["OnDelay"] = onDelay;
            changedPayload["InputSetting"][0]["OffDelay"] = offDelay;
            changedPayload["InputSetting"][0]["TypeID"] = typeID;
            inOutSettingsPostApi.RequestPayload = changedPayload.ToString();

            inOutSettingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("ProSerialNumber")));

            inOutSettingsPostApi.Run();

            Assert.IsTrue(inOutSettingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(inOutSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(inOutSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var inOutSettingsGetApi = new InOutSettingsGetApi();
            inOutSettingsGetApi.Run();

            Assert.IsTrue(inOutSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(inOutSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = inOutSettingsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                var inputItem = result["InputSetting"][0];
                Assert.AreEqual(conditionInputNumber, inputItem.GetIntValueFromPath("ConditionInputNumber"), "InputSetting.ConditionInputNumber");
                Assert.AreEqual(onDelay, inputItem.GetIntValueFromPath("OnDelay"), "InputSetting.OnDelay");
                Assert.AreEqual(offDelay, inputItem.GetIntValueFromPath("OffDelay"), "InputSetting.OffDelay");
                Assert.AreEqual(name, inputItem.GetStringValueFromPath("Name"), "InputSetting.Name");
                Assert.AreEqual(typeID, inputItem.GetIntValueFromPath("TypeID"), "InputSetting.TypeID");
            });
        }

        [Test]
        [Category("Device")]
        [Category("Analogs")]
        public void Analogs()
        {
            var analogsGetApi = new AnalogsGetApi();
            analogsGetApi.Run();

            Assert.IsTrue(analogsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(analogsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = analogsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorNumber"), "SensorNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorType"), "SensorType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("MinValueAlert"), "MinValueAlert");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("MaxValueAlert"), "MaxValueAlert");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastValue"), "LastValue");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastDateUpadte"), "LastDateUpadte");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastDate"), "LastDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Accuracy"), "Accuracy");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Analog settings Get")]
        public void AnalogSettingsGet()
        {
            var analogSettingsGetApi = new AnalogSettingsGetApi(new Dictionary<string, string>
                                       { { "SensorNumber", "1" }, {"AC", "true"} });
            analogSettingsGetApi.Run();

            Assert.IsTrue(analogSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(analogSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = analogSettingsGetApi.ResponseBody["Body"];
            var types = result["Types"];
            var sensorSetting = result["SensorSetting"];
            Assert.Multiple(() =>
            {
                foreach (var item in types["SensorTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "SensorTypes.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "SensorTypes.Name");
                }

                foreach (var item in types["ShowValueType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "ShowValueType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ShowValueType.Name");
                }

                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("SensorID"), "SensorSetting.SensorID");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("SensorNumber"), "SensorSetting.SensorNumber");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("SensorType"), "SensorSetting.SensorType");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("Name"), "SensorSetting.Name");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("MinValueAlert"), "SensorSetting.MinValueAlert");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("MaxValueAlert"), "SensorSetting.MaxValueAlert");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("LastValue"), "SensorSetting.LastValue");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("LastDateUpadte"), "SensorSetting.LastDateUpadte");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("LastDate"), "SensorSetting.LastDate");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("Calibration"), "SensorSetting.Calibration");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("MinValue"), "SensorSetting.MinValue");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("MaxValue"), "SensorSetting.MaxValue");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("Accuracy"), "SensorSetting.Accuracy");
                Assert.IsNotEmpty(sensorSetting.GetStringValueFromPath("AlarmDelay"), "SensorSetting.AlarmDelay");
            });
        }

        [Test]
        [Category("Device")]
        [Category("Analog settings Post")]
        public void AnalogSettingsPost()
        {
            var analogSettingsPostApi = new AnalogSettingsPostApi(new Dictionary<string, string>
                                       { { "SensorNumber", "1" } });
            var newName = "Analog 1 Check";
            var changedPayload = analogSettingsPostApi.payload.ToJObject();
            changedPayload["Name"] = newName;
            analogSettingsPostApi.RequestPayload = changedPayload.ToString();

            analogSettingsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("ProSerialNumber")));

            analogSettingsPostApi.Run();

            Assert.IsTrue(analogSettingsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(analogSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(analogSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var analogSettingsGetApi = new AnalogSettingsGetApi(new Dictionary<string, string>
                                       { { "SensorNumber", "1" }, {"AC", "true"} });
            analogSettingsGetApi.Run();

            var result = analogSettingsGetApi.ResponseBody["Body"]["SensorSetting"];
            Assert.AreEqual(newName, result.GetStringValueFromPath("Name"), "SensorSetting.Name");

        }

        [Test]
        [Category("Device")]
        [Category("Analog logs")]
        public void AnalogLogs()
        {
            var analogLogsGetApi = new AnalogLogsGetApi(new Dictionary<string, string>{ {"SensorNumber", "2" },
                                   { "FromTicks", "1644098400000" }, { "ToTicks", "1644184800000" } });
            analogLogsGetApi.Run();

            Assert.IsTrue(analogLogsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(analogLogsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = analogLogsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                Assert.AreEqual(result.GetStringValueFromPath("controlUnitID"), result.GetStringValueFromPath("controlUnitID"), "controlUnitID - compare");
                foreach (var item in result["items"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Date"), "Date");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Value"), "Value");

                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Conditions Get")]
        public void ConditionsGet()
        {
            var conditionsGetApi = new ConditionsGetApi();
            conditionsGetApi.Run();

            Assert.IsTrue(conditionsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(conditionsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = conditionsGetApi.ResponseBody["Body"];
            var items = result["Items"];
            Assert.Multiple(() =>
            {

                Assert.IsNotEmpty(result.GetStringValueFromPath("controlUnitID"), "controlUnitID");
                foreach (var item in items)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConditionNumber"), "Items.ConditionNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartTime"), "Items.StartTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndTime"), "Items.EndTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SetupTypeID"), "Items.SetupTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ElementType"), "Items.ElementType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ElemenNum"), "Items.ElemenNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RangeTypeID"), "Items.RangeTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OnValue"), "Items.OnValue");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OffValue"), "Items.OffValue");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Option"), "Items.Option");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CondOnDelay"), "Items.CondOnDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CondOffDelay"), "Items.CondOffDelay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Items.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OperatorsTypeID"), "Items.OperatorsTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("secondConditionNum"), "Items.secondConditionNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActionTypeID"), "Items.ActionTypeID");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Conditions Post")]
        public void ConditionsPost()
        {
            var conditionsPostApi = new ConditionsPostApi();
            var changedPayload = conditionsPostApi.payload.ToJObject();
            var newStartTime = 4;
            var newEndTime = 5;
            changedPayload[0]["StartTime"] = newStartTime;
            changedPayload[0]["EndTime"] = newEndTime;
            conditionsPostApi.RequestPayload = changedPayload.ToString();

            conditionsPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("ProSerialNumber")));

            conditionsPostApi.Run();

            Assert.IsTrue(conditionsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(conditionsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(conditionsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var conditionsGetApi = new ConditionsGetApi();
            conditionsGetApi.Run();

            Assert.IsTrue(conditionsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(conditionsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = conditionsGetApi.ResponseBody["Body"]["Items"][0];
            Assert.Multiple(() =>
            {
                Assert.AreEqual(newStartTime, result.GetIntValueFromPath("StartTime"), "Items.StartTime");
                Assert.AreEqual(newEndTime, result.GetIntValueFromPath("EndTime"), "Items.EndTime");
            });
        }

        [Test]
        [Category("Device")]
        [Category("Conditions setting")]
        public void ConditionsSetting()
        {
            var conditionsSettingGetApi = new ConditionsSettingGetApi();
            conditionsSettingGetApi.Run();

            Assert.IsTrue(conditionsSettingGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(conditionsSettingGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = conditionsSettingGetApi.ResponseBody["Body"];
            var settings = result["Setting"];
            Assert.Multiple(() =>
            {
                foreach (var item in settings["ElementType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ElementType.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ElementType.Name");

                    foreach (var nested in item["Items"])
                    {
                        Assert.IsNotEmpty(nested.GetStringValueFromPath("Num"), "ElementType.Items.Num");
                        Assert.IsNotEmpty(nested.GetStringValueFromPath("Name"), "ElementType.Items.Name");
                        Assert.IsNotEmpty(nested.GetStringValueFromPath("OutputName"), "ElementType.Items.NameOutputName");
                    }
                }

                foreach (var item in settings["Range"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Range.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Range.Name");
                }

                foreach (var item in settings["SetupType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "SetupType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "SetupType.Name");
                }

                foreach (var item in settings["OperatorsType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "OperatorsType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "OperatorsType.Name");
                }

                foreach (var item in settings["ConditionsUnit"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Num"), "ConditionsUnit.Num");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ConditionsUnit.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OutputName"), "ConditionsUnit.OutputName");
                }

                foreach (var item in settings["ActionType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "ActionType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ActionType.Name");
                }

                foreach (var item in settings["SubElementType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "SubElementType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "SubElementType.Name");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Sub element conditions")]
        public void SubElementConditions()
        {
            var subElementConditionsGetApi = new SubElementConditionsGetApi(new Dictionary<string, string> { { "Num", "1" } });
            subElementConditionsGetApi.Run();

            Assert.IsTrue(subElementConditionsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(subElementConditionsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = subElementConditionsGetApi.ResponseBody["Body"];
            var settings = result["Setting"];
            Assert.Multiple(() =>
            {
                foreach (var item in settings["SubElementType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "SubElementType.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "SubElementType.Name");

                    foreach (var nested in item["Items"])
                    {
                        Assert.IsNotEmpty(nested.GetStringValueFromPath("Num"), "SubElementType.Items.Num");
                        Assert.IsNotEmpty(nested.GetStringValueFromPath("Name"), "SubElementType.Items.Name");
                        Assert.IsNotEmpty(nested.GetStringValueFromPath("OutputName"), "SubElementType.Items.NameOutputName");
                    }
                }

                foreach (var item in settings["ElementSettings"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SubElementType"), "ElementSettings.SubElementType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ElementSettings.Name");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Programs list pro")]
        public void ProgramsListPro()
        {
            var programsListProGetApi = new ProgramsListProGetApi();
            programsListProGetApi.Run();

            Assert.IsTrue(programsListProGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(programsListProGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = programsListProGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("OrderNum"), "OrderNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Type"), "Type");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"), "WaterFactor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramSetup"), "ProgramSetup");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NextIrrigationDay"), "NextIrrigationDay");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NextIrrigationTime"), "NextIrrigationTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("line"), "line");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartTimeType"), "StartTimeType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CycleType"), "CycleType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterProgramType"), "WaterProgramType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NextTime"), "NextTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramWaterUnit"), "ProgramWaterUnit");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramStatus"), "ProgramStatus");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Fert programs")]
        public void FertPrograms()
        {
            var fertProgramsGetApi = new FertProgramsGetApi();
            fertProgramsGetApi.Run();

            Assert.IsTrue(fertProgramsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertProgramsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertProgramsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertCenterNum"), "FertCenterNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNum"), "ProgramNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramID"), "ProgramID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit"), "FertUnit");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnitName"), "FertUnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ECRequiered"), "ECRequiered");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PHRequiered"), "PHRequiered");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("MainFertDuringNoFert"), "MainFertDuringNoFert");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StopInFail"), "StopInFail");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CancelAlarm"), "CancelAlarm");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterCounterNum"), "WaterCounterNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramSetup"), "FertProgramSetup");
                    Assert.IsNotNull(item.GetObjectValueFromPath("FertStatusValves"), "FertStatusValves");
                    Assert.IsNotNull(item.GetObjectValueFromPath("fertValves"), "fertValves");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Fert program Get")]
        public void FertProgramGet()
        {
            var fertProgramGetApi = new FertProgramGetApi(new Dictionary<string, string> { { "ProgramID", "479862" } });
            fertProgramGetApi.Run();

            Assert.IsTrue(fertProgramGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertProgramGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertProgramGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result["Type"]["FertUnit"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Type.FertUnit.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Type.FertUnit.Name");
                }

                var fertProgram = result["FertProgram"];
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("FertCenterNum"), "FertCenterNum");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("ProgramNum"), "ProgramNum");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("Name"), "Name");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("ProgramID"), "ProgramID");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("FertUnit"), "FertUnit");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("FertUnitName"), "FertUnitName");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("ECRequiered"), "ECRequiered");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("PHRequiered"), "PHRequiered");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("WaterCounterNum"), "WaterCounterNum");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("Status"), "Status");
                Assert.IsNotEmpty(fertProgram.GetStringValueFromPath("FertProgramSetup"), "FertProgramSetup");
                Assert.IsNotNull(fertProgram.GetObjectValueFromPath("fertValves"), "fertValves");
            });
        }

        [Test]
        [Category("Device")]
        [Category("Fert program Post")]
        public void FertProgramPost()
        {
            var fertProgramPostApi = new FertProgramPostApi(new Dictionary<string, string> { { "ProgramID", "479862" } });
            fertProgramPostApi.RequestPayload = fertProgramPostApi.payload;

            fertProgramPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            fertProgramPostApi.Run();

            Assert.IsTrue(fertProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(fertProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            // Get check post method
            var fertProgramGetApi = new FertProgramGetApi(new Dictionary<string, string> { { "ProgramID", "479862" } });
            fertProgramGetApi.Run();

            Assert.IsTrue(fertProgramGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertProgramGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var fertProgram = fertProgramGetApi.ResponseBody["Body"]["FertProgram"];
            var payloadObject = fertProgramPostApi.payload.ToJObject();
            Assert.Multiple(() =>
            {
                Assert.AreEqual(payloadObject.GetIntValueFromPath("FertCenterNum"), fertProgram.GetIntValueFromPath("FertCenterNum"), "FertCenterNum");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("ProgramNum"), fertProgram.GetIntValueFromPath("ProgramNum"), "ProgramNum");
                Assert.AreEqual(payloadObject.GetStringValueFromPath("Name"), fertProgram.GetStringValueFromPath("Name"), "Name");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("ProgramID"), fertProgram.GetIntValueFromPath("ProgramID"), "ProgramID");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("FertUnit"), fertProgram.GetIntValueFromPath("FertUnit"), "FertUnit");
                Assert.AreEqual(payloadObject.GetStringValueFromPath("FertUnitName"), fertProgram.GetStringValueFromPath("FertUnitName"), "FertUnitName");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("ECRequiered"), fertProgram.GetIntValueFromPath("ECRequiered"), "ECRequiered");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("PHRequiered"), fertProgram.GetIntValueFromPath("PHRequiered"), "PHRequiered");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("WaterCounterNum"), fertProgram.GetIntValueFromPath("WaterCounterNum"), "WaterCounterNum");
                Assert.AreEqual(payloadObject.GetBoolValueFromPath("Status"), fertProgram.GetBoolValueFromPath("Status"), "Status");
                Assert.AreEqual(payloadObject.GetIntValueFromPath("FertProgramSetup"), fertProgram.GetIntValueFromPath("FertProgramSetup"), "FertProgramSetup");

                var num = 0;
                foreach(var item in fertProgram["fertValves"])
                {
                    Assert.AreEqual(payloadObject.GetIntValueFromPath("fertValves[" + num + "].fertNum"), item.GetIntValueFromPath("fertNum"), "fertNum");
                    Assert.AreEqual(payloadObject.GetIntValueFromPath("fertValves[" + num++ + "].value"), item.GetIntValueFromPath("value"), "value");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Fert logs")]
        public void FertLogs()
        {
            var fertLogsGetApi = new FertLogsGetApi(new Dictionary<string, string> { { "StartTicks", "1644142991000" }, { "EndTicks", "1644143537000" },
                                    {"FertProgramNum", "1" }, {"IrrigationProgramNumber", "1" }, {"StationNumber", "4" } });
            fertLogsGetApi.Run();

            Assert.IsTrue(fertLogsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertLogsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertLogsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RemoteRecordStart"), "RemoteRecordStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertCodeStart"), "FertCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit1"), "FertUnit1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit2"), "FertUnit2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit3"), "FertUnit3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit4"), "FertUnit4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd1"), "FertRequierd1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd2"), "FertRequierd2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd3"), "FertRequierd3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd4"), "FertRequierd4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert1"), "Fert1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert2"), "Fert2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert3"), "Fert3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert4"), "Fert4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ECRequierd"), "ECRequierd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PHRequierd"), "PHRequierd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EC"), "EC");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PH"), "PH");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("isDeleted"), "isDeleted");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertCodeEnd"), "FertCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Sensor logs")]
        public void SensorLogs()
        {
            var sensorLogsGetApi = new SensorLogsGetApi(new Dictionary<string, string> { { "Cols", "RecordDate_DESC" } });
            sensorLogsGetApi.Run();

            Assert.IsTrue(sensorLogsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(sensorLogsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = sensorLogsGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorValue1"), "SensorValue1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorValue2"), "SensorValue2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorValue3"), "SensorValue3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorValue4"), "SensorValue4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorValue5"), "SensorValue5");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SensorCode"), "SensorCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RowNumber"), "RowNumber");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Fert logs history")]
        public void FertLogsHistory()
        {
            var fertLogsHistoryGetApi = new FertLogsHistoryGetApi(new Dictionary<string, string> { { "StartTicks", "1644142991000" }, { "EndTicks", "1644143537000" },
                                    {"FertProgramNum", "1" }, {"IrrigationProgramNumber", "1" }, {"StationNumber", "4" } });
            fertLogsHistoryGetApi.Run();

            Assert.IsTrue(fertLogsHistoryGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertLogsHistoryGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertLogsHistoryGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RemoteRecordStart"), "RemoteRecordStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertCodeStart"), "FertCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit1"), "FertUnit1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit2"), "FertUnit2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit3"), "FertUnit3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertUnit4"), "FertUnit4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd1"), "FertRequierd1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd2"), "FertRequierd2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd3"), "FertRequierd3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertRequierd4"), "FertRequierd4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert1"), "Fert1");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert2"), "Fert2");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert3"), "Fert3");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fert4"), "Fert4");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ECRequierd"), "ECRequierd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PHRequierd"), "PHRequierd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EC"), "EC");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PH"), "PH");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("isDeleted"), "isDeleted");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertCodeEnd"), "FertCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Filter flush logs pro")]
        public void FilterFlushLogs()
        {
            var fertLogsHistoryGetApi = new FilterFlushLogsGetApi(new Dictionary<string, string> { { "RecordDateTicks", "1643068811000" },
                                        { "RemoteRecordID", "10768" } });
            fertLogsHistoryGetApi.Run();

            Assert.IsTrue(fertLogsHistoryGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(fertLogsHistoryGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = fertLogsHistoryGetApi.ResponseBody["Body"];
            Assert.Multiple(() =>
            {
                foreach (var item in result)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Code"), "Code");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FilterProgramNumber"), "FilterProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ManualDailyCycles"), "ManualDailyCycles");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PressDailyCycles"), "PressDailyCycles");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("QuantDailyCycles"), "QuantDailyCycles");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TimeDailyCycles"), "TimeDailyCycles");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ConditionDailyCycles"), "ConditionDailyCycles");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("isDeleted"), "isDeleted");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "RecordDateTicks");
                }
            });
        }

        [Test]
        [Category("Device")]
        [Category("Macro settings Get")]
        public void MacroSettingsGet()
        {
            var macroSettingsGetApi = new MacroSettingsGetApi();
            macroSettingsGetApi.Run();

            Assert.IsTrue(macroSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(macroSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = macroSettingsGetApi.ResponseBody["Body"];
            var types = result["Types"];
            Assert.Multiple(() =>
            {
                foreach (var item in types["ContractorsType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "ContractorsType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ContractorsType.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "ContractorsType.Permanent");
                }

                foreach (var item in types["RegionType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "RegionType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "RegionType.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "RegionType.Permanent");
                }

                foreach (var item in types["GeneralType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "GeneralType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "GeneralType.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "GeneralType.Permanent");
                }

                Assert.IsNotEmpty(result.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ContractorID"), "ContractorID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("RegionID"), "RegionID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("GeneralID"), "GeneralID");
            });
        }

        [TestCase(90, 51, 48)]
        [Test]
        [Category("Device")]
        [Category("Macro settings Post")]
        public void MacroSettingsPost(int contractorID, int regionID, int generalID)
        {
            var macroSettingsPostApi = new MacroSettingsPostApi(new Dictionary<string, string> { { "ProjectID", "418" } });
            var changedPayload = macroSettingsPostApi.payload.ToJObject();
            changedPayload["ContractorID"] = contractorID;
            changedPayload["RegionID"] = regionID;
            changedPayload["GeneralID"] = generalID;
            macroSettingsPostApi.RequestPayload = changedPayload.ToString();
            macroSettingsPostApi.Run();

            Assert.IsTrue(macroSettingsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(macroSettingsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            //Get check post method
            var macroSettingsGetApi = new MacroSettingsGetApi();
            macroSettingsGetApi.Run();

            Assert.IsTrue(macroSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(macroSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            var result = macroSettingsGetApi.ResponseBody["Body"];
            var types = result["Types"];
            Assert.Multiple(() =>
            {
                foreach (var item in types["ContractorsType"])
                {
                    Assert.AreEqual(contractorID, item.GetIntValueFromPath("TypeID"), "ContractorsType.TypeID");
                    Assert.AreEqual("ContractorsCheck", item.GetStringValueFromPath("Name"), "ContractorsType.Name");
                    Assert.AreEqual(false, item.GetBoolValueFromPath("Permanent"), "ContractorsType.Permanent");
                }

                foreach (var item in types["RegionType"])
                {
                    Assert.AreEqual(regionID, item.GetIntValueFromPath("TypeID"), "RegionType.TypeID");
                    Assert.AreEqual("RegionsCheck", item.GetStringValueFromPath("Name"), "RegionType.Name");
                    Assert.AreEqual(false, item.GetBoolValueFromPath("Permanent"), "RegionType.Permanent");
                }

                foreach (var item in types["GeneralType"])
                {
                    Assert.AreEqual(generalID, item.GetIntValueFromPath("TypeID"), "GeneralType.TypeID");
                    Assert.AreEqual("GeneralCheck", item.GetStringValueFromPath("Name"), "GeneralType.Name");
                    Assert.AreEqual(false, item.GetBoolValueFromPath("Permanent"), "GeneralType.Permanent");
                }

                Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("unitID"), result.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                Assert.AreEqual(contractorID, result.GetIntValueFromPath("ContractorID"), "ContractorID");
                Assert.AreEqual(regionID, result.GetIntValueFromPath("RegionID"), "RegionID");
                Assert.AreEqual(generalID, result.GetIntValueFromPath("GeneralID"), "GeneralID");
            });
        }

        [Test]
        [Category("Device")]
        [Category("Change water factor")]
        public void ChangeWaterFactor()
        {
            var changeWaterFactorPostApi = new ChangeWaterFactorPostApi(new Dictionary<string, string> { { "WaterFactor", "100" } });
            changeWaterFactorPostApi.RequestPayload = changeWaterFactorPostApi.payload;

            changeWaterFactorPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 5,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 5,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                    .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));

            changeWaterFactorPostApi.Run();

            Assert.IsTrue(changeWaterFactorPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(changeWaterFactorPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(changeWaterFactorPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }
    }
}