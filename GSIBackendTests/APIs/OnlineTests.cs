﻿using GSITestInfrastructure.GSIApis.Online;
using GSITestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class OnlineTests
    {
        [Test]
        [Category("Online")]
        [Category("Valve Open Now")]
        public void ValveOpenNow()
        {
            var valveOpenNowPostApi = new ValveOpenNowPostApi();

            valveOpenNowPostApi.RequestPayload = valveOpenNowPostApi.payload;

            valveOpenNowPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 1,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 1,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 2,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 70.0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            valveOpenNowPostApi.Run();

            Assert.IsTrue(valveOpenNowPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(valveOpenNowPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(valveOpenNowPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Stop valve now")]
        public void StopValveNow()
        {
            var stopValveNowPostApi = new StopValveNowPostApi();

            stopValveNowPostApi.RequestPayload = stopValveNowPostApi.payload;

            stopValveNowPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 1,
                                                        ""CommandProgram"": 1,
                                                        ""ValveCommands"": 1,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 1,
                                                        ""CommandCode"": 1,
                                                        ""ValveNumber"": 3,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                        }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            stopValveNowPostApi.Run();

            Assert.IsTrue(stopValveNowPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(stopValveNowPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(stopValveNowPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Update time left")]
        public void UpdateTimeLeft()
        {
            var updateTimeLeftPostApi = new UpdateTimeLeftPostApi();

            updateTimeLeftPostApi.RequestPayload = updateTimeLeftPostApi.payload;

            updateTimeLeftPostApi.AddRedisEventToValidate(@"{
                                                           ""Command"": 1,
                                                           ""CommandProgram"": 2,
                                                           ""ValveCommands"": 2,
                                                           ""SN"": ""SerialNumberPlaceHolder"",
                                                           ""CommandType"": 1,
                                                           ""CommandCode"": 2,
                                                           ""ValveNumber"": 3,
                                                           ""Slot"": 0,
                                                           ""TimeLeft"": 120,
                                                           ""ProgramNumber"": 101,
                                                           ""Value"": 0,
                                                           ""WaterUnit"": 2,
                                                           ""WaterMeterUnit"": 2,
                                                           ""CommType"": null,
                                                           ""Factor"": null,
                                                           ""RainOffDelay"": null,
                                                           ""RainyPipeNum"": null,
                                                           ""LogType"": null,
                                                           ""PipeNum"": null,
                                                           ""MinPause"": null,
                                                           ""StepNumber"": null,
                                                           ""VersionNum"": null
                                                         }"
                                               .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            updateTimeLeftPostApi.Run();

            Assert.IsTrue(updateTimeLeftPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(updateTimeLeftPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(updateTimeLeftPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Irrigation program start now")]
        public void IrrProgramStartNow()
        {
            var irrProgramStartNowPostApi = new IrrProgramStartNowPostApi();

            irrProgramStartNowPostApi.RequestPayload = irrProgramStartNowPostApi.payload;

            irrProgramStartNowPostApi.AddRedisEventToValidate(@"{
                                                               ""Command"": 2,
                                                               ""CommandProgram"": 0,
                                                               ""ValveCommands"": 0,
                                                               ""SN"": ""SerialNumberPlaceHolder"",
                                                               ""CommandType"": 2,
                                                               ""CommandCode"": 0,
                                                               ""ValveNumber"": 0,
                                                               ""Slot"": 0,
                                                               ""TimeLeft"": 0,
                                                               ""ProgramNumber"": 1,
                                                               ""Value"": 0,
                                                               ""WaterUnit"": 0,
                                                               ""WaterMeterUnit"": null,
                                                               ""CommType"": null,
                                                               ""Factor"": null,
                                                               ""RainOffDelay"": null,
                                                               ""RainyPipeNum"": null,
                                                               ""LogType"": null,
                                                               ""PipeNum"": null,
                                                               ""MinPause"": null,
                                                               ""StepNumber"": null,
                                                               ""VersionNum"": null
                                                             }"
                                                   .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            irrProgramStartNowPostApi.Run();

            Assert.IsTrue(irrProgramStartNowPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(irrProgramStartNowPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(irrProgramStartNowPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Irrigation program close now")]
        public void IrrProgramsCloseNow()
        {
            var irrProgramsCloseNowPostApi = new IrrProgramsCloseNowPostApi();

            irrProgramsCloseNowPostApi.RequestPayload = irrProgramsCloseNowPostApi.payload;

            irrProgramsCloseNowPostApi.AddRedisEventToValidate(@"{
                                                               ""Command"": 2,
                                                               ""CommandProgram"": 1,
                                                               ""ValveCommands"": 1,
                                                               ""SN"": ""SerialNumberPlaceHolder"",
                                                               ""CommandType"": 2,
                                                               ""CommandCode"": 1,
                                                               ""ValveNumber"": 0,
                                                               ""Slot"": 0,
                                                               ""TimeLeft"": 0,
                                                               ""ProgramNumber"": 1,
                                                               ""Value"": 0,
                                                               ""WaterUnit"": 0,
                                                               ""WaterMeterUnit"": null,
                                                               ""CommType"": null,
                                                               ""Factor"": null,
                                                               ""RainOffDelay"": null,
                                                               ""RainyPipeNum"": null,
                                                               ""LogType"": null,
                                                               ""PipeNum"": null,
                                                               ""MinPause"": null,
                                                               ""StepNumber"": null,
                                                               ""VersionNum"": null
                                                               }"
                                                   .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            irrProgramsCloseNowPostApi.Run();

            Assert.IsTrue(irrProgramsCloseNowPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(irrProgramsCloseNowPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(irrProgramsCloseNowPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Irrigation program pause")]
        public void IrrProgramsPause()
        {
            var irrProgramsPausePostApi = new IrrProgramsPausePostApi();

            irrProgramsPausePostApi.RequestPayload = irrProgramsPausePostApi.payload;

            irrProgramsPausePostApi.AddRedisEventToValidate(@"{
                                                            ""Command"": 2,
                                                            ""CommandProgram"": 2,
                                                            ""ValveCommands"": 2,
                                                            ""SN"": ""SerialNumberPlaceHolder"",
                                                            ""CommandType"": 2,
                                                            ""CommandCode"": 2,
                                                            ""ValveNumber"": 0,
                                                            ""Slot"": 0,
                                                            ""TimeLeft"": 0,
                                                            ""ProgramNumber"": 1,
                                                            ""Value"": 0,
                                                            ""WaterUnit"": 0,
                                                            ""WaterMeterUnit"": null,
                                                            ""CommType"": null,
                                                            ""Factor"": null,
                                                            ""RainOffDelay"": null,
                                                            ""RainyPipeNum"": null,
                                                            ""LogType"": null,
                                                            ""PipeNum"": null,
                                                            ""MinPause"": null,
                                                            ""StepNumber"": null,
                                                            ""VersionNum"": null
                                                            }"
                                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            irrProgramsPausePostApi.Run();

            Assert.IsTrue(irrProgramsPausePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(irrProgramsPausePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(irrProgramsPausePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Skip")]
        public void Skip()
        {
            var skipPostApi = new SkipPostApi();

            skipPostApi.RequestPayload = skipPostApi.payload;

            skipPostApi.AddRedisEventToValidate(@"{
                                                ""Command"": 2,
                                                ""CommandProgram"": 3,
                                                ""ValveCommands"": 3,
                                                ""SN"": ""SerialNumberPlaceHolder"",
                                                ""CommandType"": 2,
                                                ""CommandCode"": 3,
                                                ""ValveNumber"": 0,
                                                ""Slot"": 0,
                                                ""TimeLeft"": 0,
                                                ""ProgramNumber"": 2,
                                                ""Value"": 0,
                                                ""WaterUnit"": 0,
                                                ""WaterMeterUnit"": null,
                                                ""CommType"": null,
                                                ""Factor"": null,
                                                ""RainOffDelay"": null,
                                                ""RainyPipeNum"": null,
                                                ""LogType"": null,
                                                ""PipeNum"": null,
                                                ""MinPause"": null,
                                                ""StepNumber"": null,
                                                ""VersionNum"": null
                                                }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            skipPostApi.Run();

            Assert.IsTrue(skipPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(skipPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(skipPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Cancel alert")]
        public void CancelAlert()
        {
            var cancelAlertPostApi = new CancelAlertPostApi();

            cancelAlertPostApi.RequestPayload = cancelAlertPostApi.payload;

            cancelAlertPostApi.AddRedisEventToValidate(@"{
                                                       ""Command"": 0,
                                                       ""CommandProgram"": 0,
                                                       ""ValveCommands"": 0,
                                                       ""SN"": ""SerialNumberPlaceHolder"",
                                                       ""CommandType"": 0,
                                                       ""CommandCode"": 0,
                                                       ""ValveNumber"": 0,
                                                       ""Slot"": 0,
                                                       ""TimeLeft"": 0,
                                                       ""ProgramNumber"": 0,
                                                       ""Value"": 0,
                                                       ""WaterUnit"": 0,
                                                       ""WaterMeterUnit"": null,
                                                       ""CommType"": null,
                                                       ""Factor"": null,
                                                       ""RainOffDelay"": null,
                                                       ""RainyPipeNum"": null,
                                                       ""LogType"": null,
                                                       ""PipeNum"": null,
                                                       ""MinPause"": null,
                                                       ""StepNumber"": null,
                                                       ""VersionNum"": null
                                                       }"
                                                   .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            cancelAlertPostApi.Run();

            Assert.IsTrue(cancelAlertPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(cancelAlertPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(cancelAlertPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Valve open now pro")]
        public void ValveOpenNowPro()
        {
            var valveOpenNowProPostApi = new ValveOpenNowProPostApi();

            valveOpenNowProPostApi.RequestPayload = valveOpenNowProPostApi.payload;

            valveOpenNowProPostApi.AddRedisEventToValidate(@"{
                                                            ""Command"": 1,
                                                            ""CommandProgram"": 0,
                                                            ""ValveCommands"": 0,
                                                            ""SN"": ""SerialNumberPlaceHolder"",
                                                            ""CommandType"": 1,
                                                            ""CommandCode"": 0,
                                                            ""ValveNumber"": 3,
                                                            ""Slot"": 0,
                                                            ""TimeLeft"": 70.0,
                                                            ""ProgramNumber"": 0,
                                                            ""Value"": 0,
                                                            ""WaterUnit"": 2,
                                                            ""WaterMeterUnit"": null,
                                                            ""CommType"": null,
                                                            ""Factor"": null,
                                                            ""RainOffDelay"": null,
                                                            ""RainyPipeNum"": null,
                                                            ""LogType"": null,
                                                            ""PipeNum"": null,
                                                            ""MinPause"": null,
                                                            ""StepNumber"": null,
                                                            ""VersionNum"": null
                                                        }"
                                                .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            valveOpenNowProPostApi.Run();

            Assert.IsTrue(valveOpenNowProPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(valveOpenNowProPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(valveOpenNowProPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Rain off delay update")]
        public void RainOffDelayUpdate()
        {
            var rainOffDelayUpdatePostApi = new RainOffDelayUpdatePostApi();
            rainOffDelayUpdatePostApi.RequestPayload = rainOffDelayUpdatePostApi.payload;
            rainOffDelayUpdatePostApi.Run();

            Assert.IsTrue(rainOffDelayUpdatePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(rainOffDelayUpdatePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Reset logs")]
        public void ResetLogs()
        {
            var resetLogsPostApi = new ResetLogsPostApi();

            resetLogsPostApi.RequestPayload = resetLogsPostApi.payload;

            resetLogsPostApi.AddRedisEventToValidate(@"{
                                                    ""Command"": 10,
                                                    ""CommandProgram"": 0,
                                                    ""ValveCommands"": 0,
                                                    ""SN"": ""SerialNumberPlaceHolder"",
                                                    ""CommandType"": 10,
                                                    ""CommandCode"": 0,
                                                    ""ValveNumber"": 0,
                                                    ""Slot"": 0,
                                                    ""TimeLeft"": 0,
                                                    ""ProgramNumber"": 0,
                                                    ""Value"": 0,
                                                    ""WaterUnit"": 0,
                                                    ""WaterMeterUnit"": null,
                                                    ""CommType"": null,
                                                    ""Factor"": null,
                                                    ""RainOffDelay"": null,
                                                    ""RainyPipeNum"": null,
                                                    ""LogType"": 0,
                                                    ""PipeNum"": null,
                                                    ""MinPause"": null,
                                                    ""StepNumber"": null,
                                                    ""VersionNum"": null
                                                }"
                                        .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            resetLogsPostApi.Run();

            Assert.IsTrue(resetLogsPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(resetLogsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(resetLogsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Pause current pipe")]
        public void PauseCurrentPipe()
        {
            var pauseCurrentPipePostApi = new PauseCurrentPipePostApi();

            pauseCurrentPipePostApi.RequestPayload = pauseCurrentPipePostApi.payload;

            pauseCurrentPipePostApi.AddRedisEventToValidate(@"{
                                                    ""Command"": 11,
                                                    ""CommandProgram"": 0,
                                                    ""ValveCommands"": 0,
                                                    ""SN"": ""SerialNumberPlaceHolder"",
                                                    ""CommandType"": 11,
                                                    ""CommandCode"": 0,
                                                    ""ValveNumber"": 0,
                                                    ""Slot"": 0,
                                                    ""TimeLeft"": 0,
                                                    ""ProgramNumber"": 0,
                                                    ""Value"": 0,
                                                    ""WaterUnit"": 0,
                                                    ""WaterMeterUnit"": null,
                                                    ""CommType"": null,
                                                    ""Factor"": null,
                                                    ""RainOffDelay"": null,
                                                    ""RainyPipeNum"": null,
                                                    ""LogType"": null,
                                                    ""PipeNum"": 1,
                                                    ""MinPause"": null,
                                                    ""StepNumber"": null,
                                                    ""VersionNum"": null
                                                }"
                                        .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            pauseCurrentPipePostApi.Run();

            Assert.IsTrue(pauseCurrentPipePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(pauseCurrentPipePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(pauseCurrentPipePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Pause current program")]
        public void PauseCurrentProgram()
        {
            var pauseCurrentProgramPostApi = new PauseCurrentProgramPostApi();

            pauseCurrentProgramPostApi.RequestPayload = pauseCurrentProgramPostApi.payload;

            pauseCurrentProgramPostApi.AddRedisEventToValidate(@"{
                                                    ""Command"": 11,
                                                    ""CommandProgram"": 1,
                                                    ""ValveCommands"": 1,
                                                    ""SN"": ""SerialNumberPlaceHolder"",
                                                    ""CommandType"": 11,
                                                    ""CommandCode"": 1,
                                                    ""ValveNumber"": 0,
                                                    ""Slot"": 0,
                                                    ""TimeLeft"": 0,
                                                    ""ProgramNumber"": 0,
                                                    ""Value"": 0,
                                                    ""WaterUnit"": 0,
                                                    ""WaterMeterUnit"": null,
                                                    ""CommType"": null,
                                                    ""Factor"": null,
                                                    ""RainOffDelay"": null,
                                                    ""RainyPipeNum"": null,
                                                    ""LogType"": null,
                                                    ""PipeNum"": null,
                                                    ""MinPause"": null,
                                                    ""StepNumber"": null,
                                                    ""VersionNum"": null
                                                }"
                                        .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            pauseCurrentProgramPostApi.Run();

            Assert.IsTrue(pauseCurrentProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(pauseCurrentProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(pauseCurrentProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Pause pipe")]
        public void PausePipe()
        {
            var pausePipePostApi = new PausePipePostApi();

            pausePipePostApi.RequestPayload = pausePipePostApi.payload;

            pausePipePostApi.AddRedisEventToValidate(@"{
                                                     ""Command"": 11,
                                                     ""CommandProgram"": 2,
                                                     ""ValveCommands"": 2,
                                                     ""SN"": ""SerialNumberPlaceHolder"",
                                                     ""CommandType"": 11,
                                                     ""CommandCode"": 2,
                                                     ""ValveNumber"": 0,
                                                     ""Slot"": 0,
                                                     ""TimeLeft"": 0,
                                                     ""ProgramNumber"": 0,
                                                     ""Value"": 0,
                                                     ""WaterUnit"": 0,
                                                     ""WaterMeterUnit"": null,
                                                     ""CommType"": null,
                                                     ""Factor"": null,
                                                     ""RainOffDelay"": null,
                                                     ""RainyPipeNum"": null,
                                                     ""LogType"": null,
                                                     ""PipeNum"": 5,
                                                     ""MinPause"": 0,
                                                     ""StepNumber"": null,
                                                     ""VersionNum"": null
                                                    }"
                                              .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            pausePipePostApi.Run();

            Assert.IsTrue(pausePipePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(pausePipePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(pausePipePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Pause program")]
        public void PauseProgram()
        {
            var pauseProgramPostApi = new PauseProgramPostApi();

            pauseProgramPostApi.RequestPayload = pauseProgramPostApi.payload;

            pauseProgramPostApi.AddRedisEventToValidate(@"{
                                                    ""Command"": 11,
                                                    ""CommandProgram"": 3,
                                                    ""ValveCommands"": 3,
                                                    ""SN"": ""SerialNumberPlaceHolder"",
                                                    ""CommandType"": 11,
                                                    ""CommandCode"": 3,
                                                    ""ValveNumber"": 0,
                                                    ""Slot"": 0,
                                                    ""TimeLeft"": 0,
                                                    ""ProgramNumber"": 3,
                                                    ""Value"": 0,
                                                    ""WaterUnit"": 0,
                                                    ""WaterMeterUnit"": null,
                                                    ""CommType"": null,
                                                    ""Factor"": null,
                                                    ""RainOffDelay"": null,
                                                    ""RainyPipeNum"": null,
                                                    ""LogType"": null,
                                                    ""PipeNum"": null,
                                                    ""MinPause"": 0,
                                                    ""StepNumber"": null,
                                                    ""VersionNum"": null
                                                }"
                                        .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            pauseProgramPostApi.Run();

            Assert.IsTrue(pauseProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(pauseProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(pauseProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Release pipe")]
        public void ReleasePipe()
        {
            var releasePipePostApi = new ReleasePipePostApi();

            releasePipePostApi.RequestPayload = releasePipePostApi.payload;

            releasePipePostApi.AddRedisEventToValidate(@"{
                                                    ""Command"": 11,
                                                    ""CommandProgram"": 4,
                                                    ""ValveCommands"": 65535,
                                                    ""SN"": ""SerialNumberPlaceHolder"",
                                                    ""CommandType"": 11,
                                                    ""CommandCode"": 4,
                                                    ""ValveNumber"": 0,
                                                    ""Slot"": 0,
                                                    ""TimeLeft"": 0,
                                                    ""ProgramNumber"": 0,
                                                    ""Value"": 0,
                                                    ""WaterUnit"": 0,
                                                    ""WaterMeterUnit"": null,
                                                    ""CommType"": null,
                                                    ""Factor"": null,
                                                    ""RainOffDelay"": null,
                                                    ""RainyPipeNum"": null,
                                                    ""LogType"": null,
                                                    ""PipeNum"": 1,
                                                    ""MinPause"": null,
                                                    ""StepNumber"": null,
                                                    ""VersionNum"": null
                                                }"
                                        .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            releasePipePostApi.Run();

            Assert.IsTrue(releasePipePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(releasePipePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(releasePipePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Release program")]
        public void ReleaseProgram()
        {
            var releaseProgramPostApi = new ReleaseProgramPostApi();

            releaseProgramPostApi.RequestPayload = releaseProgramPostApi.payload;

            releaseProgramPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 11,
                                                        ""CommandProgram"": 5,
                                                        ""ValveCommands"": 65535,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 11,
                                                        ""CommandCode"": 5,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 4,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            releaseProgramPostApi.Run();

            Assert.IsTrue(releaseProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(releaseProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(releaseProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Cancel today fert program")]
        public void CancelTodayFertProgram()
        {
            var cancelTodayFertProgramPostApi = new CancelTodayFertProgramPostApi();

            cancelTodayFertProgramPostApi.RequestPayload = cancelTodayFertProgramPostApi.payload;

            cancelTodayFertProgramPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 12,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 12,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 6,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            cancelTodayFertProgramPostApi.Run();

            Assert.IsTrue(cancelTodayFertProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(cancelTodayFertProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(cancelTodayFertProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Cancel today fert pipe")]
        public void CancelTodayFertPipe()
        {
            var cancelTodayFertPipePostApi = new CancelTodayFertPipePostApi();

            cancelTodayFertPipePostApi.RequestPayload = cancelTodayFertPipePostApi.payload;

            cancelTodayFertPipePostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 12,
                                                        ""CommandProgram"": 1,
                                                        ""ValveCommands"": 1,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 12,
                                                        ""CommandCode"": 1,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": 2,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            cancelTodayFertPipePostApi.Run();

            Assert.IsTrue(cancelTodayFertPipePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(cancelTodayFertPipePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(cancelTodayFertPipePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Release today fert program")]
        public void ReleaseTodayFertProgram()
        {
            var releaseTodayFertProgramPostApi = new ReleaseTodayFertProgramPostApi();

            releaseTodayFertProgramPostApi.RequestPayload = releaseTodayFertProgramPostApi.payload;

            releaseTodayFertProgramPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 12,
                                                        ""CommandProgram"": 2,
                                                        ""ValveCommands"": 2,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 12,
                                                        ""CommandCode"": 2,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            releaseTodayFertProgramPostApi.Run();

            Assert.IsTrue(releaseTodayFertProgramPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(releaseTodayFertProgramPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(releaseTodayFertProgramPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Release today fert pipe")]
        public void ReleaseTodayFertPipe()
        {
            var releaseTodayFertPipePostApi = new ReleaseTodayFertPipePostApi();

            releaseTodayFertPipePostApi.RequestPayload = releaseTodayFertPipePostApi.payload;

            releaseTodayFertPipePostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 12,
                                                        ""CommandProgram"": 3,
                                                        ""ValveCommands"": 3,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 12,
                                                        ""CommandCode"": 3,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": 4,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            releaseTodayFertPipePostApi.Run();

            Assert.IsTrue(releaseTodayFertPipePostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(releaseTodayFertPipePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(releaseTodayFertPipePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Start step")]
        public void StartStep()
        {
            var startStepPostApi = new StartStepPostApi();

            startStepPostApi.RequestPayload = startStepPostApi.payload;

            startStepPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 2,
                                                        ""CommandProgram"": 4,
                                                        ""ValveCommands"": 65535,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 2,
                                                        ""CommandCode"": 4,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 1,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": 2,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            startStepPostApi.Run();

            Assert.IsTrue(startStepPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(startStepPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(startStepPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Start seq from step")]
        public void StartSeqFromStep()
        {
            var startSeqFromStepPostApi = new StartSeqFromStepPostApi();

            startSeqFromStepPostApi.RequestPayload = startSeqFromStepPostApi.payload;

            startSeqFromStepPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 2,
                                                        ""CommandProgram"": 5,
                                                        ""ValveCommands"": 65535,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 2,
                                                        ""CommandCode"": 5,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 1,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": 2,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            startSeqFromStepPostApi.Run();

            Assert.IsTrue(startSeqFromStepPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(startSeqFromStepPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(startSeqFromStepPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Stop seq from step")]
        public void StopSeqFromStep()
        {
            var stopSeqFromStepPostApi = new StopSeqFromStepPostApi();

            stopSeqFromStepPostApi.RequestPayload = stopSeqFromStepPostApi.payload;

            stopSeqFromStepPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 2,
                                                        ""CommandProgram"": 6,
                                                        ""ValveCommands"": 65535,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 2,
                                                        ""CommandCode"": 6,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 1,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": 2,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            stopSeqFromStepPostApi.Run();

            Assert.IsTrue(stopSeqFromStepPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(stopSeqFromStepPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(stopSeqFromStepPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Send new version")]
        public void SendNewVersion()
        {
            var sendNewVersionPostApi = new SendNewVersionPostApi();

            sendNewVersionPostApi.RequestPayload = sendNewVersionPostApi.payload;

            sendNewVersionPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 13,
                                                        ""CommandProgram"": 0,
                                                        ""ValveCommands"": 0,
                                                        ""SN"": ""0000000000115264"",
                                                        ""CommandType"": 13,
                                                        ""CommandCode"": 0,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": null,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": ""6.5.1.103""
                                                    }");
            sendNewVersionPostApi.Run();

            Assert.IsTrue(sendNewVersionPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(sendNewVersionPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(sendNewVersionPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Online")]
        [Category("Clear test result")]
        public void ClearTestResult()
        {
            var clearTestResultPostApi = new ClearTestResultPostApi();

            clearTestResultPostApi.RequestPayload = clearTestResultPostApi.payload;

            clearTestResultPostApi.AddRedisEventToValidate(@"{
                                                        ""Command"": 2,
                                                        ""CommandProgram"": 7,
                                                        ""ValveCommands"": 65535,
                                                        ""SN"": ""SerialNumberPlaceHolder"",
                                                        ""CommandType"": 2,
                                                        ""CommandCode"": 7,
                                                        ""ValveNumber"": 0,
                                                        ""Slot"": 0,
                                                        ""TimeLeft"": 0,
                                                        ""ProgramNumber"": 0,
                                                        ""Value"": 0,
                                                        ""WaterUnit"": 0,
                                                        ""WaterMeterUnit"": null,
                                                        ""CommType"": null,
                                                        ""Factor"": null,
                                                        ""RainOffDelay"": null,
                                                        ""RainyPipeNum"": null,
                                                        ""LogType"": null,
                                                        ""PipeNum"": 2,
                                                        ""MinPause"": null,
                                                        ""StepNumber"": null,
                                                        ""VersionNum"": null
                                                    }"
                                            .Replace("SerialNumberPlaceHolder", ConfigurationManagerWrapper.GetParameterValue("SerialNumber")));
            clearTestResultPostApi.Run();

            Assert.IsTrue(clearTestResultPostApi.RedisValidationResult, "Redis error");
            Assert.IsTrue(clearTestResultPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");

            Assert.IsTrue(clearTestResultPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }
    }
}