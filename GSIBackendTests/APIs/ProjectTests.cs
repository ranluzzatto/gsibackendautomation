﻿using GSITestInfrastructure.GSIApis.Project;
using GSITestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class ProjectTests
    {
        [Test]
        [Category("Project")]
        [Category("Unit list")]
        public void UnitList()
        {
            var unitListGetApi = new UnitListGetApi();
            unitListGetApi.Run();

            var result = unitListGetApi.ResponseBody;
            Assert.IsTrue(unitListGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach(var body in result["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("UnitName"), "UnitName");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Cache alerts")]
        public void CacheAlerts()
        {
            var cacheAlertsPostApi = new CacheAlertsPostApi();
            cacheAlertsPostApi.RequestPayload = cacheAlertsPostApi.payload;
            cacheAlertsPostApi.Run();

            var result = cacheAlertsPostApi.ResponseBody;
            Assert.IsTrue(cacheAlertsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var item in body["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "Response.ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "Response.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "Response.RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "Response.ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "Response.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AlertCode"), "Response.AlertCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Noticed"), "Response.Noticed");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "Response.StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "Response.ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "Response.ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "Response.StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "Response.ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "Response.NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Response.Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "Response.UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "Response.WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Comment"), "Response.Comment");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "Response.FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "Response.NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("InputName"), "Response.InputName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TimeInput"), "Response.TimeInput");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "Response.IsPro");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "Response.PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "Response.RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDateTicks"), "Response.ReceivedDateTicks");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Cash irrigation")]
        public void CashIrrigation()
        {
            var cashIrrigationPostApi = new CashIrrigationPostApi(new Dictionary<string, string> { { "ProgramLevel", "false" } });
            cashIrrigationPostApi.RequestPayload = cashIrrigationPostApi.payload;
            cashIrrigationPostApi.Run();

            var result = cashIrrigationPostApi.ResponseBody;
            Assert.IsTrue(cashIrrigationPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var item in body["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "Response.ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "Response.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "Response.StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "Response.EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "Response.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeStart"), "Response.IrrigationCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeEnd"), "Response.IrrigationCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "Response.StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "Response.StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "Response.ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "Response.ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterTime"), "Response.WaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterTime"), "Response.PlannedWaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "Response.WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterQuant"), "Response.PlannedWaterQuant");
                    Assert.IsNotNull(item.GetStringValueFromPath("FertQuant"), "Response.FertQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "Response.ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "Response.NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ErrorCode"), "Response.ErrorCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "Response.UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "Response.PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "Response.FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "Response.NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fertilizer_PulseTypeID"), "Response.Fertilizer_PulseTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedPulseViewTypeID"), "Response.PlannedPulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "Response.StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "Response.EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramNumber"), "Response.FertProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramName"), "Response.FertProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "Response.LineNum");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Alerts")]
        public void Alerts()
        {
            var alertsPostApi = new AlertsPostApi(new Dictionary<string, string> { { "StartTicks", "1644537599000" }, { "EndTicks", "1645103922000" } });
            alertsPostApi.RequestPayload = alertsPostApi.payload;
            alertsPostApi.Run();

            var result = alertsPostApi.ResponseBody;
            Assert.IsTrue(alertsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var item in body["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "Response.ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "Response.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDate"), "Response.RecordDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDate"), "Response.ReceivedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "Response.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AlertCode"), "Response.AlertCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "Response.StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "Response.ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "Response.StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "Response.ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "Response.ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Message"), "Response.Message");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "Response.UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "Response.FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "Response.NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "Response.WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RecordDateTicks"), "Response.RecordDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedDateTicks"), "Response.ReceivedDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("InputName"), "Response.InputName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Comment"), "Response.Comment");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TimeInput"), "Response.TimeInput");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "Response.IsPro");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Irrigation")]
        public void Irrigation()
        {
            var irrigationPostApi = new IrrigationPostApi(new Dictionary<string, string> { {"ProgramLevel" , "false"}, 
                                    { "StartTicks", "1644537599000" }, { "EndTicks", "1645105234000" } });
            irrigationPostApi.RequestPayload = irrigationPostApi.payload;
            irrigationPostApi.Run();

            var result = irrigationPostApi.ResponseBody;
            Assert.IsTrue(irrigationPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var item in body["Response"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "Response.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "Response.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeStart"), "Response.IrrigationCodeStart");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IrrigationCodeEnd"), "Response.IrrigationCodeEnd");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationNumber"), "Response.StationNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "Response.ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "Response.ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StationName"), "Response.StationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterTime"), "Response.WaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PlannedWaterTime"), "Response.PlannedWaterTime");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterQuant"), "Response.WaterQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertQuant"), "Response.FertQuant");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualFlow"), "Response.ActualFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDate"), "Response.StartDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDate"), "Response.EndDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ErrorCode"), "Response.ErrorCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ROW_NUMBER"), "Response.ROW_NUMBER");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitName"), "Response.UnitName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow"), "Response.NominalFlow");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("NominalFlow_ViewTypeID"), "Response.NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Fertilizer_PulseTypeID"), "Response.Fertilizer_PulseTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartDateTicks"), "Response.StartDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("EndDateTicks"), "Response.EndDateTicks");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PulseViewTypeID"), "Response.PulseViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FlowViewTypeID"), "Response.FlowViewTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramNumber"), "Response.FertProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertProgramName"), "Response.FertProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LineNum"), "Response.LineNum");
                }
            });

        }

        [Test]
        [Category("Project")]
        [Category("Report types")]
        public void ReportTypes()
        {
            var reportTypesGetApi = new ReportTypesGetApi();
            reportTypesGetApi.Run();

            var result = reportTypesGetApi.ResponseBody;
            Assert.IsTrue(reportTypesGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                foreach (var body in result["Body"])
                {
                    Assert.IsNotEmpty(body.GetStringValueFromPath("ReportID"), "ReportID");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("DisplayName"), "DisplayName");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("IsEnabled"), "IsEnabled");
                    Assert.IsNotEmpty(body.GetStringValueFromPath("OrderID"), "OrderID");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Unit monthly accumulators")]
        public void UnitMonthlyAccumulators()
        {
            var unitMonthlyAccumulatorsPostApi = new UnitMonthlyAccumulatorsPostApi();
            unitMonthlyAccumulatorsPostApi.RequestPayload = unitMonthlyAccumulatorsPostApi.payload;
            unitMonthlyAccumulatorsPostApi.Run();

            var result = unitMonthlyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(unitMonthlyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                Assert.IsNotEmpty(body.GetStringValueFromPath("GrandTotal"), "GrandTotal");

                foreach (var year in body["year"])
                {
                    Assert.IsNotEmpty(year.GetStringValueFromPath("year"), "year.year");
                    Assert.IsNotEmpty(year.GetStringValueFromPath("TotalValue"), "year.TotalValue");

                    foreach (var units in year["units"])
                    {
                        Assert.IsNotEmpty(units.GetStringValueFromPath("name"), "units.name");
                        Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                        Assert.IsNotEmpty(units.GetStringValueFromPath("total"), "units.total");

                        foreach (var month in units["month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("name"), "month.name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("total"), "month.total");
                        }
                    }

                    foreach (var totalmonth in year["Totalmonth"])
                    {
                        Assert.IsNotEmpty(totalmonth.GetStringValueFromPath("name"), "Totalmonth.name");
                        Assert.IsNotEmpty(totalmonth.GetStringValueFromPath("total"), "Totalmonth.total");
                    }
                }

                foreach (var grandTotalMonth in body["GrandTotalMonth"])
                {
                    Assert.IsNotEmpty(grandTotalMonth.GetStringValueFromPath("name"), "GrandTotalMonth.name");
                    Assert.IsNotEmpty(grandTotalMonth.GetStringValueFromPath("total"), "GrandTotalMonth.total");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Valve monthly accumulators")]
        public void ValveMontlyAccumulators()
        {
            var valveMontlyAccumulatorsPostApi = new ValveMontlyAccumulatorsPostApi();
            valveMontlyAccumulatorsPostApi.RequestPayload = valveMontlyAccumulatorsPostApi.payload;
            valveMontlyAccumulatorsPostApi.Run();

            var result = valveMontlyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(valveMontlyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "units.Name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var yearV in units["YearV"])
                    {
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("year"), "YearV.year");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("TotalValue"), "YearV.TotalValue");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("UnexpectedValue"), "YearV.UnexpectedValue");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("TimeTotal"), "YearV.TimeTotal");

                        foreach (var month in yearV["month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Total"), "month.Total");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TimeTotal"), "month.TimeTotal");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("UnexpectedValue"), "month.UnexpectedValue");

                            foreach (var entities in month["Entities"])
                            {
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                            }
                        }

                        foreach (var totalY in yearV["TotalY"])
                        {
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("ID"), "TotalY.ID");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Value"), "TotalY.Value");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Time"), "TotalY.Time");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Name"), "TotalY.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Program monthly accumulators")]
        public void ProgramMonthlyAccumulators()
        {
            var programMonthlyAccumulatorsPostApi = new ProgramMonthlyAccumulatorsPostApi();
            programMonthlyAccumulatorsPostApi.RequestPayload = programMonthlyAccumulatorsPostApi.payload;
            //var res = programMonthlyAccumulatorsPostApi.payload.ToJObject();
            //res["Requetst"]["StartTicks"]["0"] = "1643673600000";
            //res["Requetst"]["EndTicks"]["0"] = "1646006400000";
            //programMonthlyAccumulatorsPostApi.payload = res.ToString();
            programMonthlyAccumulatorsPostApi.Run();

            var result = programMonthlyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(programMonthlyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "units.Name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var yearV in units["YearV"])
                    {
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("year"), "YearV.year");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("TotalValue"), "YearV.TotalValue");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("UnexpectedValue"), "YearV.UnexpectedValue");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("TimeTotal"), "YearV.TimeTotal");

                        foreach (var month in yearV["month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Total"), "month.Total");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TimeTotal"), "month.TimeTotal");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("UnexpectedValue"), "month.UnexpectedValue");

                            foreach (var entities in month["Entities"])
                            {
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                            }
                        }

                        foreach (var totalY in yearV["TotalY"])
                        {
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("ID"), "TotalY.ID");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Value"), "TotalY.Value");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Time"), "TotalY.Time");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Name"), "TotalY.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Unit daily accumulators")]
        public void UnitDailyAccumulators()
        {
            var unitDailyAccumulatorsPostApi = new UnitDailyAccumulatorsPostApi();
            unitDailyAccumulatorsPostApi.RequestPayload = unitDailyAccumulatorsPostApi.payload;
            unitDailyAccumulatorsPostApi.Run();

            var result = unitDailyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(unitDailyAccumulatorsPostApi.ResponseStatusCode == System.Net.HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                var report = body["Report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "PulsViewLable");

                foreach (var daysTitles in body["DaysTitles"])
                {
                    Assert.IsNotEmpty(daysTitles.GetStringValueFromPath("Name"), "DaysTitles.Name");
                }

                foreach (var years in body["Years"])
                {
                    Assert.IsNotEmpty(years.GetStringValueFromPath("Year"), "Years.Year");
                    Assert.IsNotEmpty(years.GetStringValueFromPath("TotalY"), "Years.TotalY");

                    foreach (var grandTotal in years["GrandTotal"])
                    {
                        Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Day"), "GrandTotal.Day");
                        Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Total"), "GrandTotal.Total");
                    }

                    foreach (var months in years["Months"])
                    {
                        Assert.IsNotEmpty(months.GetStringValueFromPath("Name"), "Months.Name");
                        Assert.IsNotEmpty(months.GetStringValueFromPath("TotalM"), "Months.TotalM");

                        foreach (var totalDays in months["TotalDays"])
                        {
                            Assert.IsNotEmpty(totalDays.GetStringValueFromPath("Day"), "TotalDays.Day");
                            Assert.IsNotEmpty(totalDays.GetStringValueFromPath("Total"), "TotalDays.Total");
                        }

                        foreach (var units in months["Units"])
                        {
                            Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "Units.Name");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "Units.PulsViewLable");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("TotalU"), "Units.TotalU");

                            foreach (var day in units["Day"])
                            {
                                Assert.IsNotEmpty(day.GetStringValueFromPath("Day"), "Day.Day");
                                Assert.IsNotEmpty(day.GetStringValueFromPath("Total"), "Day.Total");
                            }
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Valve daily accumulators")]
        public void ValveDailyAccumulators()
        {
            var valveDailyAccumulatorsPostApi = new ValveDailyAccumulatorsPostApi();
            valveDailyAccumulatorsPostApi.RequestPayload = valveDailyAccumulatorsPostApi.payload;
            valveDailyAccumulatorsPostApi.Run();

            var result = valveDailyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(valveDailyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["Units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "Units.Name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "Units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var year in units["Year"])
                    {
                        Assert.IsNotEmpty(year.GetStringValueFromPath("Year"), "Year.Year");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TotalY"), "Year.TotalY");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TimeTotalY"), "Year.TimeTotalY");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("UnexpectedValue"), "Year.UnexpectedValue");

                        foreach (var month in year["Month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "Month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TotalM"), "Month.TotalM");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TimeTotal"), "Month.TimeTotal");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("UnexpectedValue"), "Month.UnexpectedValue");

                            foreach (var days in month["Days"])
                            {
                                Assert.IsNotEmpty(days.GetStringValueFromPath("day"), "Days.day");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TotalValueDay"), "Days.TotalValueDay");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TimeTotal"), "Days.TimeTotal");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("UnexpectedValue"), "Days.UnexpectedValue");

                                foreach (var entities in days["Entities"])
                                {
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                                }
                            }

                            foreach (var totalEntities in month["TotalEntities"])
                            {
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("ID"), "TotalEntities.ID");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Value"), "TotalEntities.Value");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Time"), "TotalEntities.Time");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Name"), "TotalEntities.Name");
                            }
                        }

                        foreach (var grandTotal in year["GrandTotal"])
                        {
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("ID"), "GrandTotal.ID");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Value"), "GrandTotal.Value");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Time"), "GrandTotal.Time");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Name"), "GrandTotal.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Program daily accumulators")]
        public void ProgramDailyAccumulators()
        {
            var programDailyAccumulatorsPostApi = new ProgramDailyAccumulatorsPostApi();
            programDailyAccumulatorsPostApi.RequestPayload = programDailyAccumulatorsPostApi.payload;
            programDailyAccumulatorsPostApi.Run();

            var result = programDailyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(programDailyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["Units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "Units.Name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "Units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var year in units["Year"])
                    {
                        Assert.IsNotEmpty(year.GetStringValueFromPath("Year"), "Year.Year");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TotalY"), "Year.TotalY");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TimeTotalY"), "Year.TimeTotalY");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("UnexpectedValue"), "Year.UnexpectedValue");

                        foreach (var month in year["Month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "Month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TotalM"), "Month.TotalM");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TimeTotal"), "Month.TimeTotal");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("UnexpectedValue"), "Month.UnexpectedValue");

                            foreach (var days in month["Days"])
                            {
                                Assert.IsNotEmpty(days.GetStringValueFromPath("day"), "Days.day");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TotalValueDay"), "Days.TotalValueDay");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TimeTotal"), "Days.TimeTotal");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("UnexpectedValue"), "Days.UnexpectedValue");

                                foreach (var entities in days["Entities"])
                                {
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                                }
                            }

                            foreach (var totalEntities in month["TotalEntities"])
                            {
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("ID"), "TotalEntities.ID");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Value"), "TotalEntities.Value");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Time"), "TotalEntities.Time");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Name"), "TotalEntities.Name");
                            }
                        }

                        foreach (var grandTotal in year["GrandTotal"])
                        {
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("ID"), "GrandTotal.ID");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Value"), "GrandTotal.Value");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Time"), "GrandTotal.Time");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Name"), "GrandTotal.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Unit monthly accumulators and fert")]
        public void UnitMonthlyAccumulatorsAndFert()
        {
            var unitMonthlyAccumulatorsAndFertPostApi = new UnitMonthlyAccumulatorsAndFertPostApi();
            unitMonthlyAccumulatorsAndFertPostApi.RequestPayload = unitMonthlyAccumulatorsAndFertPostApi.payload;
            unitMonthlyAccumulatorsAndFertPostApi.Run();

            var result = unitMonthlyAccumulatorsAndFertPostApi.ResponseBody;
            Assert.IsTrue(unitMonthlyAccumulatorsAndFertPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                Assert.IsNotEmpty(body.GetStringValueFromPath("GrandTotalWater"), "GrandTotalWater");
                Assert.IsNotEmpty(body.GetStringValueFromPath("GrandTotalFert"), "GrandTotalFert");

                foreach (var year in body["Year"])
                {
                    Assert.IsNotEmpty(year.GetStringValueFromPath("year"), "Year.year");
                    Assert.IsNotEmpty(year.GetStringValueFromPath("TotalWater"), "Year.TotalWater");
                    Assert.IsNotEmpty(year.GetStringValueFromPath("TotalFert"), "Year.TotalFert");

                    foreach (var units in year["Units"])
                    {
                        Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "Units.Name");
                        Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                        Assert.IsNotEmpty(units.GetStringValueFromPath("TotalWater"), "Units.TotalWater");
                        Assert.IsNotEmpty(units.GetStringValueFromPath("TotalFert"), "Units.TotalFert");

                        foreach (var month in units["Month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "Month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Water"), "Month.Water");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Fert"), "Month.Fert");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("MonthNum"), "Month.MonthNum");
                        }
                    }

                    foreach (var totalmonth in year["Totalmonth"])
                    {
                        Assert.IsNotEmpty(totalmonth.GetStringValueFromPath("Name"), "Totalmonth.Name");
                        Assert.IsNotEmpty(totalmonth.GetStringValueFromPath("Water"), "Totalmonth.Water");
                        Assert.IsNotEmpty(totalmonth.GetStringValueFromPath("Fert"), "Totalmonth.Fert");
                        Assert.IsNotEmpty(totalmonth.GetStringValueFromPath("MonthNum"), "Totalmonth.MonthNum");
                    }
                }

                foreach (var grandTotalMonth in body["GrandTotalMonth"])
                {
                    Assert.IsNotEmpty(grandTotalMonth.GetStringValueFromPath("Name"), "GrandTotalMonth.Name");
                    Assert.IsNotEmpty(grandTotalMonth.GetStringValueFromPath("Water"), "GrandTotalMonth.Water");
                    Assert.IsNotEmpty(grandTotalMonth.GetStringValueFromPath("Fert"), "GrandTotalMonth.Fert");
                    Assert.IsNotEmpty(grandTotalMonth.GetStringValueFromPath("MonthNum"), "GrandTotalMonth.MonthNum");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Unit daily accumulators and fert")]
        public void UnitDailyAccumulatorsAndFert()
        {
            var unitDailyAccumulatorsAndFertPostApi = new UnitDailyAccumulatorsAndFertPostApi();
            unitDailyAccumulatorsAndFertPostApi.RequestPayload = unitDailyAccumulatorsAndFertPostApi.payload;
            unitDailyAccumulatorsAndFertPostApi.Run();

            var result = unitDailyAccumulatorsAndFertPostApi.ResponseBody;
            Assert.IsTrue(unitDailyAccumulatorsAndFertPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var daysTitles in body["DaysTitles"])
                {
                    Assert.IsNotEmpty(daysTitles.GetStringValueFromPath("Name"), "DaysTitles.Name");
                }

                foreach (var year in body["year"])
                {
                    Assert.IsNotEmpty(year.GetStringValueFromPath("year"), "year.year");
                    Assert.IsNotEmpty(year.GetStringValueFromPath("TotalWater"), "year.TotalWater");
                    Assert.IsNotEmpty(year.GetStringValueFromPath("TotalFert"), "year.TotalFert");

                    foreach (var grandTotal in year["GrandTotal"])
                    {
                        Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("day"), "GrandTotal.day");
                        Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("TotalWater"), "GrandTotal.TotalWater");
                        Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("TotalFert"), "GrandTotal.TotalFert");
                    }

                    foreach (var months in year["months"])
                    {
                        Assert.IsNotEmpty(months.GetStringValueFromPath("name"), "months.name");
                        Assert.IsNotEmpty(months.GetStringValueFromPath("TotalWater"), "months.TotalWater");
                        Assert.IsNotEmpty(months.GetStringValueFromPath("TotalFert"), "months.TotalFert");

                        foreach (var totalDays in months["TotalDays"])
                        {
                            Assert.IsNotEmpty(totalDays.GetStringValueFromPath("day"), "TotalDays.day");
                            Assert.IsNotEmpty(totalDays.GetStringValueFromPath("TotalWater"), "TotalDays.TotalWater");
                            Assert.IsNotEmpty(totalDays.GetStringValueFromPath("TotalFert"), "TotalDays.TotalFert");
                        }

                        foreach (var units in months["units"])
                        {
                            Assert.IsNotEmpty(units.GetStringValueFromPath("name"), "units.name");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "units.PulsViewLable");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("TotalWater"), "units.TotalWater");
                            Assert.IsNotEmpty(units.GetStringValueFromPath("TotalFert"), "units.TotalFert");

                            foreach (var day in units["day"])
                            {
                                Assert.IsNotEmpty(day.GetStringValueFromPath("day"), "day.day");
                                Assert.IsNotEmpty(day.GetStringValueFromPath("TotalWater"), "day.TotalWater");
                                Assert.IsNotEmpty(day.GetStringValueFromPath("TotalFert"), "day.TotalFert");
                            }
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Valve monthly accumulators and fert")]
        public void ValveMontlyAccumulatorsAndFert()
        {
            var valveMonthlyAccumulatorsAndFertPostApi = new ValveMonthlyAccumulatorsAndFertPostApi();
            valveMonthlyAccumulatorsAndFertPostApi.RequestPayload = valveMonthlyAccumulatorsAndFertPostApi.payload;
            valveMonthlyAccumulatorsAndFertPostApi.Run();

            var result = valveMonthlyAccumulatorsAndFertPostApi.ResponseBody;
            Assert.IsTrue(valveMonthlyAccumulatorsAndFertPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "units.Name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var year in units["year"])
                    {
                        Assert.IsNotEmpty(year.GetStringValueFromPath("year"), "year.year");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TotalValue"), "year.TotalValue");

                        foreach (var month in year["month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Total"), "month.Total");

                            foreach (var entities in month["Entities"])
                            {
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                            }
                        }

                        foreach (var totalY in year["TotalY"])
                        {
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("ID"), "TotalY.ID");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Value"), "TotalY.Value");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Time"), "TotalY.Time");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Name"), "TotalY.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Valve daily accumulators and fert")]
        public void ValveDailyAccumulatorsAndFert()
        {
            var valveDailyAccumulatorsAndFertPostApi = new ValveDailyAccumulatorsAndFertPostApi();
            valveDailyAccumulatorsAndFertPostApi.RequestPayload = valveDailyAccumulatorsAndFertPostApi.payload;
            valveDailyAccumulatorsAndFertPostApi.Run();

            var result = valveDailyAccumulatorsAndFertPostApi.ResponseBody;
            Assert.IsTrue(valveDailyAccumulatorsAndFertPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("name"), "units.name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var year in units["year"])
                    {
                        Assert.IsNotEmpty(year.GetStringValueFromPath("year"), "year.year");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("totaly"), "year.totaly");

                        foreach (var month in year["month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("name"), "month.name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("totalm"), "month.totalm");

                            foreach (var days in month["days"])
                            {
                                Assert.IsNotEmpty(days.GetStringValueFromPath("day"), "days.day");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TotalValueDay"), "day.TotalValueDay");

                                foreach (var entities in days["Entities"])
                                {
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                                }
                            }

                            foreach (var totalEntities in month["TotalEntities"])
                            {
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("ID"), "TotalEntities.ID");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Value"), "TotalEntities.Value");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Time"), "TotalEntities.Time");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Name"), "TotalEntities.Name");
                            }
                        }

                        foreach (var grandTotal in year["GrandTotal"])
                        {
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("ID"), "GrandTotal.ID");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Value"), "GrandTotal.Value");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Time"), "GrandTotal.Time");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Name"), "GrandTotal.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Input monthly accumulators")]
        public void InputMonthlyAccumulators()
        {
            var inputMonthlyAccumulatorsPostApi = new InputMonthlyAccumulatorsPostApi();
            inputMonthlyAccumulatorsPostApi.RequestPayload = inputMonthlyAccumulatorsPostApi.payload;
            inputMonthlyAccumulatorsPostApi.Run();

            var result = inputMonthlyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(inputMonthlyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("name"), "units.name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var yearV in units["YearV"])
                    {
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("year"), "YearV.year");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("TotalValue"), "YearV.TotalValue");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("UnexpectedValue"), "YearV.UnexpectedValue");
                        Assert.IsNotEmpty(yearV.GetStringValueFromPath("TimeTotal"), "YearV.TimeTotal");

                        foreach (var month in yearV["month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Total"), "month.Total");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TimeTotal"), "month.TimeTotal");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("UnexpectedValue"), "month.UnexpectedValue");

                            foreach (var entities in month["Entities"])
                            {
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                            }
                        }

                        foreach (var totalY in yearV["TotalY"])
                        {
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("ID"), "TotalY.ID");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Value"), "TotalY.Value");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Time"), "TotalY.Time");
                            Assert.IsNotEmpty(totalY.GetStringValueFromPath("Name"), "TotalY.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Input daily accumulators")]
        public void InputDailyAccumulators()
        {
            var inputDailyAccumulatorsPostApi = new InputDailyAccumulatorsPostApi();
            inputDailyAccumulatorsPostApi.RequestPayload = inputDailyAccumulatorsPostApi.payload;
            inputDailyAccumulatorsPostApi.Run();

            var result = inputDailyAccumulatorsPostApi.ResponseBody;
            Assert.IsTrue(inputDailyAccumulatorsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                var report = body["report"];
                Assert.IsNotEmpty(report.GetStringValueFromPath("fromDate"), "report.fromDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("toDate"), "report.toDate");
                Assert.IsNotEmpty(report.GetStringValueFromPath("PulsViewLable"), "report.PulsViewLable");

                foreach (var units in body["Units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("Name"), "Units.Name");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("PulsViewLable"), "Units.PulsViewLable");

                    foreach (var entitiesTitles in units["EntitiesTitles"])
                    {
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("Name"), "EntitiesTitles.Name");
                        Assert.IsNotEmpty(entitiesTitles.GetStringValueFromPath("ID"), "EntitiesTitles.ID");
                    }

                    foreach (var year in units["Year"])
                    {
                        Assert.IsNotEmpty(year.GetStringValueFromPath("Year"), "Year.year");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TotalY"), "Year.TotalValue");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("TimeTotalY"), "Year.TimeTotalY");
                        Assert.IsNotEmpty(year.GetStringValueFromPath("UnexpectedValue"), "Year.UnexpectedValue");

                        foreach (var month in year["Month"])
                        {
                            Assert.IsNotEmpty(month.GetStringValueFromPath("Name"), "Month.Name");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TotalM"), "Month.TotalM");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("TimeTotal"), "Month.TimeTotal");
                            Assert.IsNotEmpty(month.GetStringValueFromPath("UnexpectedValue"), "Month.UnexpectedValue");

                            foreach (var days in month["Days"])
                            {
                                Assert.IsNotEmpty(days.GetStringValueFromPath("day"), "Days.day");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TotalValueDay"), "Days.TotalValueDay");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("TimeTotal"), "Days.TimeTotal");
                                Assert.IsNotEmpty(days.GetStringValueFromPath("UnexpectedValue"), "Days.UnexpectedValue");

                                foreach (var entities in days["Entities"])
                                {
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("ID"), "Entities.ID");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Value"), "Entities.Value");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Time"), "Entities.Time");
                                    Assert.IsNotEmpty(entities.GetStringValueFromPath("Name"), "Entities.Name");
                                }
                            }

                            foreach (var totalEntities in month["TotalEntities"])
                            {
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("ID"), "TotalEntities.ID");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Value"), "TotalEntities.Value");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Time"), "TotalEntities.Time");
                                Assert.IsNotEmpty(totalEntities.GetStringValueFromPath("Name"), "TotalEntities.Name");
                            }
                        }

                        foreach (var grandTotal in year["GrandTotal"])
                        {
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("ID"), "GrandTotal.ID");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Value"), "GrandTotal.Value");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Time"), "GrandTotal.Time");
                            Assert.IsNotEmpty(grandTotal.GetStringValueFromPath("Name"), "GrandTotal.Name");
                        }
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("All types")]
        public void AllTypes()
        {
            var allTypesGetApi = new AllTypesGetApi();
            allTypesGetApi.Run();

            var result = allTypesGetApi.ResponseBody;
            Assert.IsTrue(allTypesGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                foreach (var item in body["WaterPulseTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "WaterPulseTypes.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "WaterPulseTypes.Name");
                }

                foreach (var item in body["FertPulseTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FertPulseType"), "FertPulseTypes.FertPulseType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Quant"), "FertPulseTypes.Quant");
                }

                foreach (var item in body["LandSizeTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "LandSizeTypes.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "LandSizeTypes.Name");
                }

                foreach (var item in body["InputTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "InputTypes.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "InputTypes.Name");
                }

                foreach (var item in body["TimesZone"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ZoneID"), "TimesZone.ZoneID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DisplayName"), "TimesZone.DisplayName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GMTOffset"), "TimesZone.GMTOffset");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ActualOffset"), "TimesZone.ActualOffset");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ManualOffset"), "TimesZone.ManualOffset");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DaylightName"), "TimesZone.DaylightName");
                }

                foreach (var item in body["ConnectionIntervals"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalSeconds"), "ConnectionIntervals.TotalSeconds");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Factor"), "ConnectionIntervals.Factor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Interval"), "ConnectionIntervals.Interval");
                }

                foreach (var item in body["BatteryTypes"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "BatteryTypes.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "BatteryTypes.Name");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Settings Get")]
        public void SettingsGet()
        {
            var settingsGetApi = new SettingGetApi();
            settingsGetApi.Run();

            var result = settingsGetApi.ResponseBody;
            Assert.IsTrue(settingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("Name"), "Name");
                Assert.IsNotEmpty(body.GetStringValueFromPath("AreaID"), "AreaID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("DefaultTimeZoneID"), "DefaultTimeZoneID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("SeasonStartDate"), "SeasonStartDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("SeasonEndDate"), "SeasonEndDate");
                Assert.IsNotEmpty(body.GetStringValueFromPath("SeasonBudget"), "SeasonBudget");
                Assert.IsNotEmpty(body.GetStringValueFromPath("PeriodDailyLimit"), "PeriodDailyLimit");
                Assert.IsNotEmpty(body.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("IsMetric"), "IsMetric");

            });
        }

        [Test]
        [Category("Project")]
        [Category("ETmanual setting project Get")]
        public void ETmanualSettingProjectGet()
        {
            var eTmanualSettingProjectGetApi = new ETmanualSettingProjectGetApi();
            eTmanualSettingProjectGetApi.Run();

            var result = eTmanualSettingProjectGetApi.ResponseBody;
            Assert.IsTrue(eTmanualSettingProjectGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ETtype"), "ETtype");
                Assert.IsNotEmpty(body.GetStringValueFromPath("FactorRain"), "FactorRain");
                Assert.IsNotEmpty(body.GetStringValueFromPath("EnableFactorRain"), "EnableFactorRain");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ActualRainMM"), "ActualRainMM");

                foreach (var eTmanualSettingsView in body["ETmanualSettingsView"])
                {
                    Assert.IsNotEmpty(eTmanualSettingsView.GetStringValueFromPath("Month"), "Month");
                    Assert.IsNotEmpty(eTmanualSettingsView.GetStringValueFromPath("Week"), "Week");
                    Assert.IsNotEmpty(eTmanualSettingsView.GetStringValueFromPath("ET0"), "ET0");
                    Assert.IsNotEmpty(eTmanualSettingsView.GetStringValueFromPath("Rain"), "Rain");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("User projects")]
        public void UserProjects()
        {
            var userProjectsGetApi = new UserProjectsGetApi();
            userProjectsGetApi.Run();

            var result = userProjectsGetApi.ResponseBody;
            Assert.IsTrue(userProjectsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            foreach (var body in result["Body"])
            {
                Assert.IsNotEmpty(body.GetStringValueFromPath("ProjectID"), "ProjectID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ProjectName"), "ProjectName");
            }
        }

        [Test]
        [Category("Project")]
        [Category("Program scheduling")]
        public void ProgramScheduling()
        {
            var programSchedulingPostApi = new ProgramSchedulingPostApi(new Dictionary<string, string> {
                                            { "StartTicks", "1644098400000" }, { "EndTicks", "1644703199999" } });
            programSchedulingPostApi.RequestPayload = programSchedulingPostApi.payload;
            programSchedulingPostApi.Run();

            var result = programSchedulingPostApi.ResponseBody;
            Assert.IsTrue(programSchedulingPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ProjectID"), "ProjectID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach (var unit in body["Units"])
                {
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("Name"), "Units.Name");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("Status"), "Units.Status");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("SN"), "Units.SN");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("CorrectVersion"), "Units.CorrectVersion");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("ExpiredNoEntry"), "Units.ExpiredNoEntry");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("UnitPaymentExpirationDate"), "Units.UnitPaymentExpirationDate");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("UnitPaymentStatus"), "Units.UnitPaymentStatus");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("DaysLefttoExpired"), "Units.DaysLefttoExpired");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("UnpaidUnit"), "Units.UnpaidUnit");
                    Assert.IsNotEmpty(unit.GetStringValueFromPath("IsPro"), "Units.IsPro");

                    foreach (var irrigationSet in unit["IrrigationSet"])
                    {
                        Assert.IsNotEmpty(irrigationSet.GetStringValueFromPath("Name"), "IrrigationSet.Name");
                        Assert.IsNotEmpty(irrigationSet.GetStringValueFromPath("ProgramID"), "IrrigationSet.ProgramID");
                        Assert.IsNotEmpty(irrigationSet.GetStringValueFromPath("ProgramNumber"), "IrrigationSet.ProgramNumber");
                        Assert.IsNotEmpty(irrigationSet.GetStringValueFromPath("StartDate"), "IrrigationSet.StartDate");
                        Assert.IsNotEmpty(irrigationSet.GetStringValueFromPath("EndDate"), "IrrigationSet.EndDate");
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Charts data")]
        public void ChartsData()
        {
            var chartsDataPostApi = new ChartsDataPostApi();
            chartsDataPostApi.RequestPayload = chartsDataPostApi.payload;
            chartsDataPostApi.Run();

            var result = chartsDataPostApi.ResponseBody;
            Assert.IsTrue(chartsDataPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ProjectID"), "ProjectID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("PulseViewLabel"), "PulseViewLabel");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Code"), "Code");

                foreach (var units in body["Units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("UnitName"), "Units.UnitName");
                }

                foreach (var chart in body["Chart"])
                {
                    Assert.IsNotEmpty(chart.GetStringValueFromPath("name"), "Chart.name");
                    Assert.IsNotEmpty(chart.GetStringValueFromPath("Year"), "Chart.Year");
                    Assert.IsNotEmpty(chart.GetStringValueFromPath("Month"), "Chart.Month");
                    Assert.IsNotEmpty(chart.GetStringValueFromPath("Average"), "Chart.Average");
                }

                foreach (var chartByYear in body["ChartByYear"])
                {
                    foreach (var chartYear in chartByYear["ChartYear"])
                    {
                        Assert.IsNotEmpty(chartYear.GetStringValueFromPath("name"), "ChartYear.name");
                        Assert.IsNotEmpty(chartYear.GetStringValueFromPath("Year"), "ChartYear.Year");
                        Assert.IsNotEmpty(chartYear.GetStringValueFromPath("Month"), "ChartYear.Month");
                        Assert.IsNotEmpty(chartYear.GetStringValueFromPath("Average"), "ChartYear.Average");
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Get charts data AVG")]
        public void GetChartsDataAVG()
        {
            var getChartsDataAVGPostApi = new GetChartsDataAVGPostApi();
            getChartsDataAVGPostApi.RequestPayload = getChartsDataAVGPostApi.payload;
            getChartsDataAVGPostApi.Run();

            var result = getChartsDataAVGPostApi.ResponseBody;
            Assert.IsTrue(getChartsDataAVGPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ProjectID"), "ProjectID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("PulseViewLabel"), "PulseViewLabel");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Code"), "Code");

                foreach (var units in body["Units"])
                {
                    Assert.IsNotEmpty(units.GetStringValueFromPath("ControlUnitID"), "Units.ControlUnitID");
                    Assert.IsNotEmpty(units.GetStringValueFromPath("UnitName"), "Units.UnitName");
                }

                foreach (var unitList in body["UnitList"])
                {
                    Assert.IsNotEmpty(unitList.GetStringValueFromPath("name"), "UnitList.name");
                    Assert.IsNotEmpty(unitList.GetStringValueFromPath("ControlUnitID"), "UnitList.ControlUnitID");
                    Assert.IsNotEmpty(unitList.GetStringValueFromPath("total"), "UnitList.total");

                    foreach (var month in unitList["month"])
                    {
                        Assert.IsNotEmpty(month.GetStringValueFromPath("name"), "month.name");
                        Assert.IsNotEmpty(month.GetStringValueFromPath("total"), "month.total");
                    }
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Change state")]
        public void ChangeState()
        {
            var changeStatePostApi = new ChangeStatePostApi(new Dictionary<string, string> { { "StateID", "1" } });
            changeStatePostApi.Run();

            Assert.IsTrue(changeStatePostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(changeStatePostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Project")]
        [Category("Log out")]
        public void LogOut()
        {
            var logOutPostApi = new LogOutPostApi();
            logOutPostApi.Run();

            Assert.IsTrue(logOutPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(logOutPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Project")]
        [Category("Projects to new version")]
        public void ProjectsToNewVersion()
        {
            var projectsToNewVersionPostApi = new ProjectsToNewVersionPostApi();
            projectsToNewVersionPostApi.RequestPayload = projectsToNewVersionPostApi.payload;
            projectsToNewVersionPostApi.Run();

            var result = projectsToNewVersionPostApi.ResponseBody;
            Assert.IsTrue(projectsToNewVersionPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                foreach (var unitListToNewVersion in body["UnitListToNewVersion"])
                {
                    Assert.IsNotEmpty(unitListToNewVersion.GetStringValueFromPath("Name"), "UnitListToNewVersion.Name");
                    Assert.IsNotEmpty(unitListToNewVersion.GetStringValueFromPath("SN"), "UnitListToNewVersion.SN");
                    Assert.IsNotEmpty(unitListToNewVersion.GetStringValueFromPath("Version"), "UnitListToNewVersion.Version");
                    Assert.IsNotEmpty(unitListToNewVersion.GetStringValueFromPath("Model"), "UnitListToNewVersion.Model");
                    Assert.IsNotEmpty(unitListToNewVersion.GetStringValueFromPath("NewHardWare"), "UnitListToNewVersion.NewHardWare");
                    Assert.IsNotEmpty(unitListToNewVersion.GetStringValueFromPath("ConnectedStatus"), "UnitListToNewVersion.ConnectedStatus");

                    foreach (var listOfVersions in unitListToNewVersion["ListOfVersions"])
                    {
                        Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("VersionType"), "ListOfVersions.VersionType");
                        Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("Version"), "ListOfVersions.Version");
                        Assert.IsNotEmpty(listOfVersions.GetStringValueFromPath("TypeOfController"), "ListOfVersions.TypeOfController");
                    }
                }

                Assert.IsNotEmpty(body.GetStringValueFromPath("TotalCount"), "TotalCount");
            });
        }

        [Test]
        [Category("Project")]
        [Category("Macro settings")]
        public void MacroSettings()
        {
            var macroSettingsGetApi = new MacroSettingsGetApi();
            macroSettingsGetApi.Run();

            var result = macroSettingsGetApi.ResponseBody;
            Assert.IsTrue(macroSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];

                foreach (var item in body["Contractors"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Contractors.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Contractors.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "Contractors.Permanent");
                }

                foreach (var item in body["Region"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Region.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Region.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "Region.Permanent");
                }

                foreach (var item in body["General"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "General.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "General.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "General.Permanent");
                }

                foreach (var item in body["Classification"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Classification.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Classification.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "Classification.Permanent");
                }

                foreach (var item in body["CropType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "CropType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "CropType.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "CropType.Permanent");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Macro filter")]
        public void MacroFilter()
        {
            var macroFilterPostApi = new MacroFilterPostApi();
            macroFilterPostApi.RequestPayload = macroFilterPostApi.payload;
            macroFilterPostApi.Run();

            var result = macroFilterPostApi.ResponseBody;
            Assert.IsTrue(macroFilterPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("ObjectView"), "ObjectView");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ObjectType"), "ObjectType");

                var types = body["Types"];
                foreach (var item in types["Contractors"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Contractors.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Contractors.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "Contractors.Permanent");
                }

                foreach (var item in types["Region"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Region.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Region.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "Region.Permanent");
                }

                foreach (var item in types["General"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "General.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "General.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "General.Permanent");
                }

                foreach (var item in types["Classification"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "Classification.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "Classification.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "Classification.Permanent");
                }

                foreach (var item in types["CropType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "CropType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "CropType.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Permanent"), "CropType.Permanent");
                }

                foreach (var item in types["StatusType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "StatusType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "StatusType.Name");
                }

                foreach (var item in types["ActionType"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TypeID"), "ActionType.TypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ActionType.Name");
                }

                var resultsMacroFilterView = body["ResultsMacroFilterView"];
                foreach (var item in resultsMacroFilterView["UnitMacroFilterView"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "UnitMacroFilterView.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SN"), "UnitMacroFilterView.SN");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "UnitMacroFilterView.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StatusID"), "UnitMacroFilterView.StatusID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ContractorID"), "UnitMacroFilterView.ContractorID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ContractorName"), "UnitMacroFilterView.ContractorName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RegionID"), "UnitMacroFilterView.RegionID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RegionName"), "UnitMacroFilterView.RegionName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GeneralID"), "UnitMacroFilterView.GeneralID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GeneralName"), "UnitMacroFilterView.GeneralName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"), "UnitMacroFilterView.WaterFactor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "UnitMacroFilterView.IsPro");
                }

                foreach (var item in resultsMacroFilterView["ProgramMacroFilterView"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ProgramMacroFilterView.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SN"), "ProgramMacroFilterView.SN");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ProgramMacroFilterView.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ContractorID"), "ProgramMacroFilterView.ContractorID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ContractorName"), "ProgramMacroFilterView.ContractorName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RegionID"), "ProgramMacroFilterView.RegionID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RegionName"), "ProgramMacroFilterView.RegionName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GeneralID"), "ProgramMacroFilterView.GeneralID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GeneralName"), "ProgramMacroFilterView.GeneralName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramID"), "ProgramMacroFilterView.ProgramID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramNumber"), "ProgramMacroFilterView.ProgramNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramName"), "ProgramMacroFilterView.ProgramName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StatusID"), "ProgramMacroFilterView.StatusID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"), "ProgramMacroFilterView.WaterFactor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ClassificationTypeID"), "ProgramMacroFilterView.ClassificationTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ProgramClassificationName"), "ProgramMacroFilterView.ProgramClassificationName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitStatus"), "ProgramMacroFilterView.UnitStatus");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "ProgramMacroFilterView.IsPro");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StartTimeType"), "ProgramMacroFilterView.StartTimeType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CycleType"), "ProgramMacroFilterView.CycleType");
                }

                foreach (var item in resultsMacroFilterView["ValveMacroFilterView"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ValveMacroFilterView.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SN"), "ValveMacroFilterView.SN");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "ValveMacroFilterView.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ContractorID"), "ValveMacroFilterView.ContractorID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ContractorName"), "ValveMacroFilterView.ContractorName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RegionID"), "ValveMacroFilterView.RegionID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("RegionName"), "ValveMacroFilterView.RegionName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GeneralID"), "ValveMacroFilterView.GeneralID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("GeneralName"), "ValveMacroFilterView.GeneralName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveID"), "ValveMacroFilterView.ValveID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveNum"), "ValveMacroFilterView.ValveNum");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ValveName"), "ValveMacroFilterView.ValveName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("StatusID"), "ValveMacroFilterView.StatusID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("WaterFactor"), "ValveMacroFilterView.WaterFactor");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CropTypeID"), "ValveMacroFilterView.CropTypeID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CropTypeName"), "ValveMacroFilterView.CropTypeName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitStatus"), "ValveMacroFilterView.UnitStatus");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "ValveMacroFilterView.IsPro");
                }
            });
        }

        [Test]
        [Category("Project")]
        [Category("Update factor valves")]
        public void UpdateFactorValves()
        {
            var updateFactorValvesPostApi = new UpdateFactorValvesPostApi();
            updateFactorValvesPostApi.RequestPayload = updateFactorValvesPostApi.payload;
            updateFactorValvesPostApi.Run();

            Assert.IsTrue(updateFactorValvesPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(updateFactorValvesPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Project")]
        [Category("Update factor units")]
        public void UpdateFactorUnits()
        {
            var updateFactorUnitsPostApi = new UpdateFactorUnitsPostApi();
            updateFactorUnitsPostApi.RequestPayload = updateFactorUnitsPostApi.payload;
            updateFactorUnitsPostApi.Run();

            Assert.IsTrue(updateFactorUnitsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(updateFactorUnitsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Project")]
        [Category("Update factor programs")]
        public void UpdateFactorPrograms()
        {
            var updateFactorProgramsPostApi = new UpdateFactorProgramsPostApi();
            updateFactorProgramsPostApi.RequestPayload = updateFactorProgramsPostApi.payload;
            updateFactorProgramsPostApi.Run();

            Assert.IsTrue(updateFactorProgramsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(updateFactorProgramsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Project")]
        [Category("Update status programs")]
        public void UpdateStatusPrograms()
        {
            var updateStatusProgramsPostApi = new UpdateStatusProgramsPostApi();
            updateStatusProgramsPostApi.RequestPayload = updateStatusProgramsPostApi.payload;
            updateStatusProgramsPostApi.Run();

            Assert.IsTrue(updateStatusProgramsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(updateStatusProgramsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }

        [Test]
        [Category("Project")]
        [Category("Update status units")]
        public void UpdateStatusUnits()
        {
            var updateStatusUnitsPostApi = new UpdateStatusUnitsPostApi();
            updateStatusUnitsPostApi.RequestPayload = updateStatusUnitsPostApi.payload;
            updateStatusUnitsPostApi.Run();

            Assert.IsTrue(updateStatusUnitsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(updateStatusUnitsPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");
        }
    }
}