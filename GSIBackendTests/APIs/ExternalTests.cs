﻿using GSITestInfrastructure.GSIApis.ExternalApi;
using GSITestInfrastructure.Infrastructure;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GSIBackendTests.APIs
{
    public class ExternalTests
    {
        [Test]
        [Category("External")]
        [Category("Get config unit")]
        public void GetConfigUnit()
        {
            var getConfigUnitPostApi = new GetConfigUnitPostApi(new Dictionary<string, string> { { "Key", "6tLElZNBnLldZ0ERMfIzyypJinGztESkArMmGIm" } });
            getConfigUnitPostApi.RequestPayload = getConfigUnitPostApi.payload;
            getConfigUnitPostApi.Run();

            var result = getConfigUnitPostApi.ResponseBody["Body"];
            Assert.IsTrue(getConfigUnitPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getConfigUnitPostApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var externalUnitSettings = result["ExternalUnitSettings"];
                var config = externalUnitSettings["Config"];
                Assert.IsNotEmpty(config.GetStringValueFromPath("SN"), "Config.SN");
                Assert.AreEqual(ConfigurationManagerWrapper.GetParameterValue("SerialNumber"), config.GetStringValueFromPath("SN"), "Config.SN - compare");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ControlUnitID"), "Config.ControlUnitID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("UnitName"), "Config.UnitName");


                Assert.IsNotEmpty(config.GetStringValueFromPath("ModelView"), "Config.ModelView");
                Assert.IsNotEmpty(config.GetStringValueFromPath("WaterFactor"), "Config.WaterFactor");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ValveNum"), "Config.ValveNum");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CommTypeID"), "Config.CommTypeID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Status"), "Config.Status");
                Assert.IsNotEmpty(config.GetStringValueFromPath("CommUnit_AppVersion"), "Config.CommUnit_AppVersion");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LandTypeID"), "Config.LandTypeID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsGSIAg"), "Config.IsGSIAg");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Address"), "Config.Address");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Phone"), "Config.Phone");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ConnectionStatus"), "Config.ConnectionStatus");
                Assert.IsNotEmpty(config.GetStringValueFromPath("ControllerState"), "Config.ControllerState");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Map_Latitude"), "Config.Map_Latitude");
                Assert.IsNotEmpty(config.GetStringValueFromPath("Map_Longitude"), "Config.Map_Longitude");
                Assert.IsNotEmpty(config.GetStringValueFromPath("IsPRO"), "Config.IsPRO");
                Assert.IsNotEmpty(config.GetStringValueFromPath("LineNum"), "Config.LineNum");
                Assert.IsNotEmpty(config.GetStringValueFromPath("TimeZoneID"), "Config.TimeZoneID");
                Assert.IsNotEmpty(config.GetStringValueFromPath("NewHardware"), "Config.NewHardware");

                foreach (var valve in externalUnitSettings["Valves"])
                {
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveName"), "Valves.ValveName");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("ValveID"), "Valves.ValveID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("TypeID"), "Valves.TypeID");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("Status"), "Valves.Status");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("OutputNumber"), "Valves.OutputNumber");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("PrecipitationRate"), "Valves.PrecipitationRate");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IrrigrationArea"), "Valves.IrrigrationArea");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("LineNum"), "Valves.LineNum");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterMeter"), "Valves.WaterMeter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreWaterCounter"), "Valves.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("IgnoreMasterValve"), "Valves.IgnoreMasterValve");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("AuxiliaryOutput"), "Valves.AuxiliaryOutput");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("WaterMeterType"), "Valves.WaterMeterType");
                    Assert.IsNotEmpty(valve.GetStringValueFromPath("CalculatedNominalFlow"), "Valves.CalculatedNominalFlow");
                }

                foreach (var programs in externalUnitSettings["Programs"])
                {
                    Assert.IsNotEmpty(programs.GetStringValueFromPath("Status"), "Programs.Status");
                    Assert.IsNotEmpty(programs.GetStringValueFromPath("ProgramName"), "Programs.ProgramName");
                    Assert.IsNotEmpty(programs.GetStringValueFromPath("ProgramID"), "Programs.ProgramID");
                    Assert.IsNotEmpty(programs.GetStringValueFromPath("WaterFactor"), "Programs.WaterFactor");
                    Assert.IsNotEmpty(programs.GetStringValueFromPath("ProgramNumber"), "Programs.ProgramNumber");
                    Assert.IsNotEmpty(programs.GetStringValueFromPath("Type"), "Programs.Type");
                }

                var fertilizer = externalUnitSettings["Fertilizer"];
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseCustomFertilizerPulse"), "Fertilizer.UseCustomFertilizerPulse");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumber"), "Fertilizer.OutputNumber");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("ContinuousFert"), "Fertilizer.ContinuousFert");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseSize"), "Fertilizer.PulseSize");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseViewTypeID"), "Fertilizer.PulseViewTypeID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertPulseDuration"), "Fertilizer.FertPulseDuration");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FerlizerFaillureTime"), "Fertilizer.FerlizerFaillureTime");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("Leakage"), "Fertilizer.Leakage");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerType"), "Fertilizer.FertilizerType");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.FertPulseType"), "Fertilizer.PulseTypeID.FertPulseType");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("PulseTypeID.Quant"), "Fertilizer.PulseTypeID.Quant");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FlowTypeID"), "Fertilizer.FlowTypeID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("IsEnabled"), "Fertilizer.IsEnabled");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("NominalFlow"), "Fertilizer.NominalFlow");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertilizerID"), "Fertilizer.FertilizerID");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertCounterInputNum"), "Fertilizer.FertCounterInputNum");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseFertMeter4Error"), "Fertilizer.UseFertMeter4Error");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertLeakDetectActive"), "Fertilizer.FertLeakDetectActive");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseRelay"), "Fertilizer.UseRelay");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("UseMaster"), "Fertilizer.UseMaster");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("OutputNumberMaster"), "Fertilizer.OutputNumberMaster");
                Assert.IsNotEmpty(fertilizer.GetStringValueFromPath("FertNumber"), "Fertilizer.FertNumber");

                foreach (var fertCenter in externalUnitSettings["FertCenter"])
                {
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("FertCenterNumber"), "FertCenter.FertCenterNumber");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("Name"), "FertCenter.Name");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("FertMaxPumpNumber"), "FertCenter.FertMaxPumpNumber");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("StartDelay"), "FertCenter.StartDelay");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("ECSensorNum"), "FertCenter.ECSensorNum");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("PHSensorNum"), "FertCenter.PHSensorNum");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("DelayBefore"), "FertCenter.DelayBefore");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("FertAout"), "FertCenter.FertAout");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("MaxECInc"), "FertCenter.MaxECInc");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("MaxECDec"), "FertCenter.MaxECDec");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("MaxPHInc"), "FertCenter.MaxPHInc");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("MaxPHDec"), "FertCenter.MaxPHDec");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("LowECAlert"), "FertCenter.LowECAlert");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("HighECAlert"), "FertCenter.HighECAlert");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("LowPHAlert"), "FertCenter.LowPHAlert");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("HighPHAlert"), "FertCenter.HighPHAlert");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("AlertDelay"), "FertCenter.AlertDelay");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("LowECStop"), "FertCenter.LowECStop");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("HighECStop"), "FertCenter.HighECStop");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("LowPHStop"), "FertCenter.LowPHStop");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("HighPHStop"), "FertCenter.HighPHStop");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("IrrigationStopDelay"), "FertCenter.IrrigationStopDelay");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("ECKp"), "FertCenter.ECKp");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("ECKi"), "FertCenter.ECKi");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("ECKd"), "FertCenter.ECKd");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("PHKp"), "FertCenter.PHKp");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("PHKd"), "FertCenter.PHKd");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("PHKi"), "FertCenter.PHKi");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("LineNum"), "FertCenter.LineNum");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("MainFertDuringNoFert"), "FertCenter.MainFertDuringNoFert");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("StopInFertPumpFail"), "FertCenter.StopInFertPumpFail");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("AutoResetEnable"), "FertCenter.AutoResetEnable");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("OverlapLockChange"), "FertCenter.OverlapLockChange");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("OperationDueAverageFlow"), "FertCenter.OperationDueAverageFlow");
                    foreach (var fertPump in fertCenter["FertPumps"])
                    {
                        Assert.IsNotEmpty(fertPump.GetStringValueFromPath(""), "FertCenter.FertPumps");
                    }
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("AutoFertMethod"), "FertCenter.AutoFertMethod");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("OutputNumberFertMaster"), "FertCenter.OutputNumberFertMaster");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("Message_StartFertilizer"), "FertCenter.Message_StartFertilizer");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("Message_StopFertilizer"), "FertCenter.Message_StopFertilizer");
                    Assert.IsNotEmpty(fertCenter.GetStringValueFromPath("PercentFromRequired"), "FertCenter.PercentFromRequired");
                }
                

                foreach (var fertilizerPRO in externalUnitSettings["FertilizerPRO"])
                {
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("UseCustomFertilizerPulse"), "FertilizerPRO.UseCustomFertilizerPulse");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("OutputNumber"), "FertilizerPRO.OutputNumber");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("ContinuousFert"), "FertilizerPRO.ContinuousFert");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("PulseSize"), "FertilizerPRO.PulseSize");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("PulseViewTypeID"), "FertilizerPRO.PulseViewTypeID");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertPulseDuration"), "FertilizerPRO.FertPulseDuration");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FerlizerFaillureTime"), "FertilizerPRO.FerlizerFaillureTime");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("Leakage"), "FertilizerPRO.Leakage");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertilizerType"), "FertilizerPRO.FertilizerType");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("PulseTypeID.FertPulseType"), "FertilizerPRO.PulseTypeID.FertPulseType");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("PulseTypeID.Quant"), "FertilizerPRO.PulseTypeID.Quant");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FlowTypeID"), "FertilizerPRO.FlowTypeID");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("IsEnabled"), "FertilizerPRO.IsEnabled");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("NominalFlow"), "FertilizerPRO.NominalFlow");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertilizerID"), "FertilizerPRO.FertilizerID");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertCounterInputNum"), "FertilizerPRO.FertCounterInputNum");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("UseFertMeter4Error"), "FertilizerPRO.UseFertMeter4Error");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertLeakDetectActive"), "FertilizerPRO.FertLeakDetectActive");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("UseRelay"), "FertilizerPRO.UseRelay");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertNumber"), "FertilizerPRO.FertNumber");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertTypeID"), "FertilizerPRO.FertTypeID");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("FertType"), "FertilizerPRO.FertType");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("Name"), "FertilizerPRO.Name");
                    Assert.IsNotEmpty(fertilizerPRO.GetStringValueFromPath("RecommendedCycle"), "FertilizerPRO.RecommendedCycle");
                    foreach(var programNum in fertilizerPRO["ProgramNum"])
                    {
                        Assert.IsNotEmpty(programNum.GetStringValueFromPath("ProgramNumber"), "ProgramNum.ProgramNumber");
                    }
                }

                foreach(var projectListToUser in result["ProjectListToUser"])
                {
                    Assert.IsNotEmpty(projectListToUser.GetStringValueFromPath("ProjectName"), "ProjectListToUser.ProjectName");
                    foreach (var unitsList in projectListToUser["UnitsList"])
                    {
                        Assert.IsNotEmpty(unitsList.GetStringValueFromPath("SN"), "UnitsList.SN");
                    }
                }
                
            });
        }

        [Test]
        [Category("External")]
        [Category("Program settings")]
        public void ProgramSettings()
        {
            var programSettingsGetApi = new ProgramSettingsGetApi(new Dictionary<string, string> { { "Key", "6tLElZNBnLldZ0ERMfIzyypJinGztESkArMmGIm" } });
            programSettingsGetApi.Run();

            var result = programSettingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(programSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(programSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result.GetStringValueFromPath("Name"), "Name");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ProgramID"), "ProgramID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                Assert.IsNotEmpty(result.GetStringValueFromPath("Priority"), "Priority");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ConfigID"), "ConfigID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("IsFertilizerON"), "IsFertilizerON");
                Assert.IsNotEmpty(result.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("UnitPulseViewTypeID"), "UnitPulseViewTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ProgramSetup"), "ProgramSetup");
                Assert.IsNotEmpty(result.GetStringValueFromPath("NextIrrigationDay"), "NextIrrigationDay");
                Assert.IsNotEmpty(result.GetStringValueFromPath("NextIrrigationTime"), "NextIrrigationTime");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LineNum"), "LineNum");
                Assert.IsNotEmpty(result.GetStringValueFromPath("MainSecondaryValveID"), "MainSecondaryValveID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ProgramStatus"), "ProgramStatus");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ProgramWaterUnit"), "ProgramWaterUnit");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ClassificationTypeID"), "ClassificationTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("Status"), "Status");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ProgramType"), "ProgramType");
                Assert.IsNotEmpty(result.GetStringValueFromPath("FertExecutionType"), "FertExecutionType");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WaterFactor"), "WaterFactor");
                Assert.IsNotEmpty(result.GetStringValueFromPath("PlantTypeID"), "PlantTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DSundayState"), "WeeklyDaysProgram.DSundayState");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DMondayState"), "WeeklyDaysProgram.DMondayState");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DTuesdayState"), "WeeklyDaysProgram.DTuesdayState");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DWednesdayState"), "WeeklyDaysProgram.DWednesdayState");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DThursdayState"), "WeeklyDaysProgram.DThursdayState");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DFridayState"), "WeeklyDaysProgram.DFridayState");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WeeklyDaysProgram.DSaturdayState"), "WeeklyDaysProgram.DSaturdayState");

                foreach (var executionHours in result["ExecutionHours"])
                {
                    Assert.IsNotEmpty(executionHours.GetStringValueFromPath(""), "executionHours");
                }

                Assert.IsNotEmpty(result.GetStringValueFromPath("HoursExecution"), "HoursExecution");
                Assert.IsNotEmpty(result.GetStringValueFromPath("DaysExecutionType"), "DaysExecutionType");

                var cyclicDayProgram = result["CyclicDayProgram"];
                Assert.IsNotEmpty(cyclicDayProgram.GetStringValueFromPath("StartDate"), "CyclicDayProgram.StartDate");
                Assert.IsNotEmpty(cyclicDayProgram.GetStringValueFromPath("DaysInterval"), "CyclicDayProgram.DaysInterval");

                var hourlyCycle = result["HourlyCycle"];
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesPerDay"), "HourlyCycle.HourlyCyclesPerDay");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesStartTime"), "HourlyCycle.HourlyCyclesStartTime");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCycleTime"), "HourlyCycle.HourlyCycleTime");
                Assert.IsNotEmpty(hourlyCycle.GetStringValueFromPath("HourlyCyclesStopTime"), "HourlyCycle.HourlyCyclesStopTime");

                var advanced = result["Advanced"];
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStartDateLimit"), "Advanced.FinalStartDateLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalEndDateLimit"), "Advanced.FinalEndDateLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStartHoursLimit"), "Advanced.FinalStartHoursLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("FinalStopHoursLimit"), "Advanced.FinalStopHoursLimit");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("RunAndFinish"), "Advanced.RunAndFinish");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("RunWhenActive"), "Advanced.RunWhenActive");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("TerminateProgram"), "Advanced.TerminateProgram");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("PauseWhenActive"), "Advanced.PauseWhenActive");
                Assert.IsNotEmpty(advanced.GetStringValueFromPath("MaxWaitForStartCond"), "Advanced.MaxWaitForStartCond");

                foreach (var valveInProgram in result["ValveInProgram"])
                {
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ProgramID"), "ValveInProgram.ProgramID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OutputNumber"), "ValveInProgram.OutputNumber");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("OrderNumber"), "ValveInProgram.OrderNumber");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ID"), "ValveInProgram.ID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterQuantity"), "ValveInProgram.WaterQuantity");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertQuant"), "ValveInProgram.FertQuant");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertTime"), "ValveInProgram.FertTime");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("Name"), "ValveInProgram.Name");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterBefore"), "ValveInProgram.WaterBefore");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterAfter"), "ValveInProgram.WaterAfter");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterDuration"), "ValveInProgram.WaterDuration");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("WaterPrecipitation"), "ValveInProgram.WaterPrecipitation");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("SecondaryValveID"), "ValveInProgram.SecondaryValveID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveID"), "ValveInProgram.ValveID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ValveType"), "ValveInProgram.ValveType");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("ET"), "ValveInProgram.ET");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IsDeleted"), "ValveInProgram.IsDeleted");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("FertProgram"), "ValveInProgram.FertProgram");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("PulseViewTypeID"), "ValveInProgram.PulseViewTypeID");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IgnoreWaterCounter"), "ValveInProgram.IgnoreWaterCounter");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("IgnoreMasterValve"), "ValveInProgram.IgnoreMasterValve");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("AuxiliaryOutput"), "ValveInProgram.AuxiliaryOutput");
                    Assert.IsNotEmpty(valveInProgram.GetStringValueFromPath("LineNum"), "ValveInProgram.LineNum");
                }

                var unitPulseType = result["UnitPulseType"];
                if (unitPulseType.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("PulseTypeViewID"), "UnitPulseType.PulseTypeViewID");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("PulseTypeID"), "UnitPulseType.PulseTypeID");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("Name"), "UnitPulseType.Name");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("IsMetric"), "UnitPulseType.IsMetric");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("IsDefault"), "UnitPulseType.IsDefault");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("ViewFormat"), "UnitPulseType.ViewFormat");
                    Assert.IsNotEmpty(unitPulseType.GetStringValueFromPath("DecimalPlaces"), "UnitPulseType.DecimalPlaces");
                }

                var programPulseType = result["ProgramPulseType"];
                if (programPulseType.Type != JTokenType.Null)
                {
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("PulseTypeViewID"), "ProgramPulseType.PulseTypeViewID");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("PulseTypeID"), "ProgramPulseType.PulseTypeID");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("Name"), "ProgramPulseType.Name");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("IsMetric"), "ProgramPulseType.IsMetric");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("IsDefault"), "ProgramPulseType.IsDefault");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("ViewFormat"), "ProgramPulseType.ViewFormat");
                    Assert.IsNotEmpty(programPulseType.GetStringValueFromPath("DecimalPlaces"), "ProgramPulseType.DecimalPlaces");
                }

                foreach (var condition in result["Conditions"])
                {
                    Assert.IsNotEmpty(condition.GetStringValueFromPath(""), "Conditions");
                }
            });
            
        }

        [Test]
        [Category("External")]
        [Category("Valve settings")]
        public void ValveSettings()
        {
            var valveSettingsGetApi = new ValveSettingsGetApi(new Dictionary<string, string> { { "Key", "6tLElZNBnLldZ0ERMfIzyypJinGztESkArMmGIm" } });
            valveSettingsGetApi.Run();

            var result = valveSettingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(valveSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valveSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result.GetStringValueFromPath("Name"), "Name");
                Assert.IsNotEmpty(result.GetStringValueFromPath("OutputNumber"), "OutputNumber");
                Assert.IsNotEmpty(result.GetStringValueFromPath("Status"), "Status");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LowFlowDeviation"), "LowFlowDeviation");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LowFlowDelay"), "LowFlowDelay");
                Assert.IsNotEmpty(result.GetStringValueFromPath("HighFlowDeviation"), "HighFlowDeviation");
                Assert.IsNotEmpty(result.GetStringValueFromPath("HighFlowDelay"), "HighFlowDelay");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LineFillTime"), "LineFillTime");
                Assert.IsNotEmpty(result.GetStringValueFromPath("IrrigrationArea"), "IrrigrationArea");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ValveColor"), "ValveColor");
                Assert.IsNotEmpty(result.GetStringValueFromPath("PrecipitationRate"), "PrecipitationRate");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LastFlow"), "LastFlow");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LastFlow_Date"), "LastFlow_Date");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LastFlow_FlowTypeID"), "LastFlow_FlowTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("TypeID"), "TypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("FertilizerConnected"), "FertilizerConnected");
                Assert.IsNotEmpty(result.GetStringValueFromPath("SetupNominalFlow"), "SetupNominalFlow");
                Assert.IsNotEmpty(result.GetStringValueFromPath("StopOnFertFailure"), "StopOnFertFailure");
                Assert.IsNotEmpty(result.GetStringValueFromPath("Kc"), "Kc");
                Assert.IsNotEmpty(result.GetStringValueFromPath("PlantTypeID"), "PlantTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("SubCategoryID"), "SubCategoryID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WaterFactor"), "WaterFactor");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ValveID"), "ValveID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("ValveAlarm"), "ValveAlarm");
                Assert.IsNotEmpty(result.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("LineNum"), "LineNum");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WaterMeter"), "WaterMeter");
                Assert.IsNotEmpty(result.GetStringValueFromPath("IgnoreWaterCounter"), "IgnoreWaterCounter");
                Assert.IsNotEmpty(result.GetStringValueFromPath("IgnoreMasterValve"), "IgnoreMasterValve");
                Assert.IsNotEmpty(result.GetStringValueFromPath("WaterMeterType"), "WaterMeterType");
                Assert.IsNotEmpty(result.GetStringValueFromPath("PulseSize"), "PulseSize");
                Assert.IsNotEmpty(result.GetStringValueFromPath("FlowViewTypeWM"), "FlowViewTypeWM");
                foreach(var condition in result["Condition"])
                {
                    Assert.IsNotEmpty(condition.GetStringValueFromPath(""), "Condition");
                }
                Assert.IsNotEmpty(result.GetStringValueFromPath("CropTypeID"), "CropTypeID");
            });
        }

        [Test]
        [Category("External")]
        [Category("Irrigation logs")]
        public void IrrigationLogs()
        {
            var valveSettingsGetApi = new IrrigationLogsPostApi(new Dictionary<string, string> { { "StartTicks", "0" },
                                        { "EndTicks", "10" }, { "Key", "6tLElZNBnLldZ0ERMfIzyypJinGztESkArMmGIm" } });
            var changedPayload = valveSettingsGetApi.payload.ToJObject();
            var newTotalCount = 10;
            var newSearch = "irrigation";
            var newPageSize = 10;
            var newPageNumber = 1;
            var newColumnName = "Check";
            var newOrderType = "DESC";
            changedPayload["Requetst"]["TotalCount"] = newTotalCount;
            changedPayload["Requetst"]["Search"] = newSearch;
            changedPayload["Requetst"]["PageSize"] = newPageSize;
            changedPayload["Requetst"]["PageNumber"] = newPageNumber;
            changedPayload["Requetst"]["ColumnName"] = newColumnName;
            changedPayload["Requetst"]["OrderType"] = newOrderType;
            valveSettingsGetApi.RequestPayload = changedPayload.ToString();
            valveSettingsGetApi.Run();

            var dWHIrrigationView = valveSettingsGetApi.ResponseBody["Body"]["DWHIrrigationView"];
            Assert.IsTrue(valveSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valveSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(dWHIrrigationView.GetStringValueFromPath("TotalCount"), "TotalCount");

                foreach(var response in dWHIrrigationView["Response"])
                {
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ID"), "ID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ControlUnitID"), "ControlUnitID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("IrrigationCodeStart"), "IrrigationCodeStart");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("IrrigationCodeEnd"), "IrrigationCodeEnd");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("StationNumber"), "StationNumber");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ProgramNumber"), "ProgramNumber");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ProgramName"), "ProgramName");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("StationName"), "StationName");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("WaterTime"), "WaterTime");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("PlannedWaterTime"), "PlannedWaterTime");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("WaterQuant"), "WaterQuant");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("FertQuant"), "FertQuant");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ActualFlow"), "ActualFlow");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("StartDate"), "StartDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("EndDate"), "EndDate");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ErrorCode"), "ErrorCode");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("ROW_NUMBER"), "ROW_NUMBER");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("UnitName"), "UnitName");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("NominalFlow"), "NominalFlow");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("NominalFlow_ViewTypeID"), "NominalFlow_ViewTypeID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("Fertilizer_PulseTypeID"), "Fertilizer_PulseTypeID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("StartDateTicks"), "StartDateTicks");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("EndDateTicks"), "EndDateTicks");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("PulseViewTypeID"), "PulseViewTypeID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("FlowViewTypeID"), "FlowViewTypeID");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("FertProgramNumber"), "FertProgramNumber");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("FertProgramName"), "FertProgramName");
                    Assert.IsNotEmpty(response.GetStringValueFromPath("LineNum"), "LineNum");
                }
            });
        }

        [Test]
        [Category("External")]
        [Category("Key info")]
        public void KeyInfo()
        {
            var valveSettingsGetApi = new KeyInfoGetApi(new Dictionary<string, string> { { "Key", "6tLElZNBnLldZ0ERMfIzyypJinGztESkArMmGIm" } });
            valveSettingsGetApi.Run();

            var result = valveSettingsGetApi.ResponseBody["Body"];
            Assert.IsTrue(valveSettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(valveSettingsGetApi.ResponseBody.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result.GetStringValueFromPath("ID"), "ID");
                Assert.IsNotEmpty(result.GetStringValueFromPath("CompanyName"), "CompanyName");
                Assert.IsNotEmpty(result.GetStringValueFromPath("GalconKey"), "GalconKey");
                Assert.IsNotEmpty(result.GetStringValueFromPath("Count"), "Count");
                Assert.IsNotEmpty(result.GetStringValueFromPath("CurrentCount"), "CurrentCount");
                Assert.IsNotEmpty(result.GetStringValueFromPath("CreatDate"), "CreatDate");
                Assert.IsNotEmpty(result.GetStringValueFromPath("IsValid"), "IsValid");
                Assert.IsNotEmpty(result.GetStringValueFromPath("RequestsLeft"), "RequestsLeft");
            });
        }
    }
}
