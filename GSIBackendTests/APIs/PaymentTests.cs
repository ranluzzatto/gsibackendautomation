﻿using GSITestInfrastructure.GSIApis.Payment;
using GSITestInfrastructure.Infrastructure;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

namespace GSIBackendTests.APIs
{
    [TestFixture]
    public class PaymentTests
    {
        [Test]
        [Category("Payment")]
        [Category("Coupon list")]
        public void CouponList()
        {
            var couponListPostApi = new CouponListPostApi();
            couponListPostApi.RequestPayload = couponListPostApi.payload;
            couponListPostApi.Run();

            var result = couponListPostApi.ResponseBody;
            Assert.IsTrue(couponListPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                foreach(var item in body["Coupons"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CouponID"), "Coupons.CouponID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CouponCode"), "Coupons.CouponCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DiscountPercentage"), "Coupons.DiscountPercentage");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "Coupons.Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("DateOfUse"), "Coupons.DateOfUse");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControllerCount"), "Coupons.ControllerCount");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserCount"), "Coupons.UserCount");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("YearCount"), "Coupons.YearCount");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ReceivedbyID"), "Coupons.ReceivedbyID");
                }

                foreach (var item in body["ReceiversList"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "ReceiversList.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "ReceiversList.Name");
                }
            });
        }
        
        [Test]
        [Category("Payment")]
        [Category("Coupons")]
        public void Coupons()
        {
            var couponsPostApi = new CouponsPostApi();
            var res = couponsPostApi.payload.ToJObject();
            res["ControllerCount"] = "3";
            res["UserCount"] = "0";
            res["YearCount"] = "3";
            res["ReceivedbyID"] = "7";
            couponsPostApi.RequestPayload = res.ToString();
            couponsPostApi.Run();

            var getResponse = couponsPostApi.ResponseBody;
            Assert.IsTrue(couponsPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(getResponse.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsTrue(getResponse.GetBoolValueFromPath("Body"));
        }

        [Test]
        [Category("Payment")]
        [Category("Activate Coupon")] 
        public void ActivateCoupon()
        {
            var activateCouponGetApi = new ActivateCouponGetApi();
            activateCouponGetApi.Run();

            var response = activateCouponGetApi.ResponseBody;
            Assert.IsTrue(activateCouponGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("CouponID"), "CouponID");
                Assert.IsNotEmpty(body.GetStringValueFromPath("CouponCode"), "CouponCode");
                Assert.IsNotEmpty(body.GetStringValueFromPath("DiscountPercentage"), "DiscountPercentage");
                Assert.IsNotEmpty(body.GetStringValueFromPath("Status"), "Status");
                Assert.IsNotEmpty(body.GetStringValueFromPath("DateOfUse"), "DateOfUse");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ControllerCount"), "ControllerCount");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UserCount"), "UserCount");
                Assert.IsNotEmpty(body.GetStringValueFromPath("YearCount"), "YearCount");
                Assert.IsNotEmpty(body.GetStringValueFromPath("ReceivedbyID"), "ReceivedbyID");
            });
        }
        
        [Test]
        [Category("Payment")]
        [Category("Update status coupon")]
        public void UpdateStatusCoupon()
        {
            // Always takes the first coupon cause it's with the same id as we have in app.config
            var tempCouponID = ConfigurationManagerWrapper.GetParameterValue("CouponID");
            var tempCouponCode = ConfigurationManagerWrapper.GetParameterValue("CouponCode");
            var couponList = new CouponListPostApi();
            couponList.RequestPayload = couponList.payload;
            couponList.Run();

            var responseCouponList = couponList.ResponseBody;
            Assert.IsTrue(couponList.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(responseCouponList.GetBoolValueFromPath("Result"), "Service result error");

            var couponCode = responseCouponList.GetStringValueFromPath("Body.Coupons[0].CouponCode");
            ConfigurationManagerWrapper.SetParameterValue("CouponCode", couponCode);
            
            // api check starts
            var updateStatusCouponPostApi = new UpdateStatusCouponPostApi(new Dictionary<string, string> { { "CouponStatus", "0" } });
            updateStatusCouponPostApi.Run();

            var response = updateStatusCouponPostApi.ResponseBody;
            Assert.IsTrue(updateStatusCouponPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsTrue(response.GetBoolValueFromPath("Body"));
        }
        
        [Test]
        [Category("Payment")]
        [Category("Payment list")]
        public void PaymentList()
        {
            var paymentListPostApi = new PaymentListPostApi(new Dictionary<string, string> { { "currency", "1" }});
            paymentListPostApi.RequestPayload = paymentListPostApi.payload;
            paymentListPostApi.Run();

            var result = paymentListPostApi.ResponseBody;
            Assert.IsTrue(paymentListPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = result["Body"];
                Assert.IsNotEmpty(body.GetStringValueFromPath("UserTotalCount"), "UserTotalCount");
                Assert.IsNotEmpty(body.GetStringValueFromPath("UnitTotalCount"), "UnitTotalCount");

                foreach(var item in body["UserPayments"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "UserPayments.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Username"), "UserPayments.Username");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Role"), "UserPayments.Role");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserID"), "UserPayments.UserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserCreatedDate"), "UserPayments.UserCreatedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastPaymentDate"), "UserPayments.LastPaymentDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ExpirationDate"), "UserPayments.ExpirationDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Coin"), "UserPayments.Coin");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Price"), "UserPayments.Price");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FullPrice"), "UserPayments.FullPrice");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UserPrice"), "UserPayments.UserPrice");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TransactionID"), "UserPayments.TransactionID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TransactionYaadPayID"), "UserPayments.TransactionYaadPayID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PayerUserID"), "UserPayments.PayerUserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PayerName"), "UserPayments.PayerName");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PayerEmail"), "UserPayments.PayerEmail");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ApprovalCode"), "UserPayments.ApprovalCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PaymentActionType"), "UserPayments.PaymentActionType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AmountDifference"), "UserPayments.AmountDifference");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("YearCount"), "UserPayments.YearCount");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CouponCode"), "UserPayments.CouponCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CouponDiscountPercentage"), "UserPayments.CouponDiscountPercentage");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "UserPayments.Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalDiscount"), "UserPayments.TotalDiscount");
                }

                foreach (var item in body["UnitPayments"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ID"), "UnitPayments.ID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControlUnitID"), "UnitPayments.ControlUnitID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Name"), "UnitPayments.Name");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ControllerCreatedDate"), "UnitPayments.ControllerCreatedDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("LastPaymentDate"), "UnitPayments.LastPaymentDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ExpirationDate"), "UnitPayments.ExpirationDate");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Coin"), "UnitPayments.Coin");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Price"), "UnitPayments.Price");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("FullPrice"), "UnitPayments.FullPrice");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("UnitPrice"), "UnitPayments.UnitPrice");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SerialNumber"), "UnitPayments.SerialNumber");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TransactionID"), "UnitPayments.TransactionID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TransactionYaadPayID"), "UnitPayments.TransactionYaadPayID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PayerUserID"), "UnitPayments.PayerUserID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PayerName"), "UnitPayments.PayerName");
                    //Assert.IsNotEmpty(item.GetStringValueFromPath("PayerEmail"), "UnitPayments.PayerEmail");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("ApprovalCode"), "UnitPayments.ApprovalCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PaymentActionType"), "UnitPayments.PaymentActionType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("AmountDifference"), "UnitPayments.AmountDifference");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("YearCount"), "UnitPayments.YearCount");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CouponCode"), "UnitPayments.CouponCode");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("CouponDiscountPercentage"), "UnitPayments.CouponDiscountPercentage");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Status"), "UnitPayments.Status");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalDiscount"), "UnitPayments.TotalDiscount");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsPro"), "UnitPayments.IsPro");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("IsGSIAg"), "UnitPayments.IsGSIAg");
                }

                foreach (var item in body["UnitsReportSum"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumNis"), "UnitsReportSum.SumNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumUSD"), "UnitsReportSum.SumUSD");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumManualNis"), "UnitsReportSum.SumManualNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumManualUSD"), "UnitsReportSum.SumManualUSD");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalDiscountNis"), "UnitsReportSum.TotalDiscountNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalDiscountUSD"), "UnitsReportSum.TotalDiscountUSD");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalIncomeNis"), "UnitsReportSum.TotalIncomeNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalIncomeUSD"), "UnitsReportSum.TotalIncomeUSD");
                }

                foreach (var item in body["UserReportSum"])
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumNis"), "UserReportSum.SumNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumUSD"), "UserReportSum.SumUSD");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumManualNis"), "UserReportSum.SumManualNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("SumManualUSD"), "UserReportSum.SumManualUSD");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalDiscountNis"), "UserReportSum.TotalDiscountNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalDiscountUSD"), "UserReportSum.TotalDiscountUSD");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalIncomeNis"), "UserReportSum.TotalIncomeNis");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("TotalIncomeUSD"), "UserReportSum.TotalIncomeUSD");
                }
            });
        }
        
        [Test]
        [Category("Payment")]
        [Category("Step1")]
        public void Step1()
        {
            var step1PostApi = new Step1PostApi();
            step1PostApi.RequestPayload = step1PostApi.payload;
            step1PostApi.Run();

            var result = step1PostApi.ResponseBody;
            Assert.IsTrue(step1PostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            var body = result["Body"];
            Assert.IsNotEmpty(body.GetStringValueFromPath("PaymentUrl"));
        }

        [Test]
        [Category("Payment")]
        [Category("Paid by coupon")]
        public void PaidByCoupon()
        {
            // Get coupon list
            var couponListPostApi = new CouponListPostApi();
            couponListPostApi.RequestPayload = couponListPostApi.payload;
            couponListPostApi.Run();

            var resultCoupon = couponListPostApi.ResponseBody;
            Assert.IsTrue(couponListPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(resultCoupon.GetBoolValueFromPath("Result"), "Service result error");

            // Post method
            var paidByCouponPostApi = new PaidByCouponPostApi();
            var couponsList = resultCoupon["Body"]["Coupons"];

            foreach (var item in couponsList)
            {
                if ((item.GetIntValueFromPath("Status") == 0) && (item.GetIntValueFromPath("DiscountPercentage") == 100))
                {
                    var coupon = item.ToString().ToJObject();
                    var changedPayload = paidByCouponPostApi.payload.ToJObject();
                    changedPayload["YearCount"] = coupon["YearCount"];
                    changedPayload["CouponID"] = coupon["CouponID"];
                    paidByCouponPostApi.RequestPayload = changedPayload.ToString();
                }
            }

            paidByCouponPostApi.Run();

            var result = paidByCouponPostApi.ResponseBody;
            Assert.IsTrue(paidByCouponPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsTrue(result.GetBoolValueFromPath("Body"));
        }

        [Test]
        [Category("Payment")]
        [Category("Manual")]
        public void Manual()
        {
            var manualPostApi = new ManualPostApi();
            manualPostApi.RequestPayload = manualPostApi.payload;
            manualPostApi.Run();

            var result = manualPostApi.ResponseBody;
            Assert.IsTrue(manualPostApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(result.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsTrue(result.GetBoolValueFromPath("Body"));
        }
        
        [Test]
        [Category("Payment")]
        [Category("Prices Get Api")]
        public void Prices()
        {
            var priceGetApi = new PricesGetApi();
            priceGetApi.Run();

            var response = priceGetApi.ResponseBody;
            Assert.IsTrue(priceGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.Multiple(() =>
            {
                var body = response["Body"];
                foreach(var item in body)
                {
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PaymentPriceID"), "PaymentPriceID");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("PaymentPriceType"), "PaymentPriceType");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Coin"), "Coin");
                    Assert.IsNotEmpty(item.GetStringValueFromPath("Price"), "Price");
                }
            });
        }

        [Test]
        [Category("Payment")]
        [Category("Settings Get")]
        public void SettingsGet()
        {
            var aettingsGetApi = new SettingsGetApi();
            aettingsGetApi.Run();

            var response = aettingsGetApi.ResponseBody;
            Assert.IsTrue(aettingsGetApi.ResponseStatusCode == HttpStatusCode.OK, "HTTP request error");
            Assert.IsTrue(response.GetBoolValueFromPath("Result"), "Service result error");

            Assert.IsNotEmpty(response.GetStringValueFromPath( "Body.SumDifference"), "SumDifference");
        }
    }
}
