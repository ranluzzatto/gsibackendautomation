﻿using GSITestInfrastructure.Infrastructure;
using GSITestInfrastructure.Redis;
using NUnit.Framework;

namespace GSIBackendTests.Setup
{
    [SetUpFixture]
    public class TestsSetupAndFinalize
    {
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            //invoke connection to redis server prior to running the tests
            var redisConnection = RedisSubcriptionService.Instance;
            var host = ConfigurationManagerWrapper.GetParameterValue("SocketHost");
            if (redisConnection.IsConnected())
                TestContext.WriteLine("Succesfully connected to redis " + host + " server");
            else
                throw new System.Exception("Failed to connect to redis " + host + " server");
        }

        [OneTimeTearDown]
        public void RunAfterAnyTests()
        {
            RedisSubcriptionService.Instance.Disconnect();
        }
    }
}
